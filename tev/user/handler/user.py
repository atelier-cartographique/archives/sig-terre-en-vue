# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from ..models import Role, User, Auth
from ..session import SessionManager
from ...db.db import get_session
from ...psock.proto import response_error, response_ok
from uuid import UUID, uuid4

class UserHandler:

    def __init__ (self):
        self.session = SessionManager()

    def create(self, request):
        params = request.params
        if 'email' not in params:
            return response_error('missing id')
        if 'password' not in params:
            return response_error('missing password')
        if 'name' not in params:
            return response_error('missing name')
        if 'role' not in params:
            return response_error('missing role')

        id = uuid4()
        a = Auth(id=id, email=params['email'], password=params['password'])
        u = User(auth_id=id, name=params['name'], role=params['role'])
        sess = get_session('app')
        query = sess.query(Auth).filter(Auth.email == params['email'])
        if query.count() > 0:
            return response_error('this email is already registered')

        sess.add(a)
        sess.add(u)
        sess.commit()
        return response_ok({
            'id': str(u.id),
            'name': u.name,
            'role': u.role
        })

    def update_password (self, request):
        params = request.params
        if 'email' not in params:
            return response_error('missing id')
        if 'password' not in params:
            return response_error('missing password')

        sess = get_session('app')
        user_auth = sess.query(Auth).filter(Auth.email == params['email']).first()
        user_auth.password = params['password']
        sess.commit()
        return response_ok({
            'password': str(user_auth.password),
        })


    def ls (self, request):
        sess = get_session('app')
        query = sess.query(User)
        results = []
        for user in query:
            results.append({
                'id': str(user.id),
                'name': user.name,
                'role': user.role
            })
        return response_ok(results)
        # return response_ok(dict(users=results))

    def get(self, request):
        params = request.params
        uid = params.get('id')
        sess = get_session('app')
        result = sess.query(User).get(uid)

        if not result:
            return response_error('NotFound')

        return response_ok({
            'id': str(result.id),
            'name': result.name,
            'role': result.role
        })


    def login (self, request):
        email = request.params['email']
        password = request.params['password']
        sess = get_session('app')
        query = sess.query(Auth).filter(Auth.email == email)

        if query.count() > 0:
            a = query.first()
            if a.password == password:
                q2 = sess.query(User).filter(User.auth_id == a.id)
                u = q2.first()
                session_id = self.session.start(u.id)
                print('LOGIN {} {}'.format(u.name, u.id))
                return response_ok(dict(
                    sid=str(session_id),
                    name=u.name,
                    role=u.role,
                    id=str(u.id),
                    timeout = SessionManager.timeout
                    ))

        return response_error('wrong credentials')


    def logout (self, request):
        sid = request.sid
        uid =  self.session.get_user_id(sid)
        self.session.stop(sid)

        sess = get_session('app')
        query = sess.query(User).filter(User.id == uid)
        u = query.first()
        ruid = UUID(uid)
        if ruid == u.id:
            return response_ok(dict(message='Bye {}'.format(u.name)))

        return response_error('hmm, pas cashere tout ca {} {}'.format(
                                u.id, u.name
                            ))

    def get_user_id (self, request):
        """get user ID from session ID"""
        sid = request.sid
        uid =  self.session.get_user_id(sid)
        if uid:
            return response_ok(dict(uid=uid))
        return response_error('InvalidSid')
