# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from ..models import User, Resource, Document, Media, Query, Plot, Link
from ..session import SessionManager
from ...db.db import get_session
from ...psock.proto import response_ok, response_error
from ...geomet import wkt
from .format import ResourceFormat
from uuid import UUID, uuid4
from copy import deepcopy
from pathlib import Path
from geoalchemy2.elements import WKTElement

import logging
logger = logging.getLogger('tev')

RESOURCE_MAP = {
    'document': Document,
    'media': Media,
    'query': Query,
    'plot': Plot
}

class ResourceResponse ():

    @staticmethod
    def document (resource):
        query = get_session('app').query(Document).filter(Document.id == resource.resource_id)
        doc = query.first()
        return response_ok(ResourceFormat.document(resource, doc))


    @staticmethod
    def query (resource):
        query = get_session('app').query(Query).filter(Query.id == resource.resource_id)
        q = query.first()
        return response_ok(ResourceFormat.query(resource, q))


    @staticmethod
    def plot (resource):
        query = get_session('app').query(Plot).filter(Plot.id == resource.resource_id)
        q = query.first()
        return response_ok(ResourceFormat.plot(resource, q))


    @staticmethod
    def media (resource):
        query = get_session('app').query(Media).filter(Media.id == resource.resource_id)
        m = query.first()
        return response_ok(ResourceFormat.media(resource, m))


class ResourceHandler:

    def __init__ (self):
        self.session = SessionManager()


    def create_document(self, request):
        params = request.params
        user_id = self.session.get_user_id(request.sid)

        fmt = params.get('format', 'plain')
        doc_id = uuid4()
        resource_id = uuid4()

        doc = Document(id=doc_id,
                       body=params['body'],
                       format=fmt)

        res = Resource(id=resource_id,
                       name=params['name'],
                       user_id=user_id,
                       resource_id=doc_id,
                       type="document")

        sess = get_session('app')
        sess.add(doc)
        sess.add(res)
        sess.commit()
        return response_ok({
            'id': str(res.id)
        })


    def create_query (self, request):
        params = request.params
        user_id = self.session.get_user_id(request.sid)

        gc = params.get('geo_column', None)

        query_id = uuid4()
        resource_id = uuid4()

        query = Query(id=query_id,
                       body=params['body'],
                       description=params['description'],
                       geo_column=gc)
        res = Resource(id=resource_id,
                       name=params['name'],
                       user_id=user_id,
                       resource_id=query_id,
                       type='query')

        sess = get_session('app')
        sess.add(query)
        sess.add(res)
        sess.commit()
        return response_ok({
            'id': str(res.id)
        })


    def create_plot (self, request):
        params = request.params
        user_id = self.session.get_user_id(request.sid)
        plot_id = uuid4()
        resource_id = uuid4()
        logger.debug('create_plot {}'.format(params))
        capakey = params.pop('capakey')
        geom_text = wkt.dumps(params.pop('geom'))
        geom = WKTElement(geom_text, srid=31370)

        plot = Plot(id=plot_id, capakey=capakey, geom=geom)
        res = Resource(id=resource_id,
                       name=capakey,
                       user_id=user_id,
                       resource_id=plot_id,
                       type='plot')

        sess = get_session('app')
        sess.add(plot)
        sess.add(res)
        sess.commit()
        return response_ok({
            'id': str(res.id)
        })

    def create_media(self, request):
        params = request.params
        user_id = self.session.get_user_id(request.sid)

        name = Path(params['path']).name
        media_id = uuid4()
        resource_id = uuid4()

        media = Media(id=media_id,
                       mimetype=params['mimetype'],
                       path=params['path'])

        res = Resource(id=resource_id,
                       name=name,
                       user_id=user_id,
                       resource_id=media_id,
                       type="media")

        sess = get_session('app')
        sess.add(media)
        sess.add(res)
        sess.commit()
        return response_ok({
            'id': str(res.id)
        })

    def update_resource (self, request):
        params = deepcopy(request.params)
        user_id = self.session.get_user_id(request.sid)
        rid = params.pop('id')
        name = params.pop('name')

        sess = get_session('app')

        res_ptr = sess.query(Resource).get(rid)
        res = sess.query(RESOURCE_MAP[res_ptr.type]).get(res_ptr.resource_id)

        res_ptr.name = name
        for key in params:
            setattr(res, key, params[key])

        sess.commit()

        return response_ok({
            'id': str(res_ptr.id)
        })


    def delete_resource (self, request):
        params = request.params
        user_id = UUID(self.session.get_user_id(request.sid))
        rid = params.get('id')

        sess = get_session('app')
        res_ptr = sess.query(Resource).get(rid)
        if res_ptr.user_id != user_id:
            raise Exception('Not the owner of the resource')

        resource = sess.query(RESOURCE_MAP[res_ptr.type]).get(res_ptr.resource_id)
        sess.query(Link)\
            .filter((Link.source_id == rid) | (Link.target_id == rid))\
            .delete(synchronize_session=False)

        sess.delete(resource)
        sess.delete(res_ptr)
        sess.commit()

        return response_ok({
            'deleted': rid
        })


    def list(self, request):
        params = request.params
        keys = None
        if 'keys' in params:
            keys = params['keys']

        uid = UUID(params['user'])
        sess = get_session('app')

        types = [
            ('document', Document),
            ('media', Media),
            ('query', Query),
            ('plot', Plot),
        ]
        data = []

        for t, K in types:
            query = sess.query(Resource)\
                .with_labels()\
                .add_entity(K)\
                .join(K, Resource.resource_id == K.id)\
                .filter(Resource.user_id == uid)

            for r, k in query:
                try:
                    data.append( getattr(ResourceFormat, t)(r, k, keys) )
                except Exception as ex:
                    logger.info('list:resource_format failed [{}]'.format(ex))
                    pass
        # import pdb; pdb.set_trace()
        data.sort(key=lambda x: x['ts'], reverse=True)
        return response_ok(data)

    def list_all(self, request):
        params = request.params
        keys = None
        if 'keys' in params:
            keys = params['keys']

        model = params['model']
        sess = get_session('app')

        types = {
            'document': Document,
            'media': Media,
            'query': Query,
            'plot': Plot,
        }

        data = []
        K = types[model]

        query = sess.query(Resource)\
            .with_labels()\
            .add_entity(K)\
            .join(K, Resource.resource_id == K.id)\
            .order_by(Resource.ts.asc())

        for r, k in query:
            try:
                data.append( getattr(ResourceFormat, model)(r, k, keys) )
            except Exception as ex:
                logger.info('list_all:resource_format failed [{}]'.format(ex))
                pass

        return response_ok(data)


    def get(self, request):
        params = request.params
        sess = get_session('app')
        res = sess.query(Resource).get(params['id'])
        resource_type = res.type

        return getattr(ResourceResponse, resource_type)(res)
