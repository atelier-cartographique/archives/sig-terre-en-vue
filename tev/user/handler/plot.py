# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from ..models import Resource, User, Role, Plot, PlotElement
from ..session import SessionManager
from ...db.db import get_session
from ...psock.proto import response_ok
from .format import ResourceFormat

from uuid import UUID, uuid4

import logging
logger = logging.getLogger('tev')


def format_element (elem):
    return dict(
        id = str(elem.id),
        ts = int(elem.ts.timestamp() * 1000),
        plot_id = str(elem.plot_id),
        user_id = str(elem.user_id),
        question_code = elem.question_code,
        answer_code = elem.answer_code
    )


class PlotHandler:

    def __init__ (self):
        self.session = SessionManager()


    def create_element (self, request):
        params = request.params
        user_id = self.session.get_user_id(request.sid)
        if not user_id:
            raise Exception('NotLogged')

        plot_id = params['plot_id']
        question_code = params['question_code']
        answer_code = params['answer_code']
        db_session = get_session('app')

        pe = PlotElement(
            user_id = user_id,
            plot_id = plot_id,
            question_code = question_code,
            answer_code = answer_code
        )
        db_session.add(pe)
        db_session.commit()
        return response_ok(format_element(pe))

    def delete_element (self, request):
        params = request.params
        user_id = self.session.get_user_id(request.sid)
        if not user_id:
            raise Exception('NotLogged')

        element_id = params['element_id']
        db_session = get_session('app')
        element = db_session.query(PlotElement).get(element_id)
        if str(element.user_id) != user_id:
            raise Exception('NotTheOwner')
        db_session.delete(element)
        db_session.commit()
        return response_ok({'deleted': element_id})


    def list_element (self, request):
        params = request.params
        pid = UUID(params['pid'])
        db_session = get_session('app')

        elements = db_session.query(PlotElement)\
                             .filter(PlotElement.plot_id == pid)\
                             .order_by(PlotElement.ts.asc())

        return response_ok([format_element(elem) for elem in elements])
