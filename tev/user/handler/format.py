# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from ..models import User, Resource, Document, Media, Query, Plot
from ..session import SessionManager
from ...db.db import get_session
from ...psock.proto import response_ok, response_error
from uuid import UUID, uuid4
from copy import deepcopy

import logging
logger = logging.getLogger('tev')

from ...config import Config
config = Config.instance()


def prepare_result (resource):
    return dict(
        id=str(resource.id),
        ts=int(resource.ts.timestamp() * 1000),
        name=resource.name,
        type=resource.type,
        user_id=str(resource.user_id)
    )

class ResourceFormat ():

    @staticmethod
    def document (resource, doc, keys=None):
        ks = keys or ['body', 'format']
        result = prepare_result(resource)
        for k in ks:
            try:
                result[k] = getattr(doc, k)
            except Exception:
                pass
        return result

    @staticmethod
    def query (resource, q, keys=None):
        result = prepare_result(resource)
        result['geo_column'] = q.geo_column
        result['description'] = q.description
        result['body'] = q.body
        return result

    @staticmethod
    def plot (resource, plot, keys=None):
        result = prepare_result(resource)
        result['capakey'] = plot.capakey
        return result

    @staticmethod
    def media (resource, m, keys=None):
        result = prepare_result(resource)
        result['path'] = m.path
        result['mimetype'] = m.mimetype
        result['url'] = config.get('http.upload.base_url') + m.path
        return result
