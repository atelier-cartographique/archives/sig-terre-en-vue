# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from ..models import Resource, Query, Link, Document, Media
from ..session import SessionManager
from ...db.db import get_session
from ...psock.proto import response_ok
from .format import ResourceFormat

from uuid import UUID, uuid4

import logging
logger = logging.getLogger('tev')

class ResourceNotOwned(Exception):
    pass

class LinkExists(Exception):
    pass

class LinkHandler:

    def __init__ (self):
        self.session = SessionManager()


    def create_link (self, request):
        params = request.params
        user_id = self.session.get_user_id(request.sid)
        db_session = get_session('app')
        source_id = params['source']
        target_id = params['target']

        source_ptr = db_session.query(Resource).get(source_id)
        target_ptr = db_session.query(Resource).get(target_id)

        logger.debug('Link source({}) target({}) user({})'.format(
            source_id,
            target_id,
            user_id
        ))

        if user_id != str(source_ptr.user_id):
            raise ResourceNotOwned('{} IsNot {}'.format(
                source_ptr.user_id,
                user_id
                ))

        link_exists = (db_session.query(Link)
                       .filter(Link.source_id == source_id,
                               Link.target_id == target_id)
                       .count()) > 0
        if link_exists:
            raise LinkExists('LinkExists[{}->{}]'.format(source_id, target_id))

        link = Link(
            source_id = source_ptr.id,
            target_id = target_ptr.id
        )

        db_session.add(link)
        db_session.commit()
        return response_ok({
            'id': str(link.id),
            'target_id': target_id,
            'source_id': source_id,
        })

    def delete_link (self, request):
        params = request.params
        user_id = self.session.get_user_id(request.sid)
        db_session = get_session('app')
        source_id = params['source']
        target_id = params['target']

        logger.debug('Unlink source({}) target({}) user({})'.format(
            source_id,
            target_id,
            user_id
        ))

        source_ptr = db_session.query(Resource).get(source_id)

        if user_id != str(source_ptr.user_id):
            raise ResourceNotOwned('{} IsNot {}'.format(
                source_ptr.user_id,
                user_id
                ))

        link =  (db_session.query(Link)
                       .filter(Link.source_id == source_id,
                               Link.target_id == target_id)
                       .one())
        link_id = link.id
        db_session.delete(link)
        db_session.commit()
        return response_ok({
            'id': str(link_id)
        })

    def list (self, request):
        params = request.params
        rid = UUID(params['target'])
        db_session = get_session('app')

        resources = db_session.query(Resource)\
                             .with_labels()\
                             .add_entity(Link)\
                             .join(Link, Resource.id == Link.source_id)\
                             .filter(Link.target_id == rid)\
                             .order_by(Link.ts.asc())

        types = {
            'document': Document,
            'media': Media,
            'query': Query,
        }

        data = []
        for r, l in resources:
            try:
                resource = db_session.query(types[r.type]).get(r.resource_id)
                data.append( getattr(ResourceFormat, r.type)(r, resource) )
            except Exception as ex:
                logger.info('link:list:resource_format failed [{}]'.format(ex))
                pass


        return response_ok(data)

    def exists (self, request):
        params = request.params
        source_id = UUID(params['source'])
        target_id = UUID(params['target'])
        db_session = get_session('app')

        link_exists = (db_session.query(Link)
                       .filter(Link.source_id == source_id,
                               Link.target_id == target_id)
                       .count()) > 0

        return response_ok(dict(result=link_exists))
