# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import redis
import json
from uuid import UUID, uuid4

from .models import User
from ..db.db import get_session

import logging
logger = logging.getLogger('tev')

from ..config import Config
config = Config.instance().get_config('user.session')

connection_pool = redis.ConnectionPool(
        host=config.get('redis.host', '127.0.0.1'),
        port=config.get('redis.port', 6379),
        db=config.get('redis.db', 0)
    )

PREFIX = 'tev_sess.'
PREFIX_LEN = len(PREFIX)

def prefix (k):
    return PREFIX + str(k)

def un_prefix (k):
    return str(k)[PREFIX_LEN:]

class SessionManager:

    timeout = config.get('timeout', 3600)

    def start(self, uid):
        timeout = self.timeout * 1000
        sid = uuid4()
        conn = redis.Redis(connection_pool=connection_pool)
        logger.debug('session.start {} {} {}'.format(timeout, prefix(sid), str(uid)))
        conn.psetex(prefix(sid), timeout, str(uid))
        return sid

    def stop(self, sid):
        conn = redis.Redis(connection_pool=connection_pool)
        conn.delete(prefix(sid))

    def get_user_id(self, sid):
        conn = redis.Redis(connection_pool=connection_pool)
        uid = conn.get(prefix(sid))
        try:
            return uid.decode()
        except Exception:
            return None

    def get_user(self, sid):
        uid = self.get_user_id(sid)
        db_sess = get_session('app')
        return db_sess.query(User).get(str(uid))
