# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



from sqlalchemy import Column, Integer, Text, String, Enum
from sqlalchemy_utils import PasswordType, UUIDType, JSONType
from geoalchemy2 import Geometry

import enum

from ..db.sa import Base

# class Role(enum.Enum):
#     admin = 'admin'
#     staff = 'staff'
#     embassador = 'embassador'
#     no_role = 'no_role'
#

Role = Enum(
    'admin',
    'staff',
    'embassador',
    'no_role',
    name='user_role'
    )

class Auth(Base):
    __tablename__ = 'user_auth'
    email = Column(String)
    password = Column(PasswordType(schemes=[ 'pbkdf2_sha512', 'md5_crypt' ], deprecated=['md5_crypt']))
    # role = Column(Integer)

class User(Base):
    __tablename__ = 'user_user'
    auth_id = Column(UUIDType)
    name = Column(String)
    role = Column(Role)

class Resource(Base):
    __tablename__ = 'user_resource'
    user_id = Column(UUIDType)
    name = Column(String)
    resource_id = Column(UUIDType)
    type = Column(String)

class Document(Base):
    __tablename__ = 'user_document'
    body = Column(Text)
    format = Column(String)

class Media(Base):
    __tablename__ = 'user_media'
    mimetype = Column(String)
    path = Column(String)

class Query(Base):
    __tablename__ = 'user_query'
    geo_column = Column(String)
    body = Column(Text)
    description = Column(Text)

class Plot(Base):
    __tablename__ = 'user_plot'
    capakey = Column(String)
    geom = Column(Geometry('POLYGON', srid=31370))

class PlotElement(Base):
    __tablename__ = "user_plot_element"
    user_id = Column(UUIDType)
    plot_id = Column(UUIDType)
    question_code = Column(String)
    answer_code = Column(String)

class Link(Base):
    __tablename__ = 'user_link'
    source_id = Column(UUIDType)
    target_id = Column(UUIDType)
