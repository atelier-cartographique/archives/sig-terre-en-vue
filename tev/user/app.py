# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import asyncio
from zmq.asyncio import install

from ..db import db; db.configure();
from ..config import Config
from ..psock.server import Server
from ..psock.dispatch import Dispatch
from .handler.user import UserHandler
from .handler.resource import ResourceHandler
from .handler.link import LinkHandler
from .handler.plot import PlotHandler

import logging
logger = logging.getLogger('tev')

import trace
tracer = trace.Trace(count=0, trace=1)

def start ():
    loop = install()
    config = Config.instance()

    dispatch = Dispatch('user')
    dispatch.add('user', UserHandler())
    dispatch.add('resource', ResourceHandler())
    dispatch.add('link', LinkHandler())
    dispatch.add('plot', PlotHandler())

    broker_connect = '{connect}:{port}'.format(**config.get('broker'))
    server = Server(broker_connect, dispatch)

    def ss ():
        server.start()

    # tracer.runfunc(ss)
    ss()

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        tasks = asyncio.Task.all_tasks(loop)
        for task in tasks:
            task.cancel()
        print('shutting down user')
    loop.close()
