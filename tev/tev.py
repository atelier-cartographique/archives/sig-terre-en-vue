# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import click
import asyncio

import logging; logging.basicConfig()
logger = logging.getLogger('tev')

from .config import Config


@click.group()
@click.option('--debug/--no-debug', default=False)
@click.pass_context
def commander(ctx, debug):
    if debug:
        logger.setLevel(logging.DEBUG)
        click.echo('Debug mode is %s' % ('on' if debug else 'off'))

    ctx.obj = dict()
    ctx.obj['config'] = Config.instance()



def start_service(service):
    if 'user' == service:
        from .user import app as user_app
        user_app.start()
    elif 'analysis' == service:
        from .analysis import app as analysis_app
        analysis_app.start()
    elif 'http' == service:
        from .http import app as http_app
        http_app.start()
    elif 'tile' == service:
        from .tile import app as tile_app
        tile_app.start()
    elif 'broker' == service:
        from .broker import app as broker_app
        broker_app.start()


@commander.command()
@click.pass_context
@click.argument('service')
def start(ctx, service):
    services = ['user', 'analysis', 'http', 'tile', 'broker']
    try:
        services.index(service)
    except ValueError:
        click.secho('Service [{}] does not exist'.format(service), fg='red')
        click.secho('available services are: {}'.format(services, fg='blue'))
        return
    start_service(service)


@commander.command()
@click.argument('service')
@click.argument('method')
@click.argument('session_id')
@click.option('--param', '-p', type=(str, str), multiple=True)
@click.pass_context
def client(ctx, service, method, session_id, param):
    from zmq.asyncio import install
    import signal
    from functools import partial
    pd = dict()
    for k,v in param:
        if v.startswith('@'):
            fn = v[1:]
            with open(fn) as f:
                pd[k] = f.read()
        else:
            pd[k] = v
    from .client import send

    loop = install()
    try:
        loop.run_until_complete(send(service, method, session_id, pd))
    finally:
        loop.close()


@commander.command()
@click.option('--watch/--no-watch', '-w/-nw', default=False)
@click.argument('source_dir')
@click.argument('target_dir')
def template_gen(watch, source_dir, target_dir):
    from .http import parser
    from pathlib import Path

    tp = parser.TemplateParser()
    sd = Path(source_dir)
    td = Path(target_dir)

    def process_templates ():
        for source_path in sd.glob('*.html'):
            with source_path.open() as f:
                try:
                    t = f.read()
                    results = tp.apply_template(t)
                    for meta, fn in results:
                        tf = td.joinpath(meta['name'] + '.js')
                        with tf.open('w') as sink:
                            sink.write(fn)
                            click.secho('processed {}'.format(meta['name']), fg='green')
                except Exception as ex:
                    click.secho('failed to process {} because of {}'.format(source_path, ex), fg='red')

    process_templates()

    if watch:
        from inotify.adapters import InotifyTree
        click.secho('Watching {}'.format(sd.as_posix()), fg='blue')
        i = InotifyTree(sd.as_posix().encode('utf-8'))
        try:
            for event in i.event_gen():
                if event is not None:
                    (header, type_names, watch_path, filename) = event
                    # print('tn {}'.format(type_names))
                    if 'IN_MODIFY' in type_names:
                        process_templates()
                    # if len(filename) > 0:
                    #     logger.debug("WD=(%d) MASK=(%d) COOKIE=(%d) LEN=(%d) MASK->NAMES=%s "
                    #          "WATCH-PATH=[%s] FILENAME=[%s]",
                    #          header.wd, header.mask, header.cookie, header.len, type_names,
                    #          watch_path.decode('utf-8'), filename.decode('utf-8'))
        except KeyboardInterrupt:
            return


if __name__ == "__main__":
    commander()
