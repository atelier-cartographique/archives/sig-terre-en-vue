# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from aiohttp_jinja2 import render_string
from jinja2 import contextfilter

import logging
logger = logging.getLogger('tev')


class ResourceComponent:

    def url (self, request, resource):
        router = request.app.router['resource']
        return router.url(parts=dict(
                        uid=resource.get('user_id', None),
                        type=resource.get('type', None),
                        rid=resource.get('id')
                        ))

    def template_name (self, request, resource, comp_name):
        return '{}_{}.html'.format(resource['type'], comp_name)

    def format (self, request, resource, comp_name):
        template = self.template_name(request, resource, comp_name)
        return render_string(template, request, dict(
                resource=resource,
                url=self.url(request, resource)
            ))


_rc = ResourceComponent()

@contextfilter
def resource_comp (context, resource, comp_name='link'):
    return _rc.format(context.get('request'), resource, comp_name)
