# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import asyncio
from aiohttp import web
import aiohttp_jinja2
from jinja2 import PackageLoader

from .serve_static import StaticResource
from .index import configure as index
from .api.user import configure as user
from .api.service import configure as service
from .api.extra import configure as extra
from .auth import configure as auth
from .wms import configure as wms
from .rules import configure as rules
from .session import session_middleware
from .processors import processors
# from .filters import configure as configure_filters

import logging
logger = logging.getLogger('tev')

from ..config import Config
config = Config.instance().get_config('http')



@asyncio.coroutine
def log_req_middleware(app, handler):
    @asyncio.coroutine
    def log_req_handler(request):
        logger.info('{}'.format(request.path_qs))
        return (yield from handler(request))
    return log_req_handler

middlewares = [
    log_req_middleware,
    session_middleware,
]

def start ():
    app = web.Application(middlewares=middlewares)
    jinja_env = aiohttp_jinja2.setup(
        app,
        loader=PackageLoader('tev', 'http/templates'),
        context_processors=None,
    )
    # configure_filters(jinja_env)

    router = app.router
    rules(router)
    auth(router)
    user(router)
    service(router)
    extra(router)
    wms(router)
    router.add_route('*', '/static/{resource:.*}', StaticResource)
    index(router)


    loop = asyncio.get_event_loop()
    handler = app.make_handler()
    f = loop.create_server(
            handler,
            config.get('iface', '127.0.0.1'),
            config.get('port', 8080)
    )
    srv = loop.run_until_complete(f)
    print('serving on', srv.sockets[0].getsockname())
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        srv.close()
        loop.run_until_complete(srv.wait_closed())
        loop.run_until_complete(app.shutdown())
        loop.run_until_complete(handler.finish_connections(60.0))
        loop.run_until_complete(app.cleanup())
        print('shutting down http')
    loop.close()
