# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pathlib import Path
from asyncio import Task, coroutine, wait_for, get_event_loop, gather
from aiohttp import web, MultiDict
from pkg_resources import resource_string
import mistune
import filetype
import sys, traceback

from ...psock import proto
from ...psock.client import Client
from ..sanitizer import Sanitizer
from ..session import get_user
from ..multipart import get_file
from ..permissions import (UserPerm, ResourceOwnerPerm,
                          EmbassadorPerm, StaffPerm)

import logging
logger = logging.getLogger('tev')

from ...config import Config
config = Config.instance()


client = None


html_whitelist = (
    'a', 'b', 'blockquote', 'br', 'caption', 'cite', 'code', 'col', 'colgroup', 'dd', 'div', 'dl', 'dt', 'em', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'i', 'img', 'li', 'ol', 'p', 'pre', 'q', 'small', 'span', 'strike', 'strong', 'sub', 'sup', 'table', 'tbody', 'td', 'tfoot', 'th', 'thead', 'tr', 'u', 'ul',
)
html_sanitizer = Sanitizer(html_whitelist)


API_PREFIX = '/api/v1'
API_RESOURCES = {
    'documents': 'document',
    'queries': 'query',
    'plots': 'plot',
    'medias': 'media'
}


def multi_to_dict (m):
    result = dict()
    for key in m.keys():
        result[key] = m.get(key)
    return result


def prepare_swagger (request):
    from yaml import load as load_yaml
    swagger_yaml = resource_string('tev', 'http/api/swagger.yaml')
    swagger = load_yaml(swagger_yaml)
    swagger['host'] = request.host
    swagger['schemes'] = [config.get('http.scheme', request.scheme)]
    return swagger


def get_resource_type (api_res):
    return API_RESOURCES[api_res]


def mk_api_path (p) :
    return API_PREFIX + p

@coroutine
def process_json_request (req, client):
    response = yield from client.get(req)
    if proto.is_ok(response):
        return web.json_response(response.data)
    return web.HTTPInternalServerError(
            text='process_json_request: {}'.format(proto.error_message(response))
        )

@coroutine
def get_me (request):
    me = yield from get_user(request.session_id)
    return web.json_response(me)

@coroutine
def list_user (request):
    req = proto.Request('user', 'user.ls',
                        request.session_id, dict())
    return (yield from process_json_request(req, client))

class User(web.View):

    @coroutine
    def get_user (self, uid):

        # first get the user itself
        req = proto.Request('user', 'user.get',
                            self.request.session_id, dict(id=uid))

        return (yield from process_json_request(req, client))

    @coroutine
    def get (self):
        uid = self.request.match_info['uid']
        return (yield from self.get_user(uid))



class ResourceRender:

    @staticmethod
    @coroutine
    def document(request, doc, links, user_resources, editable):
        try:
            doc['transformed'] = html_sanitizer.process(mistune.markdown(doc['body']))
        except Exception:
            doc['transformed'] = doc['body']

        return doc
        # return web.json_response(doc)

    @staticmethod
    @coroutine
    def query(request, query, links, user_resources, editable):
        return query
        # return web.json_response(query)

    @staticmethod
    @coroutine
    def plot(request, plot, links, user_resources, editable):
        return plot
        # return web.json_response(plot)

    @staticmethod
    @coroutine
    def media(request, media, links, user_resources, editable):
        # logger.debug('media =: {}'.format(media))
        return media
        # return web.json_response(media)

class UserResourceList(web.View):

    @coroutine
    def get (self):
        resource_type = get_resource_type(self.request.match_info['type'])
        uid = self.request.match_info['uid']

        req = proto.Request('user', 'resource.list',
                            self.request.session_id, dict(user=uid))
        response = yield from client.get(req)
        resources = []
        rdr = getattr(ResourceRender, resource_type)

        for rs in response.data:
            if rs.get('type') == resource_type:
                data = yield from rdr(self.request, rs, None, None, None)
                resources.append(data)

        return web.json_response(resources)

    @coroutine
    def post_documents (self, uid, body):

        permission = UserPerm(self.request)
        editable = yield from permission.check()
        if not editable:
            return (yield from permission.deny())

        doc = dict(
            name=body.get('name'),
            body=body.get('body')
        )

        req = proto.Request('user', 'resource.create_document',
                            self.request.session_id, doc)
        return (yield from process_json_request(req, client))


    @coroutine
    def post_queries (self, uid, body):

        permission = StaffPerm(self.request)
        editable = yield from permission.check()
        if not editable:
            return (yield from permission.deny())

        query = dict(
            body=body.get('body'),
            name=body.get('name'),
            description=body.get('description'),
            geo_column=body.get('geo_column', None)
        )
        req = proto.Request('user', 'resource.create_query',
                            self.request.session_id, query)
        return (yield from process_json_request(req, client))

    @coroutine
    def post_plots (self, uid, body):
        # import pdb; pdb.set_trace()
        permission = EmbassadorPerm(self.request)
        editable = yield from permission.check()
        if not editable:
            return (yield from permission.deny())

        capakey = body.get('capakey')
        geometry = None

        req = proto.Request('analysis', 'plot.capakey_geom',
                            self.request.session_id, dict(capakey=capakey))
        response = yield from client.get(req)
        if proto.is_ok(response):
            geometry = response.data
        else:
            logger.error('NO GEOMETRY FOR {}'.format(capakey))

        plot = dict(capakey=capakey, geom=geometry)
        req = proto.Request('user', 'resource.create_plot',
                            self.request.session_id, plot)
        return (yield from process_json_request(req, client))

    @coroutine
    def post_medias (self, uid):
        permission = EmbassadorPerm(self.request)
        editable = yield from permission.check()
        if not editable:
            return (yield from permission.deny())

        meta, reader = yield from get_file(self.request, 'media')
        base_upload_path = Path(config.get('http.upload.dir_path'))
        user_upload_dir = base_upload_path.joinpath(uid)
        try:
            user_upload_dir.mkdir(parents=True)
        except FileExistsError:
            pass
        media_path = Path(uid, meta['filename'])
        upload_path = base_upload_path.joinpath(media_path)
        with upload_path.open('wb') as sink:
            data = yield from reader.read(decode=True)
            if False == filetype.is_image(data):
                return web.HTTPBadRequest(text='NotAnImage')
            ft = filetype.guess(data)
            sink.write(data)

        req = proto.Request('user', 'resource.create_media',
                            self.request.session_id, {
                                'mimetype': ft.mime,
                                'path': media_path.as_posix()
                            })
        return (yield from process_json_request(req, client))


    @coroutine
    def post (self):
        resource_type = self.request.match_info['type']
        uid = self.request.match_info['uid']

        try:
            method_name = 'post_' + resource_type
            method = getattr(self, method_name)
            if 'medias' != resource_type:
                body = yield from self.request.json()
                return (yield from method(uid, body))
            return (yield from method(uid))
        except Exception as ex:
            logger.error('UserResourceList:post {} {}'.format(method_name, ex))
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.debug('=' * 60)
            formatted_lines = traceback.format_exc().splitlines()
            for line in formatted_lines:
                logger.debug(line)
            return web.HTTPInternalServerError()

class UserResourceSingle(web.View):

    @coroutine
    def get_resource (self, uid, rid):
        req = proto.Request('user', 'resource.get',
                            self.request.session_id, dict(id=rid))
        response = yield from client.get(req)
        resource = response.data
        editable = uid == self.request.user_id

        req = proto.Request('user', 'link.list',
                            self.request.session_id, dict(rid=rid))
        link_response = yield from client.get(req)
        links = []
        user_resources = []

        if proto.is_ok(link_response):
            links = link_response.data

        if self.request.user_id:
            req = proto.Request('user', 'resource.list',
                                self.request.session_id,
                                dict(user=self.request.user_id))
            res_response = yield from client.get(req)
            if proto.is_ok(res_response):
                user_resources = res_response.data
            else:
                logger.warning('could not get user resources')


        try:
            rdr = getattr(ResourceRender, resource.get('type'))
        except Exception as ex:
            logger.warning(str(ex))
            return web.HTTPInternalServerError()
        data = yield from rdr(self.request, resource, links, user_resources, editable)
        return web.json_response(data)

    @coroutine
    def get (self):
        uid = self.request.match_info['uid']
        rid = self.request.match_info['rid']
        return (yield from self.get_resource(uid, rid))

    @coroutine
    def put (self):
        uid = self.request.match_info['uid']
        resource_type = self.request.match_info['type']
        rid = self.request.match_info['rid']
        permission = ResourceOwnerPerm(self.request, rid=rid)
        editable = yield from permission.check()
        if not editable:
            return (yield from permission.deny())

        resource = yield from self.request.json()
        try:
            # resource = multi_to_dict(body)
            resource['id'] = rid
            req = proto.Request('user', 'resource.update_resource',
                                self.request.session_id, resource)
            response = yield from client.get(req)
            if proto.is_ok(response):
                return (yield from self.get())
            else:
                logger.error('UserResourceSingle:put {}'.format(response.meta))
                return web.HTTPInternalServerError(text=proto.error_message(response))
        except Exception as ex:
            logger.error('UserResourceSingle:put {}'.format(ex))
            return web.HTTPInternalServerError(text=str(ex))

    @coroutine
    def post (self):
        return (yield from self.put())


class LinkList(web.View):

    @coroutine
    def get (self):
        target = self.request.match_info['tid']
        req = proto.Request('user', 'link.list',
                            self.request.session_id, dict(target=target))

        return (yield from process_json_request(req, client))


    @coroutine
    def post (self):
        uid = self.request.user_id
        target = self.request.match_info['tid']
        source = self.request.match_info['sid']

        permission = ResourceOwnerPerm(self.request, rid=source)
        checked = yield from permission.check()
        if not checked:
            return (yield from permission.deny())

        req = proto.Request('user', 'link.create_link',
                            self.request.session_id, dict(
                                source=source,
                                target=target
                            ))

        return (yield from process_json_request(req, client))


    @coroutine
    def delete(self):
        uid = self.request.user_id
        target = self.request.match_info['tid']
        source = self.request.match_info['sid']

        permission = ResourceOwnerPerm(self.request, rid=source)
        checked = yield from permission.check()
        if not checked:
            return (yield from permission.deny())

        req = proto.Request('user', 'link.delete_link',
                                self.request.session_id, dict(
                                source=source,
                                target=target
                            ))

        return (yield from process_json_request(req, client))

@coroutine
def get_geometry (sid, c):
    req = proto.Request('analysis', 'plot.capakey_geom', sid, c)
    response = yield from client.get(req)
    if proto.is_ok(response):
        c.update(dict(geometry=response.data))
        return c
    return None

@coroutine
def list_plots (request):
    sid = request.session_id
    req = proto.Request('user', 'resource.list_all',
                        sid, dict(model='plot'))
    response = yield from client.get(req)

    if proto.is_ok(response):
        capakeys = response.data
        results = []
        tasks = []


        # for c in capakeys:
        #     logger.debug('capakey {}'.format(c['capakey']))
        #     req = proto.Request('analysis', 'plot.capakey_geom', sid, c)
        #     response = yield from client.get(req)
        #     if proto.is_ok(response):
        #         c.update(dict(geometry=response.data))
        #         results.append(c)


        loop = get_event_loop()
        for c in capakeys:
            tasks.append(loop.create_task(get_geometry(sid, c)))
        results = [r for r in (yield from gather(*tasks)) if r]

        return web.json_response(results)

    return web.HTTPInternalServerError(
        text=proto.error_message(response)
    )


@coroutine
def list_plot_elements (request):
    sid = request.session_id
    uid = request.match_info['uid']
    rid = request.match_info['rid']
    req = proto.Request('user', 'plot.list_element',
                        sid, dict(pid=rid))

    return (yield from process_json_request(req, client))

@coroutine
def create_plot_element (request):
    sid = request.session_id
    uid = request.match_info['uid']
    rid = request.match_info['rid']
    params = dict(plot_id=rid)
    body = yield from request.json()

    params.update(body)
    req = proto.Request('user', 'plot.create_element',
                        sid, params)

    return (yield from process_json_request(req, client))

@coroutine
def delete_plot_element (request):
    sid = request.session_id
    eid = request.match_info['eid']
    params = dict(element_id=eid)
    req = proto.Request('user', 'plot.delete_element',
                        sid, params)

    return (yield from process_json_request(req, client))


def configure(router):
    global client
    client = Client()

    router.add_route('GET', mk_api_path('/users'),
                     list_user)
    router.add_route('GET', mk_api_path('/users/me'),
                     get_me)

    router.add_route('GET', mk_api_path('/resources/all/plots'),
                     list_plots)
    router.add_route('POST', mk_api_path('/users/{uid}/plots/{rid}'),
                     create_plot_element)
    router.add_route('GET', mk_api_path('/users/{uid}/plots/{rid}/elements'),
                     list_plot_elements)
    router.add_route('DELETE', mk_api_path('/users/{uid}/plots/{rid}/elements/{eid}'),
                     delete_plot_element)


    user_item = router.add_resource(mk_api_path('/users/{uid}'),
                                    name='user')
    res_list = router.add_resource(mk_api_path('/users/{uid}/{type}'),
                                   name='resources')
    res_item = router.add_resource(mk_api_path('/users/{uid}/{type}/{rid}'),
                                   name='resource')
    resource_get = router.add_resource(mk_api_path('/resources/{rid}'),
                                   name='resource_get')
    link_list = router.add_resource(mk_api_path('/resources/{tid}/links'),
                                    name='links')
    link_manip = router.add_resource(mk_api_path('/resources/{tid}/links/{sid}'),
                                     name='link_del')


    user_item.add_route('*', User)
    res_list.add_route('*', UserResourceList)
    res_item.add_route('*', UserResourceSingle)
    resource_get.add_route('*', UserResourceSingle)

    link_list.add_route('GET', LinkList)
    link_manip.add_route('POST', LinkList)
    link_manip.add_route('DELETE', LinkList)

    #swagger_data = prepare_swagger()
    def swagger (req):
        return web.json_response(prepare_swagger(req))

    router.add_route('GET', mk_api_path('/swagger.json'), swagger)
