# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from asyncio import Task, coroutine, wait_for
from aiohttp import web, MultiDict
from pkg_resources import resource_string

from ...psock import proto
from ...psock.client import Client
from ..permissions import (UserPerm, ResourceOwnerPerm,
                          EmbassadorPerm, StaffPerm)

import logging
logger = logging.getLogger('tev')

from ...config import Config
config = Config.instance()

client = None

API_PREFIX = '/api/v1'

def mk_api_path (p) :
    return API_PREFIX + p


@coroutine
def hit(sid, body):
    req = proto.Request('analysis', 'plot.hit', sid, body)
    response = yield from client.get(req)
    if proto.is_ok(response):
        return web.json_response(response.data)

    return web.HTTPInternalServerError(text=response.meta['error'])

@coroutine
def capakey(sid, body):
    req = proto.Request('analysis', 'plot.capakey', sid, body)
    response = yield from client.get(req)
    if proto.is_ok(response):
        return web.json_response(response.data)

    return web.HTTPInternalServerError(text=response.meta['error'])

@coroutine
def execQuery (sid, body):
    req = proto.Request('analysis', 'query.qexec', sid, body)
    response = yield from client.get(req)
    if proto.is_ok(response):
        return web.json_response(response.data)

    return web.HTTPInternalServerError(text=response.meta['error'])


SERVICES = {
    'hit' : hit,
    'capakey': capakey,
    'query': execQuery
}

class Service(web.View):

    @coroutine
    def post (self):
        service_name = self.request.match_info['name']
        body = yield from self.request.json()
        return (yield from SERVICES[service_name](self.request.session_id, body))


def configure(router):
    global client
    client = Client()

    service = router.add_resource(
        mk_api_path('/service/{name}'),
        name='service'
    )
    service.add_route('*', Service)
