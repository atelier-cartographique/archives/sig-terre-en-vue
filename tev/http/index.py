
# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from asyncio import Task, coroutine, wait_for
from aiohttp import web, MultiDict
from aiohttp_jinja2 import render_template

from ..psock import proto
from ..psock.client import Client

import logging
logger = logging.getLogger('tev')

from ..config import Config
config = Config.instance()

client_user = None


def list_user (request):
    req = proto.Request('user', 'user.ls',
                        request.session_id, dict())
    response = yield from client_user.get(req)
    if proto.is_ok(response):
        return response.data
    return []


@coroutine
def index (request):
    """Home page
    """
    data = dict()
    # data = list_user(request)
    return render_template('index.html', request, data)

def configure(router):
    global client_user
    client_user = Client()
    router.add_route('GET', r'/{path:.*}', index)
