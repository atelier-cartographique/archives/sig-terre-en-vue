# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math
from base64 import b64decode
from asyncio import Task, coroutine, wait_for
from aiohttp import web, MultiDict
from aiohttp_jinja2 import render_template
from osgeo import osr

from ..psock import proto
from ..psock.client import Client
from ..tile.lingua import LinguaWMS, BoundingBox

import logging
logger = logging.getLogger('tev')

from ..config import Config
config = Config.instance()
config_tile = config.get('tile')

client = None

def tile_context (request, options=dict()):
    base = {
        'wms_url': request.app.router['wms'].url(),
        'version': config_tile.get('version'),
        'title': config_tile.get('title'),
        'mime': config_tile.get('mime')
    }
    base.update(options)
    return base

def get_params (request, params):
    ret = []
    for param in params:
        ret.append(request.match_info[param])
    return ret;

ex_geo_srs = osr.SpatialReference()
ex_geo_srs.SetWellKnownGeogCS('CRS:84')

EPSG = dict()

def ex_geo (srs, bbox):
    if srs in EPSG:
        source_srs = EPSG.get(srs)
    else:
        code = int(srs.split(':')[1])
        source_srs = osr.SpatialReference()
        source_srs.ImportFromEPSG(code)
        EPSG[srs] = source_srs
    ct = osr.CreateCoordinateTransformation(source_srs, ex_geo_srs)
    minx, miny, z = ct.TransformPoint(bbox.minx, bbox.miny)
    maxx, maxy, z = ct.TransformPoint(bbox.maxx, bbox.maxy)
    return BoundingBox(minx, miny, maxx, maxy)


class WMS(web.View):

    cap_req_parms_m = [
        LinguaWMS.Request.service,
        # LinguaWMS.Request.request
    ]
    cap_req_parms_o = [
        LinguaWMS.Request.version,
        LinguaWMS.Request.format,
        LinguaWMS.Request.updatesequence
    ]
    map_req_parms_m = [
        LinguaWMS.Request.version,
        # LinguaWMS.Request.request,
        LinguaWMS.Request.layers,
        LinguaWMS.Request.styles,
        LinguaWMS.Request.crs,
        LinguaWMS.Request.bbox,
        LinguaWMS.Request.width,
        LinguaWMS.Request.height
    ]
    map_req_parms_o = [
        LinguaWMS.Request.transparent,
        LinguaWMS.Request.bgcolor,
        LinguaWMS.Request.exceptions,
        LinguaWMS.Request.time,
        LinguaWMS.Request.elevation
    ]

    @coroutine
    def get_capabilities (self):
        request = self.request
        query = request.GET
        params = LinguaWMS.Request.gets(query, WMS.cap_req_parms_m)
        params.update(
            LinguaWMS.Request.gets(query, WMS.cap_req_parms_o, False)
        )

        ctx = {
            'wms_url': request.app.router['wms'].url(),
            'version': config_tile.get('version'),
            'title': config_tile.get('title'),
            'mime': config_tile.get('mime')
        }
        layers = []
        for layer in config_tile.get('layers'):
            srs = layer.get('srs')
            bbox = BoundingBox._make(layer.get('bbox'))
            geo = ex_geo(srs, bbox)
            layers.append({
                'name': layer.get('name'),
                'title': layer.get('title'),
                'srs': srs,
                'bbox': bbox,
                'geo': geo
            })
        ctx['layers'] = layers
        # logger.debug('WMS:GetCapabilities {}'.format(ctx))
        response = render_template('wms_capabilities.xml', request, ctx)
        response.content_type = 'application/xml'
        return response

    @coroutine
    def get_map (self):
        request = self.request
        query = request.GET
        params = LinguaWMS.Request.gets(query, WMS.map_req_parms_m)
        params.update(
            LinguaWMS.Request.gets(query, WMS.map_req_parms_o, False)
        )

        logger.debug(params)
        tile_req = proto.Request('tile', 'tile.get_tile',
                            request.session_id, params)

        tile_response = yield from client.get(tile_req)
        if proto.is_ok(tile_response):
            data = tile_response.data
            if 'png' in data:
                pack = b64decode(data['png'].encode())
                headers = MultiDict({
                    # 'CONTENT-DISPOSITION': 'attachment; filename="{}-{}-{}.png"'
                })
                response = web.StreamResponse(headers=headers)
                response.content_type = config_tile.get('mime')
                response.content_length = len(pack)
                yield from response.prepare(request)
                response.write(pack)
                return response
        return web.HTTPInternalServerError(
                text=proto.error_message(tile_response)
            )


    @coroutine
    def get (self):
        wms_req = LinguaWMS.Request.get(self.request.GET,
                                        LinguaWMS.Request.request)

        if LinguaWMS.Token.GetCapabilities == wms_req:
            return (yield from self.get_capabilities())
        elif LinguaWMS.Token.GetMap == wms_req:
            return (yield from self.get_map())

        return web.HTTPBadRequest(text=wms_req)


def configure(router):
    global client
    client = Client()

    wms = router.add_resource('/wms', name='wms')
    wms.add_route('*', WMS)
