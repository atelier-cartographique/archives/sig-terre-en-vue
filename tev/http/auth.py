# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from asyncio import Task, coroutine, wait_for
from aiohttp import web
from aiohttp_jinja2 import render_template

from ..psock import proto
from ..psock.client import Client

import logging
logger = logging.getLogger('tev')

from ..config import Config
config = Config.instance()

client = None


class LoginView(web.View):

    @coroutine
    def get (self):
        return render_template('login.html', self.request,
                                dict())

    @coroutine
    def post (self):
        body = yield from self.request.post()
        req = proto.Request('user', 'user.login',
                            self.request.session_id,
                            dict(email=body.get('email'), password=body.get('password')))
        res = yield from client.get(req)

        if proto.is_ok(res):
            data = res.data
            logger.debug('login good', data)
            # response = render_template('index.html', self.request, dict(user=data))
            response = web.HTTPSeeOther('/')
            response.set_cookie('sid', data['sid'], max_age=data['timeout'])
            return response

        return render_template('login.html', self.request,
                                res.meta)


@coroutine
def logout (request):
    req = proto.Request('user', 'user.logout',
                        request.session_id, dict())
    response = yield from client.get(req)
    if proto.is_ok(response):
        return web.HTTPFound('/')

    return web.HTTPInternalServerError(text=proto.error_message(response))

def configure(router):
    global client
    client = Client()

    router.add_route('*', '/login', LoginView)
    router.add_route('GET', '/logout', logout)
