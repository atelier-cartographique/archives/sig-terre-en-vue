#!/bin/bash
#

for family in *
do
    for weight in ${family}/*
    do
        for style in ${weight}/*
        do
            ftn=$(ls ${style}/*.otf)
            fn=$(basename ${ftn} .otf)
            weight_name=$(basename ${weight})
            style_name=$(basename ${style})
            echo "@include font-face('${fn}', '${family}', '${weight_name}', '${style_name}');";

        done
    done

done
