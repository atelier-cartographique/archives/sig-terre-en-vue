# Copyright (C) 2016  Pierre Marchand <pierremarc@oep-h.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from html import escape as html_escape
from html.parser import HTMLParser
from html.entities import html5
from uuid import UUID, uuid4


PROTOTYPE_TEMPLATE = """

// {name} fragment mixin

var  {name} = Base => class extends Base {{


names () {{
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
{anchors}
//> end of generated code
    return names;
}};


build () {{
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
{builder}
//> end of generated code

    Object.defineProperty(this, 'clear', {{
        value: function () {{
            elements.forEach((element) => {{
                if (element && element.parentNode) {{
                    element.parentNode.removeChild(element);
                }}
            }})
        }}
    }})
    return this;
}};

}}

export default {name};

"""

def escape (s):
    return s.replace('"', '\\"')

name_count = 0
def var_name ():
    global name_count
    name_count += 1
    return 'n{}'.format(name_count)

def create_element (name, tag, attrs=None):
    result = '\nlet {} = document.createElement("{}");'.format(name, tag)
    if attrs:
        for attr in attrs:
            k, v = attr
            result += '\n    {}.setAttribute("{}", "{}");'.format(name, k, v)
    result += '\nelements.push({})'.format(name)
    return result;


def make_anchor (anchor_name, elem_name):
    return 'anchors["{}"] = {};'.format(anchor_name, elem_name)

def make_anchor_name (anchor_name):
    return 'names.push("{}");'.format(anchor_name)

def append_element(name, parent):
    return '{}.appendChild({});'.format(parent, name)


def append_text (parent, data):
    return '{}.appendChild(document.createTextNode("{}"));'.format(
                                                        parent, escape(data))


def get_attribute (attrs, name):
    for k, v in attrs:
        if name == k:
            return v
    raise KeyError(name)

class TemplateParserError(Exception):
    pass

class TemplateParser(HTMLParser):

    def __init__ (self):
        HTMLParser.__init__(self, convert_charrefs=True)


    def add_current_node (self, name):
        self.parents.append(name)

    def pop_current_node (self):
        try:
            return self.parents.pop()
        except IndexError:
            raise TemplateParserError('popin too far a parent')

    def get_current_node (self):
        try:
            return self.parents[-1]
        except IndexError:
            raise TemplateParserError('getin too far a parent')

    def start_one (self):
        self.meta = dict()
        self.name = None
        self.result = []
        self.parents = ['root']
        self.anchors = []
        self.started = True

    def end_one (self):
        builder = '\n'.join(self.result)
        anchors = '\n'.join(self.anchors)
        fragment_js = PROTOTYPE_TEMPLATE.format(
                        name=self.name,
                        builder=builder,
                        anchors=anchors
                    )

        self.started = False
        self.results.append((self.meta, fragment_js,));

    def handle_starttag(self, tag, attrs):
        if self.started:
            name = var_name()
            parent = self.get_current_node()

            elem = create_element(name, tag, attrs)
            self.result.append(elem)
            self.result.append(append_element(name, parent))
            self.add_current_node(name)

            try:
                anchor = get_attribute(attrs, 'data-anchor')
                self.result.append(make_anchor(anchor, self.get_current_node()))
                self.anchors.append(make_anchor_name(anchor))
            except KeyError:
                pass

        elif 'template' == tag:
            self.start_one()

            for k, v in attrs:
                self.meta[k] = v
            try:
                self.name = self.meta['name']
            except KeyError:
                try:
                    self.name = self.meta['id']
                    self.meta['name'] = self.name
                except KeyError:
                    raise Exception('MissingTemplateIdentifier (id or name)')

    def handle_endtag(self, tag):
        if self.started:
            self.pop_current_node()
            if 'template' == tag:
                self.end_one()

    def handle_data(self, data):
        if self.started:
            lines = data.split('\n')
            for line in lines:
                if len(line) > 0:
                    self.result.append(append_text(self.get_current_node(), line))

    def handle_comment(self, data):
        if self.started:
            lines = data.split('\n')
            self.result.append('/**')
            for line in lines:
                self.result.append(' * {}'.format(line))
            self.result.append(' */')

    def handle_entityref(self, name):
        if self.started:
            self.result.append(append_text(html5[name]))

    def handle_decl(self, data):
        # self.result.append('<!{}>'.format(data))
        pass

    def apply_template (self, template):
        self.reset()
        self.started = False
        self.results = [];
        self.feed(template)

        if 0 == len(self.results):
            raise Exception('EmptyTemplate')

        return self.results
