# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from asyncio import coroutine
from aiohttp import web
from .permission import Rule
from .session import get_user
from ..user.models import Role
from ..psock import proto
from ..psock.client import Client

from ..config import Config
config = Config.instance()

client = None

class BaseRule(Rule):

    def __init__ (self, request, **kwargs):
        self.request = request
        self.options = kwargs
        self._user = None
        Rule.__init__(self)

    @coroutine
    def user (self):
        if not self._user:
            self._user = yield from get_user(self.request.session_id)
        return self._user

    @coroutine
    def deny (self):
        return web.HTTPForbidden(text='Permission Denied')

class UserRule(BaseRule):

    @coroutine
    def check (self):
        user = yield from self.user()
        return user


class AdminRule(BaseRule):

    def base (self):
        return UserRule(self.request)

    @coroutine
    def check (self):
        user = yield from self.user()
        return user and ('admin' == user['role'])


class StaffRule(BaseRule):

    def base (self):
        return UserRule(self.request)

    @coroutine
    def check (self):
        user = yield from self.user()
        return user and (('admin' == user['role']) or ('staff' == user['role']))


class EmbassadorRule(BaseRule):

    def base (self):
        return UserRule(self.request)

    @coroutine
    def check (self):
        user = yield from self.user()

        return user and (('admin' == user['role']) or ('staff' == user['role']) or ('embassador' == user['role']))

class ResourceOwnerRule(BaseRule):

    def base (self):
        return UserRule(self.request)

    @coroutine
    def check (self):
        user = yield from self.user()
        rid = self.options.get('rid')
        req = proto.Request('user', 'resource.get',
                            self.request.session_id, dict(id=rid))
        response = yield from client.get(req)
        resource = response.data
        return (user['id'] == self.request.user_id)


def configure(router):
    global client
    client = Client()
