# Copyright (c) 2015 Zhipeng Liu
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from functools import wraps
from asyncio import coroutine

class PermissionDeniedException(RuntimeError):
    """Permission denied to the resource."""


class Permission:
    def __init__(self):
        rule = self.rule()
        if not rule:
            raise AttributeError()
        self.rule = rule

    def __call__(self, func):
        """Give Permission instance the ability to be used as view decorator."""

        @wraps(func)
        @coroutine
        def decorator(*args, **kwargs):
            checked = yield from self.check()
            if not checked:
                return (yield from self.deny())
            return (yield from func(*args, **kwargs))

        return decorator

    @coroutine
    def __enter__(self):
        """Enter the runtime context checking whether there is enough
        permission for this.

        This is a suplimentary method for with-statement."""
        checked = yield from self.check()
        if not checked:
            try:
                yield from self.deny()
            except Exception as e:
                raise e
            else:
                raise PermissionDeniedException()

    def __exit__(self, exc_type, exc_value, traceback):
        """Exit the runtime context.

        This is a suplimentary method for with-statement."""
        pass

    def rule(self):
        """Add rule to this permission.

        Must be overrided."""
        raise NotImplementedError

    @coroutine
    def check(self):
        """Check rule."""
        result, self.deny = yield from self.rule.run()
        return result

    def show(self):
        """Show the structure of rule, only for debug."""
        self.rule.show()


class Rule:
    def __init__(self):
        self.rules_list = [[(self.check, self.deny)]]
        # if subclass override base(), serial merge the return rule's
        # rules_list to self.rules_list.
        base_rule = self.base()
        if base_rule:
            self.rules_list = Rule._and(base_rule.rules_list, self.rules_list)

    def __and__(self, other):
        """& bitwise operation.

        Serial merge self.rules_list to other.rules_list and return self.
        """
        self.rules_list = Rule._and(self.rules_list, other.rules_list)
        return self

    def __or__(self, other):
        """| bitwise operation.

        Parallel merge self.rules_list to other.rules_list and return self.
        """
        for rule in other.rules_list:
            self.rules_list.append(rule)
        return self

    def show(self):
        """Show the structure of self.rules_list, only for debug."""
        for rule in self.rules_list:
            result = ", ".join([str(check) for check, deny in rule])
            print(result)

    def base(self):
        """Add base rule."""
        return None

    @coroutine
    def run(self):
        """Run self.rules_list.

        Return True if one rule channel has been passed.
        Otherwise return False and the deny() method of the last failed rule.
        """
        failed_result = None
        for rule in self.rules_list:
            for check, deny in rule:
                checked = yield from check()
                if not checked:
                    failed_result = (False, deny)
                    break
            else:
                return (True, None)
        return failed_result

    @coroutine
    def check(self):
        """Codes to determine whether this rule is passed.

        Must be overrided.
        """
        raise NotImplementedError()

    @coroutine
    def deny(self):
        """Codes to be execute when check() failed.

        Must be overrided.
        """
        raise NotImplementedError()

    @staticmethod
    def _and(rules_list_pre, rules_list_pro):
        """Serial merge rule_list_pre to rule_list_pro."""
        return [rule_pre + rule_pro
                for rule_pre in rules_list_pre
                for rule_pro in rules_list_pro]
