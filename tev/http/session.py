# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from asyncio import coroutine

from ..psock import proto
from ..psock.client import Client

import logging
logger = logging.getLogger('tev')

from ..config import Config
config = Config.instance()

client = Client()

@coroutine
def get_user_id(sid):
    req = proto.Request('user', 'user.get_user_id', sid, dict())
    response = yield from  client.get(req)
    if proto.is_ok(response):
        data = response.data
        uid = data['uid']
        return uid
    return None

@coroutine
def get_user(sid):
    uid = yield from get_user_id(sid)
    req = proto.Request('user', 'user.get', sid, dict(id=uid))
    response = yield from client.get(req)
    if proto.is_ok(response):
        return response.data
    return None

@coroutine
def session_middleware(app, handler):
    @coroutine
    def session_handler(request):
        cookies = request.cookies
        try:
            request.session_id = cookies['sid']
            request.user_id = yield from get_user_id(request.session_id)
        except KeyError:
            request.session_id = None
            request.user_id = None
        return (yield from handler(request))
    return session_handler
