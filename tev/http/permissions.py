# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .permission import Permission
from .rules import (UserRule, AdminRule, StaffRule, EmbassadorRule, ResourceOwnerRule)

class BasePermission(Permission):
    def __init__ (self, request, **kwargs):
        rule = self.rule(request, **kwargs)
        if not rule:
            raise AttributeError()
        self.rule = rule


class UserPerm(BasePermission):
    def rule (self, request, **kwargs):
        return UserRule(request, **kwargs)


class EmbassadorPerm(BasePermission):
    def rule (self, request, **kwargs):
        return EmbassadorRule(request, **kwargs) | StaffRule(request, **kwargs)


class StaffPerm(BasePermission):
    def rule (self, request, **kwargs):
        return StaffRule(request, **kwargs)


class ResourceOwnerPerm(BasePermission):
    def rule (self, request, **kwargs):
        return UserRule(request, **kwargs) & ResourceOwnerRule(request, **kwargs)
