# Copyright (C) 2016  Pierre Marchand <pierremarc@oep-h.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from html import escape as html_escape
from html.parser import HTMLParser
from html.entities import html5
import xml.etree.ElementTree as ET

def escape (s):
    return html_escape(s, True)


def get_attribute (attrs, name):
    for k, v in attrs:
        if name == k:
            return v
    raise KeyError(name)

class SanitizerError(Exception):
    pass

class Sanitizer(HTMLParser):

    def __init__ (self, safe_tags=None):
        HTMLParser.__init__(self, convert_charrefs=True)
        if safe_tags:
            self.safe_tags = safe_tags
        else:
            self.safe_tags = (
                'a', 'b', 'blockquote', 'br', 'cite', 'code', 'dd', 'dl', 'dt', 'em', 'i', 'li', 'ol', 'p', 'pre', 'q', 'small', 'span', 'strike', 'strong', 'sub', 'sup', 'u', 'ul'
            )

    def check_tag (self, tag):
        return tag in self.safe_tags

    def handle_starttag(self, tag, attrs):
        self.is_safe = self.check_tag(tag)
        if self.is_safe:
            self.builder.start(tag)

    def handle_endtag(self, tag):
        if self.is_safe:
            self.builder.end(tag)

    def handle_data(self, data):
        if self.is_safe:
            self.builder.data(data)

    def handle_comment(self, data):
        pass

    def handle_entityref(self, name):
        if self.is_safe:
            self.builder.data(html5[name])

    def handle_decl(self, data):
        pass

    def process (self, fragment):
        self.is_safe = False
        self.builder = ET.TreeBuilder()
        self.builder.start('div')
        self.feed(fragment)
        self.reset()

        elem = self.builder.close()
        return ET.tostring(elem, encoding='unicode', method='html')
