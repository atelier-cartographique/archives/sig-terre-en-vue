# # Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
# #
# # This program is free software: you can redistribute it and/or modify
# # it under the terms of the GNU Affero General Public License as
# # published by the Free Software Foundation, either version 3 of the
# # License, or (at your option) any later version.
# #
# # This program is distributed in the hope that it will be useful,
# # but WITHOUT ANY WARRANTY; without even the implied warranty of
# # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# # GNU Affero General Public License for more details.
# #
# # You should have received a copy of the GNU Affero General Public License
# # along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# from aiohttp_jinja2 import render_string
# from jinja2 import contextfilter
# import mistune
#
# from ..psock import proto
# from ..psock.client import SyncClient
#
# import logging
# logger = logging.getLogger('tev')
#
#
# from ..config import Config
# config = Config.instance()
# client_user = SyncClient(config.get('user.zmq.connect'))
# client_analysis = SyncClient(config.get('analysis.zmq.connect'))
#
# def url (request, resource):
#     router = request.app.router['resource']
#     return router.url(parts=dict(
#                     uid=resource.get('user_id', None),
#                     type=resource.get('type', None),
#                     rid=resource.get('id')
#                     ))
#
#
# def author (request, resource):
#     uid = resource.get('user_id', None)
#     req = proto.Request('user', 'user.get',
#                         request.session_id, dict(id=uid))
#     response = yield from client_user.get(req)
#     user = response.data
#     return '<a class="author-link" href="/user/{}">{}</a>'.format(
#                             user.get('id'), user.get('name') )
#
#
# def comp_template_name (request, resource, comp_name):
#     return '{}_{}.html'.format(resource['type'], comp_name)
#
#
# def format (request, resource, comp_name, options):
#     template = comp_template_name(request, resource, comp_name)
#     editable = request.user_id == resource['user_id']
#     return render_string(template, request, dict(
#             resource=resource,
#             url=url(request, resource),
#             user_id=request.user_id,
#             editable=editable,
#             options=options
#         ))
#
#
# def linker (request, source, target):
#         req = proto.Request('user', 'link.exists',
#                             request.session_id,
#                             dict(source=source['id'], target=target['id']))
#
#         response = yield from client_user.get(req)
#         if proto.is_ok(response):
#             return render_string('linker.html', request, dict(
#                     source=source,
#                     target=target,
#                     link_exists=response.data['result'],
#                     user_id=request.user_id
#                 ))
#
#         return ''
#
#
#
# @contextfilter
# def resource_comp (context, resource, comp_name='link', options=dict()):
#     return format(context.get('request'), resource, comp_name, options)
#
# @contextfilter
# def resource_url (context, resource):
#     return url(context.get('request'), resource)
#
# @contextfilter
# def resource_author (context, resource):
#     return author(context.get('request'), resource)
#
# @contextfilter
# def resource_linker (context, source, target):
#     return linker(context.get('request'), source, target)
#
# @contextfilter
# def do_json(context, value, **kwargs):
#     """Serializes an object as JSON. Optionally given keyword arguments
#     are passed to json.dumps(), ensure_ascii however defaults to False.
#     """
#     import json
#     kwargs.setdefault('ensure_ascii', False)
#     return json.dumps(value, **kwargs)
#
#
# def feature (geom):
#     return { 'type': 'Feature', 'geometry': geom, 'properties': {} }
#
# @contextfilter
# def get_results (context, query):
#     req = proto.Request('analysis', 'query.qexec',
#                         context.get('request').session_id,
#                         dict(qid=query['id']))
#     response = yield from client_analysis.get(req)
#
#     results = None
#     if proto.is_ok(response):
#         results = response.data
#
#     # logger.debug('get_results {}'.format(results))
#     return results
#
# @contextfilter
# def get_features (context, results):
#     features = []
#     try:
#         geom = results.get('geom')
#         if geom:
#             for item in results.get('results', []):
#                 try:
#                     features.append(feature(item[geom]))
#                 except Exception as ex:
#                     logger.warning('get_features {}'.format(ex))
#                     pass
#     except Exception:
#         pass
#     return features
#
#
# @contextfilter
# def markdown (context, body, **kwargs):
#     try:
#         return mistune.markdown(body, **kwargs)
#     except Exception:
#         return body
#
#
# def configure(env):
#         env.filters['component'] = resource_comp
#         env.filters['url'] = resource_url
#         env.filters['author'] = resource_author
#         env.filters['linker'] = resource_linker
#         env.filters['json'] = do_json
#         env.filters['results'] = get_results
#         env.filters['features'] = get_features
#         env.filters['md'] = markdown
