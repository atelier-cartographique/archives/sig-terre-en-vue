# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import asyncio
from aiohttp import web
from pkg_resources import resource_string

import mimetypes
mimetypes.init()

import logging
logger = logging.getLogger('tev')

from ..config import Config
config = Config.instance().get_config('http')

class StaticResource(web.View):
    """StaticResource serves static resources"""

    cache = dict()
    want_cache = config.get('static.cache', True)

    @asyncio.coroutine
    def _get_cache (self, resource):
        logger.info('static.get [{}]'.format(resource))
        if resource not in self.cache:
            try:
                self.cache[resource] = resource_string('tev', resource)
            except FileNotFoundError:
                logger.info('static.get NotFound [{}]'.format(resource))
                return web.HTTPNotFound()
        return web.Response(body=self.cache[resource])

    @asyncio.coroutine
    def _get_no_cache (self, resource):
        logger.info('static.get_no_cache [{}]'.format(resource))
        try:
            mt = mimetypes.guess_type(resource)
            return web.Response(body=resource_string('tev', resource),
                                content_type=mt[0])
        except FileNotFoundError:
            logger.info('static.get_no_cache NotFound [{}]'.format(resource))
            return web.HTTPNotFound()

    @asyncio.coroutine
    def get (self):
        resource = 'http/static/' + self.request.match_info['resource']
        if self.want_cache:
            return (yield from self._get_cache(resource))
        return (yield from self._get_no_cache(resource))
