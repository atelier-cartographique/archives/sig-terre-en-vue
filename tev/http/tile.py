# # Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
# #
# # This program is free software: you can redistribute it and/or modify
# # it under the terms of the GNU Affero General Public License as
# # published by the Free Software Foundation, either version 3 of the
# # License, or (at your option) any later version.
# #
# # This program is distributed in the hope that it will be useful,
# # but WITHOUT ANY WARRANTY; without even the implied warranty of
# # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# # GNU Affero General Public License for more details.
# #
# # You should have received a copy of the GNU Affero General Public License
# # along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# import math
# from base64 import b64decode
# from asyncio import Task, coroutine, wait_for
# from aiohttp import web, MultiDict
# from aiohttp_jinja2 import render_template
#
# from ..psock import proto
# from ..psock.client import SyncClient
#
# import logging
# logger = logging.getLogger('tev')
#
# from ..config import Config
# config = Config.instance()
# config_tile = config.get('tile')
#
# client_tile = None
#
# C = 40075016.686
#
# from collections import namedtuple
# Resolution = namedtuple('Resolution', ['z', 'res'])
# Size = namedtuple('Size', ['width', 'height'])
# Point = namedtuple('Point', ['x', 'y'])
# BoundingBox = namedtuple('BoundingBox', ['minx', 'miny', 'maxx', 'maxy'])
#
#
# def get_params (request, params):
#     ret = []
#     for param in params:
#         ret.append(request.match_info[param])
#     return ret;
#
# def layer_data (name):
#     layers = config_tile.get('layers')
#     layer = None
#     for l in layers:
#         if name == l['name']:
#             layer = l
#             break
#     if not layer:
#         return dict()
#
#
#     tw = config_tile.get('width')
#     sz = Size._make(layer['size'])
#     px = Size._make(layer['pixel'])
#     o = Point._make(layer['origin'])
#     oo = Point._make([0,0])
#     end = Point(
#         o.x + (sz.width * px.width),
#         o.y + (sz.height * px.height)
#     )
#     bbox = BoundingBox(o.x, o.y, end.x, end.y)
#     resolutions = []
#     for z in range(19):
#         res = C / tw / math.pow(2, z)
#         resolutions.append(Resolution(z, res))
#     return {
#         'srs': layer.get('srs'),
#         'name': layer.get('name'),
#         'title': layer.get('title'),
#         'abstract': layer.get('abstract'),
#         'bbox': bbox,
#         'origin': o,
#         'resolutions': resolutions
#     }
#
# def tile_context (request, options=dict()):
#     base = {
#         'tile_url': request.app.router['tile'].url(),
#         'tile_version': config_tile.get('version'),
#         'tile_width': config_tile.get('width'),
#         'tile_height': config_tile.get('height'),
#         'tile_mime': config_tile.get('mime'),
#         'tile_extension': config_tile.get('extension')
#     }
#     base.update(options)
#     return base
#
# def _get_tile (request, params):
#     req = proto.Request('tile', 'tile.get_tile',
#                         request.session_id, params)
#     response = client_tile.get(req)
#     if proto.is_ok(response):
#         return response.data
#     return {}
#
#
# @coroutine
# def get_tile (request):
#     """Get Tile
#     """
#     service, layer, z, x, y = get_params(request,
#                                          ['service', 'layer', 'z', 'x', 'y'])
#     payload = dict(
#         service=service, layer=layer,
#         x=int(x), y=int(y), z=int(z)
#     )
#     data = _get_tile(request, payload)
#     if 'png' in data:
#         pack = b64decode(data['png'].encode())
#         headers = MultiDict({
#             # 'CONTENT-DISPOSITION': 'attachment; filename="{}-{}-{}.png"'
#         })
#         response = web.StreamResponse(headers=headers)
#         response.content_type = config_tile.get('mime')
#         response.content_length = len(pack)
#         yield from response.prepare(request)
#         response.write(pack)
#         return response
#
#     return web.json_response(data)
#
#
# @coroutine
# def server_capabilities (request):
#     """Get Server Capabilities
#     """
#     return render_template('tile_server_capabilities.xml',
#                            request, tile_context(request))
#
# @coroutine
# def service_capabilities (request, service):
#     """Get Service Capabilities
#     """
#     service = request.match_info['service']
#     options= dict(layers=config_tile.get('layers', []))
#     return render_template('tile_server_capabilities.xml',
#                            request, tile_context(request, options))
#
# @coroutine
# def layer_capabilities (request, service, layer):
#     """Get layer Capabilities
#     """
#     service, layer = get_params(request, ['service', 'layer'])
#     options= dict(layer=layer_data(layer))
#     return render_template('tile_layer_capabilities.xml',
#                            request, tile_context(request, options))
#
#
# def configure(router):
#     global client_tile
#     client_tile = SyncClient(config.get('tile.zmq.connect'))
#
#     router.add_route('GET', '/tile/',
#                      server_capabilities, name="tile")
#
#     router.add_route('GET', '/tile/{service}/',
#                      service_capabilities, name="tile_service")
#
#     router.add_route('GET', '/tile/{service}/{layer}/',
#                      layer_capabilities, name="tile_layer")
#
#     router.add_route('GET', '/tile/{service}/{layer}/{z}/{x}/{y}',
#                      get_tile, name="tile_tile")
