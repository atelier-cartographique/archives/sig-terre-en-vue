# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from asyncio import coroutine
from aiohttp import (MultipartReader, hdrs)
from json import loads

def multidict_to_dict (md):
    result = dict()
    for k, v in md.items():
        result[k] = v
    return result

def disposition (cd):
    cds = [x.strip() for x in cd.split(';')]
    result = dict()
    for srec in cds:
        rec = srec.split('=')
        if len(rec) == 2:
            result[rec[0]] = loads(rec[1])
    return result

class PairsMultipartReader(MultipartReader):

    @coroutine
    def next(self):
        """Emits a tuple of document object (:class:`dict`) and multipart
        reader of the followed attachments (if any).

        :rtype: tuple
        """
        reader = yield from super().next()

        if self._at_eof:
            return None, None

        if isinstance(reader, MultipartReader):
            part = yield from reader.next()
            doc = multidict_to_dict(reader.headers)
        else:
            # data = yield from reader.read(decode=True)
            doc = multidict_to_dict(reader.headers)

        return doc, reader


@coroutine
def get_file (client_response, name):
    reader = PairsMultipartReader.from_response(client_response)
    limit = 22

    for r in range(limit):
        doc, files_reader = yield from reader.next()
        if doc is None:
            return None
        
        if 'CONTENT-DISPOSITION' in doc:
            disp = disposition(doc['CONTENT-DISPOSITION'])
            if disp['name'] == name:
                return (disp, files_reader)
        elif 'Content-Disposition' in doc:
            disp = disposition(doc['Content-Disposition'])
            if disp['name'] == name:
                return (disp, files_reader)
