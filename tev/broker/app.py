# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import asyncio
from zmq.asyncio import ZMQEventLoop
from ..config import Config
from ..psock.broker import Broker
from ..psock.dispatch import Dispatch

import logging
logger = logging.getLogger('tev')

def start ():
    # loop = asyncio.get_event_loop()
    loop = ZMQEventLoop()
    asyncio.set_event_loop(loop)
    config = Config.instance()
    bind = config.get('broker.bind', 'tcp://0.0.0.0')
    connect = config.get('broker.connect', 'tcp://127.0.0.1')
    port = config.get('broker.port', 8885)

    broker = Broker(bind, connect, port)
    broker.start()

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        tasks = asyncio.Task.all_tasks(loop)
        for task in tasks:
            task.cancel()
        print('shutting down broker')
    loop.close()
