# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from time import monotonic

class CacheMiss(KeyError):
    def __init__ (self, *args, **kwargs):
        KeyError.__init__(self, *args, **kwargs)

class CacheItem:

    def __init__ (self, data):
        self.data = data
        self.ts = monotonic()
        self.freq = 0

    def get (self):
        self.freq += 1
        return self.data

class Cache:

    def __init__ (self, max_size=24):
        self.max_size = max_size
        self._cache = {}

    def ensure_space (self):
        if (len(self._cache) > self.max_size):
            candidate_item = None
            candidate_key = None
            for key in self._cache:
                item = self._cache[key]
                if not candidate_key:
                    candidate_key = key
                    candidate_item = item
                    continue
                if item.freq < candidate_item.freq:
                    candidate_key = key
                    candidate_item = item
                    continue
                if item.freq == candidate_item.freq:
                    if item.ts < candidate_item.ts:
                        candidate_key = key
                        candidate_item = item

            del self._cache[candidate_key]

    def put (self, key, data):
        self.ensure_space()
        self._cache[key] = CacheItem(data)

    def get (self, key):
        try:
            return self._cache[key].get()
        except KeyError:
            raise CacheMiss(key)
