# # Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
# #
# # This program is free software: you can redistribute it and/or modify
# # it under the terms of the GNU Affero General Public License as
# # published by the Free Software Foundation, either version 3 of the
# # License, or (at your option) any later version.
# #
# # This program is distributed in the hope that it will be useful,
# # but WITHOUT ANY WARRANTY; without even the implied warranty of
# # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# # GNU Affero General Public License for more details.
# #
# # You should have received a copy of the GNU Affero General Public License
# # along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# from ..models import Link
# from ...
# from ...psock.proto import response_ok
# from uuid import UUID, uuid4
# from copy import deepcopy
#
# import logging
# logger = logging.getLogger('tev')
#
#
# class LinkHandler:
#
#     def __init__ (self):
#         user_addr = config.get('user.zmq.connect')
#         self.uclient = Client(user_addr)
#         self.session = SessionManager()
#
#     @asyncio.coroutine
#     def create_link (self, request):
#         params = request.params
#         qid = params['qid']
#         rid = params['rid']
#         uid = self.session.get_user_id(request.sid)
#
#         cr = Request('user', 'resource.get', uid, dict(id=rid))
#         resource_response = yield from self.uclient.get(cr)
#         resource = resource_response.data
#
#         if uid != resource['user_id']:
#             raise Exception('ResourceNotOwned')
#
#         sess = get_session('app')
#         results = sess.execute(stmt)
#         result_array = []
#         for row in results:
#             result_array.append(process_row(row, geom))
#         return response_ok(dict(results=result_array, geom=geom))
