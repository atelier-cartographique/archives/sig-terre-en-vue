# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import asyncio
import json
from sqlalchemy.sql import text as SQLText
from hashlib import md5

from ...config import Config
from ...db.db import get_session
from ...psock.client import Client
from ...psock.proto import Request, Response, response_error, response_ok
from ...user.session import SessionManager

from .processor import process_geom
from .cache import Cache, CacheMiss

import logging
logger = logging.getLogger('tev')

config = Config.instance()

def process_row (row, geom, format=None):
    data = dict()
    for k,v in row.items():
        if k == geom:
            try:
                data[k] = process_geom(v)
            except Exception as ex:
                logger.warning('process_row: ogr [{}]'.format(ex))
        else:
            try:
                json.dumps(v)
                data[k] = v
            except Exception as exc:
                logger.info('query:process_row: Skip key [{}] because causing serialization to fail on [{}]'.format(k, exc))
    return data


class QueryHandler:

    def __init__ (self):
        self.uclient = Client()
        self.session = SessionManager()
        self.qcache = Cache()

    @asyncio.coroutine
    def qexec (self, request):
        params = request.params
        qid = params['qid']
        uid = self.session.get_user_id(request.sid)

        cr = Request('user', 'resource.get', uid, dict(id=qid))
        resource_response = yield from self.uclient.get(cr)
        resource = resource_response.data

        body = resource.get('body')
        geom = resource.get('geo_column')
        result_array = []
        key = md5(body.encode()).hexdigest()
        try:
            result_array = self.qcache.get(key)
            logger.debug('query.exec:cache_key retrieve {} {}'.format(
                key, len(result_array)
            ))
        except CacheMiss:
            stmt = SQLText(body)
            sess = get_session('geo')
            results = sess.execute(stmt)
            for row in results:
                # logger.debug('qexec: row: {}'.format(row))
                result_array.append(process_row(row, geom))
            logger.debug('query.exec:cache_key insert {} {}'.format(
                key, len(result_array)
            ))
            self.qcache.put(key, result_array)
        return response_ok(dict(results=result_array, geom=geom))


    @asyncio.coroutine
    def qexec_sql (self, request):
        params = request.params
        body = params.get('sql')
        geom = params.get('geo_column')
        result_array = []
        key = md5(body.encode()).hexdigest()
        try:
            result_array = self.qcache.get(key)
            logger.debug('query.exec:cache_key retrieve {} {}'.format(
                key, len(result_array)
            ))
        except CacheMiss:
            stmt = SQLText(body)
            sess = get_session('geo')
            results = sess.execute(stmt)
            for row in results:
                result_array.append(process_row(row, geom))
            logger.debug('query.exec:cache_key insert {} {}'.format(
                key, len(result_array)
            ))
            self.qcache.put(key, result_array)
        return response_ok(dict(results=result_array, geom=geom))
