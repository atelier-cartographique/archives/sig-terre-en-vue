# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import asyncio
import json
from sqlalchemy.sql import text as SQLText
from hashlib import md5

from ...config import Config
from ...db.db import get_session
from ...psock.client import Client
from ...psock.proto import Request, Response, response_error, response_ok
from ...user.session import SessionManager

from .processor import process_geom
from .cache import Cache, CacheMiss

import logging
logger = logging.getLogger('tev')

config = Config.instance()

PLOT_FORMAT_SQL = """
SELECT *
FROM {cadastre}
WHERE capakey=:capakey
""".format(cadastre=config.get('analysis.cadastre'))

PLOT_CAPAKEY_GEOM = """
SELECT capakey, St_AsGeoJSON(geom)
FROM {cadastre}
WHERE capakey=:capakey
""".format(cadastre=config.get('analysis.cadastre'))

PLOT_CAPAKEY_CENTER = """
SELECT ST_SRID(ST_Centroid(geom)) as srid, ST_Y(ST_Centroid(geom)) as latitude, ST_X(ST_Centroid(geom)) as longitude
FROM {cadastre}
WHERE capakey = :capakey
""".format(cadastre=config.get('analysis.cadastre'))

PLOT_HIT = """
SELECT capakey, St_AsGeoJSON(geom), ST_Area(geom)
FROM {cadastre}
WHERE St_Contains(geom, ST_GeomFromEWKT('SRID=:srid;POINT(:longitude :latitude)'))
""".format(cadastre=config.get('analysis.cadastre'))

PLOT_HIT_MUNICIPALITY = """
-- municipality hit
SELECT mun.name_fre, mun.name_dut
FROM tev.urb_adm_municipality as mun
WHERE St_Contains(mun.geom, ST_GeomFromEWKT('SRID=:srid;POINT(:longitude :latitude)'));
"""

PLOT_HIT_AFFECTATION = """
-- affectation
SELECT a."NAME_FR", a."NAME_NL"
FROM tev.brugis_affectation as a
WHERE St_Contains(a.geom, ST_GeomFromEWKT('SRID=:srid;POINT(:longitude :latitude)'));
"""

PLOT_HIT_NATURA_STATION = """
-- natura2000 station & habitat
SELECT a.id
FROM tev.ibge3_natura_2000_station as a
WHERE St_Contains(a.geom, ST_GeomFromEWKT('SRID=:srid;POINT(:longitude :latitude)'));
"""

PLOT_HIT_NATURA_HABITAT = """
SELECT a.id
FROM tev.ibge3_natura_2000_habitat as a
WHERE St_Contains(a.geom, ST_GeomFromEWKT('SRID=:srid;POINT(:longitude :latitude)'));
"""

PLOT_HIT_RESERVE_NAT = """
-- reserve naturelle
SELECT a.id
FROM tev.ibge3_natural_reserve as a
WHERE St_Contains(a.geom, ST_GeomFromEWKT('SRID=:srid;POINT(:longitude :latitude)'));
"""

PLOT_HIT_PAC = """
-- declaration PAC
SELECT a.id
FROM tev.alv_2015 as a
WHERE St_Contains(a.geom, ST_GeomFromEWKT('SRID=:srid;POINT(:longitude :latitude)'));
"""


PLOT_HIT_POLLUTION = """
-- pollution
SELECT a.cat
FROM david.potentiel_intersection_pollution as a
WHERE St_Contains(a.geom, ST_GeomFromEWKT('SRID=:srid;POINT(:longitude :latitude)'));
"""

PLOT_HIT_NATURE_PROPRIETAIRE = """
-- nature proprietaire
SELECT a.ac_poten_1
FROM david.potentiel_process_nature_proprietaire as a
WHERE St_Contains(a.geom, ST_GeomFromEWKT('SRID=:srid;POINT(:longitude :latitude)'));
"""


def format_plot (row):
    return dict()


class PlotHandler:

    def __init__ (self):
        # user_addr = config.get('user.zmq.connect')
        self.uclient = Client()
        self.session = SessionManager()
        self.qcache = Cache(200)
        self.capa_cache = Cache(200)

    @asyncio.coroutine
    def format (self, request):
        params = request.params
        pid = params['pid']
        uid = self.session.get_user_id(request.sid)

        cr = Request('user', 'resource.get', uid, dict(id=pid))
        resource_response = yield from self.uclient.get(cr)
        resource = resource_response.data

        capakey = resource.get('capakey')
        result = dict()
        try:
            result = self.qcache.get(capakey)
        except CacheMiss:
            stmt = SQLText(PLOT_FORMAT_SQL)
            sess = get_session('geo')
            results = sess.execute(stmt, capakey=capakey)
            result = format_plot(results.first())
            self.qcache.put(key, result)
        return response_ok(result=result)

    @asyncio.coroutine
    def hit (self, request):
        """
        this method runs a hit test
        it expects params:
            srid
            latitude
            longitude
        """
        params = request.params
        srid = int(params['srid'])
        lat = float(params['latitude'])
        lon = float(params['longitude'])
        return (yield from hit_private(srid, lat, lon))

    @asyncio.coroutine
    def capakey(self, request):
        params = request.params
        logger.debug('capakey {}'.format(params['capakey']))
        sess = get_session('geo')
        stmt = SQLText(PLOT_CAPAKEY_CENTER).bindparams(capakey=params['capakey'])
        point = sess.execute(stmt).first()
        return (yield from hit_private(point[0], point[1], point[2]))

    @asyncio.coroutine
    def capakey_geom(self, request):
        params = request.params
        capakey=params['capakey']
        logger.debug('capakey_geom {}'.format(capakey))
        try:
            geom = self.capa_cache.get(capakey)
            logger.debug('capakey from cache {}'.format(capakey))
        except CacheMiss:
            sess = get_session('geo')
            stmt = SQLText(PLOT_CAPAKEY_GEOM).bindparams(capakey=capakey)
            plot = sess.execute(stmt).first()
            geom = json.loads(plot[1]);
            self.capa_cache.put(capakey, geom)

        return response_ok(geom)

@asyncio.coroutine
def hit_private(srid, lat, lon):
    logger.debug('hit_private {} {}'.format(lat, lon))
    bparams = dict(
        srid=srid,
        latitude=lat,
        longitude=lon
    )
    sess = get_session('geo')

    ## plot hit
    stmt = SQLText(PLOT_HIT).bindparams(**bparams)
    # print_statement(sess, stmt)
    plot = sess.execute(stmt).first()
    if plot:
        geom = json.loads(plot[1])
        props = dict(
            capakey=plot[0],
            area=plot[2]
        )
        stmt = SQLText(PLOT_HIT_MUNICIPALITY).bindparams(**bparams)
        row = sess.execute(stmt).first()
        if row:
            props['municipality'] = dict(fr=row[0], nl=row[1])

        stmt = SQLText(PLOT_HIT_AFFECTATION).bindparams(**bparams)
        row = sess.execute(stmt).first()
        if row:
            props['affectation'] = dict(fr=row[0], nl=row[1])

        stmt = SQLText(PLOT_HIT_NATURA_STATION).bindparams(**bparams)
        row = sess.execute(stmt).first()
        if row:
            props['natura2000_station'] = True
        else:
            props['natura2000_station'] = False

        stmt = SQLText(PLOT_HIT_NATURA_HABITAT).bindparams(**bparams)
        row = sess.execute(stmt).first()
        if row:
            props['natura2000_habitat'] = True
        else:
            props['natura2000_habitat'] = False

        stmt = SQLText(PLOT_HIT_RESERVE_NAT).bindparams(**bparams)
        row = sess.execute(stmt).first()
        if row:
            props['reserve_naturelle'] = True
        else:
            props['reserve_naturelle'] = False

        stmt = SQLText(PLOT_HIT_PAC).bindparams(**bparams)
        row = sess.execute(stmt).first()
        if row:
            props['pac'] = True
        else:
            props['pac'] = False

        stmt = SQLText(PLOT_HIT_POLLUTION).bindparams(**bparams)
        row = sess.execute(stmt).first()
        if row:
            props['pollution'] = row[0]
        else:
            props['pollution'] = -1

        stmt = SQLText(PLOT_HIT_NATURE_PROPRIETAIRE).bindparams(**bparams)
        row = sess.execute(stmt).first()
        if row:
            props['proprietaire'] = row[0]
        else:
            props['proprietaire'] = False



        return response_ok(dict(geometry=geom, properties=props))
    return response_error('NoHit')

def print_statement (sess, stmt):
    from sqlalchemy.sql import compiler
    dialect = sess.bind.dialect
    logger.debug('statement: {}'.format(stmt.compile(dialect=dialect, compile_kwargs={"literal_binds": True})))
    # comp = compiler.SQLCompiler(dialect, stmt)
    # comp.compile()
