# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



from osgeo import ogr, osr
import json
import binascii

import logging
logger = logging.getLogger('tev')


ogr.UseExceptions()
sourceSref = osr.SpatialReference()
sourceSref.ImportFromEPSG(31370)         # from EPSG
targetSref = osr.SpatialReference()
targetSref.ImportFromEPSG(3857)         # from EPSG

transform = osr.CoordinateTransformation(sourceSref, targetSref)

#
# GEO_FORMATS = {
#     'wkb': ogr.CreateGeometryFromWkb,
#     'wkt': ogr.CreateGeometryFromWkt,
#     'json': ogr.CreateGeometryFromJson,
# }
#
# def getGeoJSON (geometry, format):
#     if not format:
#


def process_geom (data):
    wkb_geom = None
    data_t  = type(data)
    if (str == data_t) or (bytes == data_t):
        wkb_geom = data
    elif memoryview == data_t:
        wkb_geom = data.tobytes()

    geometry = None
    try:
        geometry = ogr.CreateGeometryFromWkb(wkb_geom)
    except Exception as ex:
        logger.warning('process_geom {}'.format(ex))
    # geometry.Transform(transform)
    return json.loads(geometry.ExportToJson())
