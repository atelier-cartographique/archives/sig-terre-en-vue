# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math
from base64 import b64encode
from ...db.db import get_session
from ...psock.proto import (response_ok, response_error)

from .layer import create_layer
from ..lingua import (LinguaWMS as Lingua, Size, BoundingBox)

from ...config import Config
config = Config.instance().get_config('tile')

from uuid import UUID, uuid4

import logging
logger = logging.getLogger('tev')

class TileHandler:

    def __init__ (self):
        tc = config.get('layers')
        self.layers = dict()
        for lc in tc:
            try:
                name = lc.get('name')
                layer = create_layer(name, lc.get('source'))
                self.layers[name] = layer
                logger.info('Added Layer [{}] To Tile Server'.format(name))
            except Exception as ex:
                logger.error('TileHandler {}'.format(ex))

    def get_tile (self, request):
        params = request.params
        logger.debug('get_tile {}'.format(params))
        name = params.get(Lingua.Request.layers.name)[0]
        if name not in self.layers:
            return response_error('No Layer By This Name [{}]'.format(name))

        layer = self.layers[name]
        bbox = BoundingBox._make(params.get(Lingua.Request.bbox.name))
        tile_size = Size(
            params.get(Lingua.Request.width.name),
            params.get(Lingua.Request.height.name)
        )
        png_bytes = layer.render(tile_size, bbox)
        pack = b64encode(png_bytes)
        return response_ok({
            'png': pack.decode('utf-8')
        })
