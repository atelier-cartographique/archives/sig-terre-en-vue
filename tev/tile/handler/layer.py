# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math
from osgeo import gdal, osr
gdal.UseExceptions()
import numpy
from PIL import Image, ImageDraw, ImageFont, ImageOps, ImageEnhance
from io import BytesIO
import mapnik
mapnik.logger.set_severity(mapnik.severity_type.Debug)

import logging
logger = logging.getLogger('tev')


def world2Pixel(geoMatrix, x, y):
    """
    Uses a gdal geomatrix (gdal.GetGeoTransform()) to calculate
    the pixel location of a geospatial coordinate
    """
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    rtnX = geoMatrix[2]
    rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return (pixel, line)

class BaseLayer:

    def __init__ (self, name, options):
        raise NotImplementedError()

    def render(self, sz, bbox):
        raise NotImplementedError()

def round_bbox(bbox):
    return BoundingBox._make([int(x) for x in bbox])


def process_image (im):
    contrast = ImageEnhance.Contrast(im)
    im = contrast.enhance(1.5)
    brightness = ImageEnhance.Brightness(im)
    im = brightness.enhance(1.5)
    # im = ImageOps.autocontrast(im)
    # im = ImageOps.invert(im)
    im = ImageOps.grayscale(im)
    im = ImageOps.colorize(im, (0, 0, 0), (255, 243, 191))
    return im

def create_image(np_array):
    bn, w, h = np_array.shape
    bands = []
    for a in np_array:
        bands.append(Image.fromarray(a, mode='L'))
    return Image.merge('RGB', bands)

class GdalLayer(BaseLayer):

    def __init__ (self, name, options):
        self.name = name
        self.path = options.get('path')
        self.source = None

    def get_source (self):
        if not self.source:
            self.source = gdal.Open(self.path)
        return self.source

    def render (self, sz, bbox):
        # bbox = round_bbox(bbox_f)
        logger.debug('gdal:render {} {}'.format(sz, bbox))
        source = self.get_source()
        bands = numpy.zeros((3, sz.height, sz.width), dtype=numpy.uint8)
        sgt = source.GetGeoTransform()

        # world2pixel gives a line number for y, from top to bottom in the image
        minx, maxy = world2Pixel(sgt, bbox.minx, bbox.miny)
        maxx, miny = world2Pixel(sgt, bbox.maxx, bbox.maxy)
        logger.debug('gdal:render:bbox [{}, {}, {}, {}]'.format(
            minx, miny, maxx,  maxy
        ))
        source.ReadAsArray(
            minx, miny, maxx - minx, maxy - miny, bands,
            resample_alg=gdal.GRIORA_Lanczos
        )
        im = process_image(create_image(bands))
        f = BytesIO()
        im.save(f, 'PNG')
        # import pdb; pdb.set_trace()
        return f.getvalue()


def make_style (style_config):
    style = mapnik.Style()
    rule = mapnik.Rule()
    ps = mapnik.PolygonSymbolizer()
    ls = mapnik.LineSymbolizer()

    ps.fill = mapnik.Color(style_config.get('fill', '#3778D5'))
    ls.stroke = mapnik.Color(style_config.get('stroke', '#3778D5'))
    ls.stroke_width = style_config.get('stroke_width', 1)

    rule.symbols.append(ps)
    rule.symbols.append(ls)
    style.rules.append(rule)

    return style

def make_datasource (ds_config):
    # data_source = mapnik.PostGIS(
    #     host = ds_config.get('host'),
    #     user = ds_config.get('user'),
    #     password = ds_config.get('password'),
    #     dbname = ds_config.get('name'),
    #     table = ds_config.get('table')
    # )
    data_source = mapnik.PostGIS(**ds_config)
    return data_source

class MapnikLayer (BaseLayer):

    def __init__ (self, name, options):
        self.name = name
        self.options = options
        self.setup_map()

    def setup_map (self):
        style_name = 'the_style'
        style = make_style(self.options.get('style'))

        self.map = mapnik.Map(256, 256, self.options.get('srs'))
        self.map.append_style(style_name, style)

        layer = mapnik.Layer(self.name, self.options.get('srs'))
        layer.datasource = make_datasource(self.options.get('datasource'))
        layer.styles.append(style_name)
        self.map.layers.append(layer)

    def render (self, sz, bbox):
        logger.debug('mapnik:render {} {}'.format(sz, bbox))
        extent = mapnik.Box2d(bbox.minx, bbox.miny, bbox.maxx, bbox.maxy)
        self.map.resize(sz.width, sz.height)
        self.map.zoom_to_box(extent)
        # self.map.zoom_all()
        logger.debug('mapnik:map \n\t{} \n\t{} \n\t{} \n\t{}'.format(
                    self.map.srs,
                    self.map.envelope(),
                    list(self.map.styles),
                    list(self.map.layers)))
        im = mapnik.Image(sz.width, sz.height)
        # im.fill(mapnik.Color('#ffffff00'))
        self.map.background_color = mapnik.Color('#ffffff00')
        mapnik.render(self.map, im)
        return im.tostring('png32')




MODULES = {
    'gdal': GdalLayer,
    'mapnik': MapnikLayer
}

def create_layer (name, source):
    options = source.get('options')
    layer_module = source.get('module')
    return MODULES[layer_module](name, options)
