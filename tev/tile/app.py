# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import asyncio
from zmq.asyncio import install

from ..db import db; db.configure();
from ..config import Config
from ..psock.server import Server
from ..psock.dispatch import Dispatch
from .handler.tile import TileHandler

import logging
logger = logging.getLogger('tev')

def start ():
    loop = install()

    config = Config.instance()
    zmqConfig = config.get_config('tile.zmq')

    dispatch = Dispatch('tile')
    dispatch.add('tile', TileHandler())


    server = Server('{connect}:{port}'.format(**config.get('broker')), dispatch)
    server.start()

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        tasks = asyncio.Task.all_tasks(loop)
        for task in tasks:
            task.cancel()
        print('shutting down user')
    loop.close()
