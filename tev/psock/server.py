# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import asyncio
from .proto import Response, Request, response_error
import zmq
from zmq.asyncio import Context, Poller
from collections import namedtuple
from uuid import uuid4
import logging
logger = logging.getLogger('tev')

LockedSock = namedtuple('LockedSock', ['lock', 'sock'])

class Server:

    def __init__ (self, register_iface, dispatch):
        self.iface = register_iface
        self.dispatch = dispatch
        self.socks = []


    def set_listeners (self, fut):
        logger.debug('set_listeners  {}'.format(fut))
        message = fut.result()
        ctx = Context.instance()
        logger.debug('messaged  {}'.format(message))
        resp = Response.from_msg(message)
        service_connect = resp.data.get('connect')
        logger.debug('registered at {}'.format(service_connect))
        for i in range(4):
            sock = ctx.socket(zmq.REP)
            sid = 'server.{}-{}'.format(self.dispatch.service, uuid4())
            sock.set_string(zmq.IDENTITY, sid)
            sock.connect(service_connect)
            logger.debug('connected {} to {}'.format(sid, service_connect))
            self.socks.append(LockedSock(asyncio.Lock(), sock))

        loop = asyncio.get_event_loop()
        loop.create_task(self.start_listener())

    def start (self):
        logger.debug('start {} {}'.format(self.dispatch.service, self.iface))
        ctx = Context.instance()
        register_sock = ctx.socket(zmq.REQ)
        logger.debug('connect to {}'.format(self.iface))
        register_sock.connect(self.iface)
        logger.debug('connected to {}'.format(self.iface))
        req = Request('broker', 'create_service', 0,
                        dict(service_name=self.dispatch.service))

        register_sock.send_multipart(req.get_frames())
        logger.debug('sent {}'.format(req.get_frames()))
        message = register_sock.recv_multipart()
        logger.debug('typeof message {}'.format(message.done()))
        message.add_done_callback(self.set_listeners)

        # logger.debug('messaged  {}'.format(message.result()))
        # resp = Response.from_msg(message.result())
        # service_connect = resp.data.get('connect')
        # logger.debug('registered at {}'.format(service_connect))
        # for i in range(4):
        #     sock = ctx.socket(zmq.REP)
        #     sock.connect(service_connect)
        #     self.socks.append(LockedSock(asyncio.Lock(), sock))
        #
        # # loop = asyncio.get_event_loop()
        # loop.create_task(self.start_listener())


    @asyncio.coroutine
    def process_request (self, l_sock):
        lock, sock = l_sock
        with (yield from lock):
            resp = None
            try:
                msg = yield from sock.recv_multipart(copy=True)
                req = Request.from_msg(msg)
                logger.debug('server: {}/{}'.format(req.service, req.method))
            except Exception as ex:
                resp = response_error(str(ex))

            if not resp:
                try:
                    resp = yield from self.dispatch.dispatch(req)
                    logger.debug('got a response from dispatch {}'.format(resp.get_frames()))
                except Exception as ex:
                    resp = response_error(str(ex))

            yield from sock.send_multipart(resp.get_frames())


    def find_l_sock (self, sock):
        for l_sock in self.socks:
            if l_sock.sock == sock:
                return l_sock


    @asyncio.coroutine
    def start_listener (self):
        loop = asyncio.get_event_loop()
        poller = Poller()
        for s in self.socks:
            poller.register(s.sock, zmq.POLLIN)

        while loop.is_running():
            events = yield from poller.poll(1000)
            # logger.debug('>>>>> {}'.format(events))
            if None == events:
                continue
            for sock, event in events:
                logger.debug('server event {} {}'.format(sock, event))
                if event == zmq.POLLIN:
                    l_sock = self.find_l_sock(sock)
                    yield from self.process_request(l_sock)
