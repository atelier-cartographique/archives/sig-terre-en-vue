# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import asyncio
from .socket import Response, Request
from .proto import response_error

import logging
logger = logging.getLogger('tev')


class Dispatch:

    def __init__ (self, service):
        self.service = service
        self.processors = dict()

    def add (self, name, instance):
        if name in self.processors:
            logging.warning('adding a processor which is already added', name)
        self.processors[name] = instance


    @asyncio.coroutine
    def dispatch (self, request):
        procs = self.processors
        logger.debug('dispatch %s %s', self.service, request.method)
        method = request.method.split('.')
        assert (2 == len(method))

        proc_name = method[0]
        proc_method = method[1]

        if proc_name not in procs:
            logging.warning('unknown processor', proc_name)
            return response_error('unknown processor')

        inst = procs[proc_name]
        func = getattr(inst, proc_method)

        if not func:
            logging.warning('unknown method', proc_method)
            return response_error('unknown method')

        try:
            return func(request)
        except Exception as ex:
            logger.error('Dispatch:dispatch <{}>\n{}'.format(request.method, ex))
            return response_error(str(ex))
