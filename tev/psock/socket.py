# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import zmq
import zmq.asyncio
from .proto import Request, Response

class Socket(zmq.asyncio.Socket):

    def send_response(self, response):
        yield from self.send(response.get_frames())

    def send_request (self, request):
        yield from self.send(request.get_frames())

    def recv_response (self, flags=0):
        msg = yield from recv_multipart(flags=flags, copy=True)
        return Response.from_msg(msg)

    def recv_request (self, flags=0):
        msg = yield from recv_multipart(flags=flags, copy=True)
        return Request.from_msg(msg)



class Context(zmq.asyncio.Context):
    _socket_class = Socket

    def socket_rep (self):
        return self.socket(zmq.REP)

    def socket_req (self):
        return self.socket(zmq.REQ)
