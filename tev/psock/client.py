# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import signal
import asyncio
from time import time
from .proto import Response, Request, response_error
import zmq
from zmq.asyncio import Context

from uuid import uuid4

from time import perf_counter
import logging
logger = logging.getLogger('tev')

from ..config import Config
config = Config.instance()

zmq.asyncio.install()


def bound_range (start, bound):
    for a in range(start, bound):
        yield a
    for b in range(start):
        yield b

class ExclusiveSocket:

    def __init__ (self, sock):
        self._sock = sock
        self._lock = asyncio.Lock()

    def is_locked (self):
        return self._lock.locked()

    def swap_sock (self, sock):
        self._sock.close()
        self._sock = sock

    @asyncio.coroutine
    def request (self, frames, rid):
        message = None
        yt = time()
        with (yield from self._lock):
            st = time()
            logger.debug('ExclusiveSocket enter {} {}'.format(rid, round((st - yt) *1000)))
            yield from self._sock.send_multipart(frames)
            logger.debug('Frames Sent {}'.format(rid))
            message = yield from self._sock.recv_multipart()
            et = round((time() - st) * 1000)
            logger.debug('ExclusiveSocket exit {} {}'.format(rid, et))
        return message

class SockPool:

    def __init__ (self, max_sock=128):
        self.max_sock = max_sock
        self.sockets = dict()
        self.sockets_ptr = dict()

    def recycle (self, excl_sock, addr):
        ctx = Context.instance()
        sock = ctx.socket(zmq.REQ)
        sock.set_string(zmq.IDENTITY, 'client.{}'.format(uuid4()))
        sock.connect(addr)
        excl_sock.swap_sock(sock)

    def create_socket (self, addr):
        if addr in self.sockets:
            return

        self.sockets[addr] = []
        self.sockets_ptr[addr] = 0
        ctx = Context.instance()
        for n in range(self.max_sock):
            sock = ctx.socket(zmq.REQ)
            sock.set_string(zmq.IDENTITY, 'client.{}'.format(uuid4()))
            sock.connect(addr)
            self.sockets[addr].append(ExclusiveSocket(sock))


    def get_sock (self, addr):
        if addr not in self.sockets:
            raise Exception('Unknow Socket Address')

        for ptr in bound_range(self.sockets_ptr[addr], self.max_sock):
            sock = self.sockets[addr][ptr]
            if not sock.is_locked():
                logger.debug('SockPool:get_sock {} {}'.format(addr, ptr))
                self.sockets_ptr[addr] = ptr
                return self.sockets[addr][ptr]
            else:
                logger.warning('SockPool:get_sock LOCKED {} {}'.format(addr, ptr))

        # slow but not frozen
        ptr = self.sockets_ptr[addr] + 1
        if ptr >= self.max_sock:
            ptr = 0
        logger.warning('SockPool:get_sock FROZEN {} {}'.format(addr, ptr))
        self.sockets_ptr[addr] = ptr
        return self.sockets[addr][ptr]

## GLOBAL POOL
sock_pool = SockPool()

class Client:

    def __init__(self):
        connect = config.get('broker.connect')
        port = config.get('broker.port', 8885)
        self.con_addr = '{}:{}'.format(connect, port)
        logger.debug('client {}'.format(self.con_addr))
        sock_pool.create_socket(self.con_addr)

    @asyncio.coroutine
    def get (self, request):
        rid = uuid4()
        try:
            frames = request.get_frames()
        except Exception as ex:
            logger.error('client: failed to get frames {} [{}]'.format(rid, ex))
            return response_error(str(ex))

        try:
            sock = sock_pool.get_sock(self.con_addr)
            msg = yield from sock.request(frames, rid)
        except Exception as ex:
            logger.error('client: failed to process request {} [{}] \n {}/{} \n recycling socket'.format(rid, ex,request.service, request.method))
            sock_pool.recycle(sock, self.con_addr)
            return response_error(str(ex))

        try:
            resp = Response.from_msg(msg)
        except Exception as ex:
            logger.error('client: failed to build response {} [{}]'.format(rid, ex))
            return response_error(str(ex))

        return resp
