# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from enum import Enum

import logging
logger = logging.getLogger('tev')


class ProtoEnum(Enum):
    pass

class Status(ProtoEnum):
    OK = 200
    ERROR = 400

suported_proto_enums = {
    'Status': Status,
}

def ser_proto_enum(obj):
    tname = type(obj).__name__
    val = obj.name
    e = dict(t=tname, v=val)
    return dict(__proto_enum__=e)

def deser_proto_enum(obj):
    tname = obj['__proto_enum__']['t']
    val = obj['__proto_enum__']['v']
    T = suported_proto_enums[tname]
    return T[val]


class ProtoEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ProtoEnum):
            return ser_proto_enum(obj)
        return json.JSONEncoder.default(self, obj)

def as_proto_enum(obj):
    if '__proto_enum__' in obj:
        return deser_proto_enum(obj)
    else:
        return obj

def json_encode(obj):
    return json.dumps(obj, cls=ProtoEncoder)

def json_decode(j_str):
    return json.loads(j_str, object_hook=as_proto_enum)

def frame_encode(frame):
    return bytes(json_encode(frame), 'utf-8')

def frame_decode(frame):
    return json_decode(str(frame, 'utf-8'))

class FramedMsg:
    def get_frames (self):
        frames = getattr(self, 'frames')
        ret = []
        for frame in frames:
            ret.append(frame_encode(getattr(self, frame)))
        return ret

    @classmethod
    def from_msg(cls, msg):
        frames = cls.frames
        if len(msg) == len(frames):
            args = []
            for frame in msg:
                args.append(frame_decode(frame))
            return cls(*args)

    def to_dict (self):
        frames = self.frames
        data = dict()
        for frame in frames:
            data[frame] = getattr(self, frame)
        return json_decode(json_encode(data))

    def to_json (self):
        frames = self.frames
        data = dict()
        for frame in frames:
            data[frame] = getattr(self, frame)
        return json_encode(data)


class Response(FramedMsg):

    frames = ['meta', 'data']

    def __init__ (self, meta=dict(), data=dict()):
        self.meta = meta
        self.data = data

class Request(FramedMsg):

    frames = ['service', 'method', 'sid', 'params']

    def __init__ (self, service, method, session_id, params=dict()):
        self.service = service
        self.method = method
        self.sid = session_id
        self.params = params


def response_error (err_str):
    logger.error('response_error: {}'.format(err_str))
    meta = dict(status=Status.ERROR, error=err_str)
    return Response(meta)


def response_ok (data):
    meta = dict(status=Status.OK)
    return Response(meta, data)


def is_ok (resp):
    if 'status' in resp.meta:
        return Status.OK == resp.meta['status']
    return False


def error_message(response):
    return response.meta.get('error', 'unknown error')

def error_message_bytes(response):
    return bytes(response.meta.get('error', 'unknown error'), 'utf-8')
