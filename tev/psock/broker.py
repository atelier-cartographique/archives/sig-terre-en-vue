# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import asyncio
import zmq
from zmq.asyncio import Context, Poller
from .proto import Request, response_ok, response_error
from collections import namedtuple
from uuid import uuid4
import datetime

import logging
logger = logging.getLogger('tev')

ServiceIn = namedtuple('ServiceIn', ['name', 'iface'])
Service = namedtuple('Service', ['port', 'sock'])

def url(iface, port):
    return '{}:{}'.format(iface, port)

class Broker:

    def __init__ (self, bind, connect, port):
        self.bind = bind
        self.connect = connect
        self.port  = port
        self.services = dict()


    def set_socks (self):
        ctx = Context.instance()
        self.front_sock = ctx.socket(zmq.ROUTER)
        self.front_sock.set_string(zmq.IDENTITY,
                                  'broker.main.{}'.format(uuid4()))
        self.front_sock.bind(url(self.bind, self.port))
        self.poller = Poller()
        logger.debug('broker bound to {}'.format(url(self.bind, self.port)))


    def start (self):
        self.set_socks()
        loop = asyncio.get_event_loop()
        loop.create_task(self.start_listener())


    @asyncio.coroutine
    def process_request (self, req, header):
        if 'create_service' == req.method:
            service_name = req.params.get('service_name')
            if service_name in self.services:
                logger.debug('service already in existence {}'.format(service_name))
                service = self.services[service_name]
                port = service.port
            else:
                ctx = Context.instance()
                sock = ctx.socket(zmq.DEALER)
                sock.set_string(zmq.IDENTITY, 'dealer.{}.{}'.format(service_name, uuid4()))
                port = sock.bind_to_random_port(self.bind)
                self.services[service_name] = Service(port, sock)
                self.poller.register(sock, zmq.POLLIN)

            resp = response_ok(dict(connect=url(self.connect, port)))
            logger.debug('service {} on port {}'.format(service_name, port))

        elif 'heart_beat' == req.method:
            resp = response_ok(dict(ts=datetime.datetime.utcnow()))

        else:
            resp = response_error('Unknown method [{}]'.format(req.method))

        yield from self.front_sock.send_multipart(header + resp.get_frames())

    @asyncio.coroutine
    def dispatch_request (self, message):
        req = Request.from_msg(message[2:])
        logger.debug('dispatch_request {}/{}'.format(req.service, req.method))

        if 'broker' == req.service:
            yield from self.process_request(req, message[:2])
            return

        if req.service in self.services:
            sock = self.services[req.service].sock
            yield from sock.send_multipart(message)
            logger.debug('sent to {}'.format(req.service))
            return
        logger.error('broker: unknown service {}'.format(req.service))


    @asyncio.coroutine
    def start_listener (self):
        front = self.front_sock
        poller = self.poller
        poller.register(front, zmq.POLLIN)
        loop = asyncio.get_event_loop()

        while loop.is_running():
            events = yield from poller.poll(1000)
            if None == events:
                continue
            for sock, event in events:
                if zmq.POLLIN == event:
                    try:
                        message = yield from sock.recv_multipart()
                        if sock == front:
                            # logger.debug('broker got request => dispatching')
                            yield from self.dispatch_request(message)
                        else:
                            logger.debug('broker got response, forwarding to frontend')
                            yield from front.send_multipart(message)
                    except Exception as ex:
                        logger.error('polling error {}'.format(ex))
