# Copyright (C) 2016  Atelier Cartographique <contact@atelier-carographique.be>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .sa import Base

from ..user import models as user_models
# from ..analysis import models as analysis_models

from ..config import Config

import logging
logger = logging.getLogger('tev')

config = Config.instance().get_config('database')

engine = dict()
Session = dict()

def create_tables (engine, check_first=True):
    Base.metadata.create_all(bind=engine, checkfirst=check_first)

def configure ():
    global engine
    global Session
    dbs = ['app', 'geo']
    for db in dbs:
        check_first = True
        db_config = config.get_config(db)
        if not db_config:
            engine[db] = create_engine('sqlite:///:memory:', echo=True)
            check_first = False
            logger.warning('empty database config, goes for in-memory storage')
        else:
            db_host = db_config.get('host')
            db_user = db_config.get('user')
            db_password = db_config.get('password')
            db_db = db_config.get('database')
            db_driver = db_config.get('driver')
            dsn = '{}://{}:{}@{}/{}'.format(
                db_driver,
                db_user,
                db_password,
                db_host,
                db_db
            )
            engine[db] = create_engine(dsn, echo=False, pool_size=10, max_overflow=30)

        if db_config.get('create', False):
            create_tables(engine[db], check_first=check_first)

        Session[db] = sessionmaker(bind=engine[db])

def get_engine (name):
    if name not in engine:
        raise Exception('EngineNotConfigured [{}]'.format(name))
    return engine[name]


def get_session (name):
    if name not in Session:
        raise Exception('UnknownSession [{}]'.format(name))
    return Session[name]()
