#!/bin/bash

trim() {
    local var="$*"
    var="${var#"${var%%[![:space:]]*}"}"   # remove leading whitespace characters
    var="${var%"${var##*[![:space:]]}"}"   # remove trailing whitespace characters
    echo -n "$var"
}

BASE_IMAGE=atelier-carto/tev
VERSION=$(docker images | grep ${BASE_IMAGE} -m 1 | sed -E 's/.+\s+v([0123456789]+).*/\1/')
NEXT_VERSION=$((${VERSION} + 1))
RUN_OPTIONS="-it -u root -e LC_ALL=C.UTF-8 -e LANG=C.UTF-8 -v /:/host"

SOURCE_IMG=${BASE_IMAGE}:v${VERSION}
TARGET_IMG=${BASE_IMAGE}:v${NEXT_VERSION}

CONTAINER_ID=$(docker run ${RUN_OPTIONS} ${SOURCE_IMG}  /venv/update_tev.sh)
# echo $(trim ${CONTAINER_ID})

docker commit \
    -a 'Pierre Marchand' \
    -m 'updated terre-en-vue package' \
    $(trim ${CONTAINER_ID}) \
    ${TARGET_IMG}

echo 'deleting temporary container'
docker rm $(trim ${CONTAINER_ID})

echo 'New Version is: ' ${TARGET_IMG}
