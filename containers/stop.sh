#!/bin/bash


DOCKER_NETWORK=tev0

for c in http tile user analysis broker
do
    echo "[stopping] ${c}.tev"
    docker kill ${c}.tev
    docker rm ${c}.tev
done

docker network rm ${DOCKER_NETWORK}
