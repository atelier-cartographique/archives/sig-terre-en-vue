#!/bin/bash


BASE_IMAGE=atelier-carto/tev
# VERSION=v$(docker images | grep ${BASE_IMAGE} -m 1 | sed -E 's/.+\s+v([0123456789]+).*/\1/')
VERSION=dep0
DOCKER_IMAGE=${BASE_IMAGE}:${VERSION}
DOCKER_NETWORK=tev0

docker network create -d bridge ${DOCKER_NETWORK}

echo '[starting] broker.tev'
docker run  \
    -ti -d \
    --network ${DOCKER_NETWORK} \
    --name broker.tev \
    --workdir /var/tev \
    -v $(pwd):/var/tev \
    -e LC_ALL=C.UTF-8 -e LANG=C.UTF-8  \
    --entrypoint /venv/bin/tev \
    ${DOCKER_IMAGE} \
    --debug start broker


echo '[starting] http.tev'
docker run  \
    -ti -d \
    --name http.tev \
    --network ${DOCKER_NETWORK} \
    --expose 8080 \
    -p 8080:8080 \
    --workdir /var/tev \
    -v $(pwd):/var/tev \
    -e LC_ALL=C.UTF-8 -e LANG=C.UTF-8  \
    --entrypoint /venv/bin/tev \
    ${DOCKER_IMAGE} \
    --debug start http


for SERVICE in user analysis tile
do
    echo '[starting] ' ${SERVICE}.tev
    docker run  \
        -ti -d \
        --name ${SERVICE}.tev \
        --network ${DOCKER_NETWORK} \
        --workdir /var/tev \
        -v $(pwd):/var/tev \
        -v /home/pierre/tmp:/geodata \
        -e LC_ALL=C.UTF-8 -e LANG=C.UTF-8  \
        --entrypoint /venv/bin/tev \
        ${DOCKER_IMAGE} \
        --debug start ${SERVICE}
done
