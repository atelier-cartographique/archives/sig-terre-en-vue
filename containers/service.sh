#!/bin/bash


BASE_IMAGE=atelier-carto/tev
VERSION=$(docker images | grep ${BASE_IMAGE} -m 1 | sed -E 's/.+\s+v([0123456789]+).*/\1/')
DOCKER_IMAGE=${BASE_IMAGE}:v${VERSION}
DOCKER_NETWORK=tev0


SERVICE=$1

echo '[kill & rm] ' ${SERVICE}.tev
docker kill ${SERVICE}.tev
docker rm ${SERVICE}.tev

echo '[run] ' ${SERVICE}.tev
docker run  \
    -ti  \
    --name ${SERVICE}.tev \
    --network ${DOCKER_NETWORK} \
    --workdir /var/tev -v $(pwd):/var/tev -e LC_ALL=C.UTF-8 -e LANG=C.UTF-8  \
    --entrypoint /venv/bin/tev \
    ${DOCKER_IMAGE} \
    --debug start ${SERVICE}
