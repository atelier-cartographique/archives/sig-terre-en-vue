
from setuptools import setup, find_packages


name = 'terre-en-vue'
package = 'tev'
description = 'terre en vue'
url = 'https://phab.atelier-cartographique.be/diffusion/APPTEV/'
author = 'Pierre Marchand'
author_email = 'pierremarc07@gmail.com'
license = 'Affero GPL3'
classifiers = [
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'Topic :: Software Development :: Build Tools',
    'License :: OSI Approved :: Affero Gnu Public License 3',
    'Operating System :: POSIX',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3.4',
]
install_requires = [
    'aiohttp',
    'aiohttp-jinja2',
    'aiohttp-security',
    'aiohttp-session',
    'aiohttp-utils',
    'click',
    'filetype',
    'GeoAlchemy2',
    'gunicorn',
    'Pillow',
    'psycopg2',
    'python-mimeparse',
    'PyYAML',
    'pyzmq',
    'redis',
    'SQLAlchemy',
    'SQLAlchemy-Utils',
]

package_dir = {'tev': 'tev'}
packages = [
'tev',
    'tev.http',
        'tev.http.api',
    'tev.user',
        'tev.user.handler',
    'tev.tile',
        'tev.tile.handler',
    'tev.analysis',
        'tev.analysis.handler',
    'tev.broker',
    'tev.geomet',
    'tev.db',
    'tev.psock',
    ]
package_data = {
    'tev': [
        'http/api/swagger.yaml',
        'http/templates/*',
        'http/static/*.js',
        'http/static/*.css',
        'http/static/*.map',
        'http/static/fonts/*/*/*/*'
        ]
    }
entry_points = {'console_scripts': [ 'tev=tev.tev:commander']}

setup(
    name=name,
    version='0.1.0',
    url=url,
    license=license,
    description=description,
    author=author,
    author_email=author_email,
    packages=packages,
    package_dir=package_dir,
    package_data=package_data,
    install_requires=install_requires,
    classifiers=classifiers,
    entry_points=entry_points
)
