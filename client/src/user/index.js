import _ from 'lodash';
import App from '../proto/App';
import Action from '../proto/Action';
import UserInfo from '../component/UserInfo';
import Store from '../sys/Store';
import {User as UserModel} from '../sys/models';

class User extends App {
    constructor(...args) {
        super(...args);
        this.userInfo = new UserInfo();
        this.addFragment(this.userInfo.fragment);
    }

    get appName () {
        return 'user';
    }

    get urlPattern () {
        return '/user/:uid';
    }

    activate(options) {
        if ('uid' in options) {
            this.sys.store.dispatch(new Action('app/user@select', {
                'id': options.uid
            }));
            // this.sys.api.request('getUser', {
            //     'user-id': options.uid
            // });
            //
            UserModel.fetch(options.uid);
            this.sys.api.request('listDocument', {
                'user-id': options.uid
            });
            this.sys.api.request('listPlot', {
                'user-id': options.uid
            });
            this.sys.api.request('listQuery', {
                'user-id': options.uid
            });
            this.sys.api.request('listMedia', {
                'user-id': options.uid
            });
        }
    }
}


export default User;
