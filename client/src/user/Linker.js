import App from '../proto/App';
import Action from '../proto/Action';
import LinkerInfo from '../component/LinkerInfo';
import Store from '../sys/Store';
import {User as UserModel} from '../sys/models';

class Linker extends App {
    constructor(...args) {
        super(...args);
        this.linkerInfo = new LinkerInfo(this.sys.api);
        this.addFragment(this.linkerInfo.fragment);
    }

    get appName () {
        return 'linker';
    }

    get urlPattern () {
        return '/link/:rid';
    }

    activate(options) {
        const uid = Store.get('sys.user.id');
        if (!uid) {
            const usnsub = Store.subscribe('sys.user', () => {
                usnsub();
                this.activate(options);
            });
            return;
        }
        if ('rid' in options) {
            const requests = [
                this.sys.api.request('listDocument', { 'user-id': uid }),
                this.sys.api.request('listPlot', { 'user-id': uid }),
                this.sys.api.request('listMedia', { 'user-id': uid })
            ];

            Promise.all(requests)
                .then(() => {
                    this.sys.store.dispatch(new Action('app/linker@select', {
                        'id': options.rid
                    }));
                });
        }
    }
}


export default Linker;
