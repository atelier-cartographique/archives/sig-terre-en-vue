import _ from 'lodash';
import Action from '../proto/Action';
import Store from '../sys/Store';
import App from '../proto/App';
import QueryComp from '../component/Query';
import LinkList from '../component/LinkList';
import TevMap from '../proto/Map';

class Query extends App {
    constructor(...args) {
        super(...args);
        this.hasActivated = false;
        this.map = new TevMap();
        this.query = new QueryComp(this.map);
        this.linkList;
        this.addFragment(this.query.fragment);

    }

    get appName () {
        return 'query';
    }

    get urlPattern () {
        return '/user/:uid/query/:rid';
    }

    activate(options) {
        if (!this.hasActivated) {
            this.hasActivated = true;
            this.query.fragment.applyTo('map', function(node){
                this.map.attach(node, []);
            }, this);
        }

        if (('rid' in options)
            && ('uid' in options)) {
            if (this.linkList) {
                this.removeFragment(this.linkList.fragment);
            }
            this.linkList = new LinkList(options.rid, this.sys.api);
            this.addFragment(this.linkList.fragment);

            this.sys.store.dispatch(new Action('app/query@select', {
                'id': options.rid
            }));

            this.sys.api.request('getQuery', {
                'user-id': options.uid,
                'resource-id': options.rid
            });

            this.sys.api.request('serviceQuery', {
                'query': {
                    'qid': options.rid
                }
            }).then(response => {
                Action.dispatch(Store, 'app/query@result', response.obj);
            });
        }
    }
}

export default Query;
