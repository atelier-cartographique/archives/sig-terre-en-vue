

import Semaphore from './Semaphore';
import helpers from './helpers';

const removeElement = helpers.removeElement, addClass = helpers.addClass, removeClass = helpers.removeClass, appendText = helpers.appendText, _ = helpers._;


function Table (elem) {
    this.container = elem || body();
    this.drawer = document.createElement('div');
    addClass(this.drawer, 'drawer hidden');
    body().appendChild(this.drawer);
    this.elements = {};
    this.currentElement = null;

    const add = _.bind(this.add, this);
    _.forEach(document.querySelectorAll('[data-role="tool"]'), add);
}



export default Table;
