import _ from 'lodash';
import Action from '../proto/Action';
import App from '../proto/App';
import MediaComp from '../component/Media';
import LinkList from '../component/LinkList';

class Media extends App {
    constructor(...args) {
        super(...args);
        this.media = new MediaComp();
        this.linkList;
        this.addFragment(this.media.fragment);
    }

    get appName () {
        return 'media';
    }

    get urlPattern () {
        return '/user/:uid/media/:rid';
    }

    activate(options) {
        if (('rid' in options)
            && ('uid' in options)) {
            if (this.linkList) {
                this.removeFragment(this.linkList.fragment);
            }
            this.linkList = new LinkList(options.rid, this.sys.api);
            this.addFragment(this.linkList.fragment);

            this.sys.store.dispatch(new Action('app/media@select', {
                'id': options.rid
            }));

            this.sys.api.request('getMedia', {
                'user-id': options.uid,
                'resource-id': options.rid
            });
        }
    }
}

export default Media;
