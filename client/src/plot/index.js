import {get, forEach, find, split, startCase, zipObject} from 'lodash/fp';
import Action from '../proto/Action';
import App from '../proto/App';
import Fragment from '../proto/Fragment';
import PlotF from '../fragment/Plot';
import PlotInfoFragment from '../fragment/PlotInfo';
import LinkList from '../component/LinkList';
import PlotElement from '../component/PlotElement';
import TevMap from '../proto/Map';
import {emptyElement, removeElement, removeClass, addClass} from '../helpers';
import {User as UserModel} from '../sys/models';
import UserLabel from '../component/UserLabel';

import TerrainCouvertureSol from '../fragment/PlotElementTerrainCouvertureSol';
import TerrainRelief from '../fragment/PlotElementTerrainRelief';
import TerrainQualite from '../fragment/PlotElementTerrainQualite';
import TerrainEauVille from '../fragment/PlotElementTerrainEauVille';
import TerrainEauPluie from '../fragment/PlotElementTerrainEauPluie';
import TerrainEcologie from '../fragment/PlotElementTerrainEcologie';
import TerrainPollution from '../fragment/PlotElementTerrainPollution';

import InfraStockage from '../fragment/PlotElementInfraStockage';
import InfraCloture from '../fragment/PlotElementInfraCloture';

import AccesTaille from '../fragment/PlotElementAccesTaille';
import AccesParking from '../fragment/PlotElementAccesParking';
import AccesTransport from '../fragment/PlotElementAccesTransport';
import AccesVelo from '../fragment/PlotElementAccesVelo';

import ContexteType from '../fragment/PlotElementContexteType';
import ContexteHabitat from '../fragment/PlotElementContexteHabitat';
import ContexteMarche from '../fragment/PlotElementContexteMarche';
import ContexteCommerce from '../fragment/PlotElementContexteCommerce';



const PF = Fragment(PlotF),
    PIF = Fragment(PlotInfoFragment);


const SURVEY = Object.freeze([
    'terrain.couverture_sol',
    'terrain.relief',
    'terrain.qualite',
    'terrain.eau_ville',
    'terrain.eau_pluie',
    'terrain.ecologie',
    'terrain.pollution',

    'infra.stockage',
    'infra.cloture',

    'access.taille',
    'access.parking',
    'access.transport',
    'access.velo',

    'contexte.type',
    'contexte.habitat',
    'contexte.marche',
    'contexte.commerce'
]);

const PlotElementFragment = zipObject(SURVEY, [
    TerrainCouvertureSol,
    TerrainRelief,
    TerrainQualite,
    TerrainEauVille,
    TerrainEauPluie,
    TerrainEcologie,
    TerrainPollution,
    InfraStockage,
    InfraCloture,
    AccesTaille,
    AccesParking,
    AccesTransport,
    AccesVelo,
    ContexteType,
    ContexteHabitat,
    ContexteMarche,
    ContexteCommerce
]);


const splitter = split(/[\._]/);
function mkFragmentName (key) {
    return splitter(key).reduce((m, w) => m + startCase(w), '');
}

class Plot extends App {

    constructor(...args) {
        super(...args);
        this.plot = new PF();
        this.request = {};
        this.addFragment(this.plot);
    }


    get appName () {
        return 'plot';
    }


    get urlPattern () {
        return '/user/:uid/plot/:rid';
    }


    updateInfo (node, info) {
        const frag = new PIF();
        const area = info.area / 10000,
            isPac = info.pac,
            isResNat = info.reserve_naturelle,
            isNat2000 = info.natura2000_station;

        frag.setText('municipality.fr', get('municipality.fr', info))
            .setText('municipality.nl', get('municipality.nl', info))
            .setText('area', area.toFixed(2))
            .setText('affectation.fr', get('affectation.fr', info))
            .setText('affectation.nl', get('affectation.nl', info))
            .applyTo('reserve_naturelle_yes', node => {
                if(!isResNat) removeElement(node);
            })
            .applyTo('reserve_naturelle_no', node => {
                if(isResNat) removeElement(node);
            })
            .applyTo('natura2000_yes', node => {
                if(!isNat2000) removeElement(node);
            })
            .applyTo('natura2000_no', node => {
                if(isNat2000) removeElement(node);
            })
            .applyTo('pac_yes', node => {
                if(!isPac) removeElement(node);
            })
            .applyTo('pac_no', node => {
                if(isPac) removeElement(node);
            })
            .applyTo('proprietaire', node => {
                if(info.proprietaire) {
                    frag.setText('proprietaire', info.proprietaire);
                }
                else {
                    frag.setText('proprietaire', 'nn');
                }
            })
            .applyTo('pollution', node => {
                if(info.pollution < 0) {
                    frag.setText('pollution', 'nn');
                }
                else {
                    frag.setText('pollution', info.pollution);
                }
            });

        node.appendChild(frag.el);
    }

    updateMap (response) {
        const obj = response.obj;
        this.map.source.clear();
        this.map.addGeometry(obj.geometry);
        this.map.fitSource();
        this.plot.applyTo('results', node => {
            this.updateInfo(node, obj.properties);
        });
    }

    installSurvey (uid, rid) {
        return  (response) => {
            const elements = response.obj;
            this.plot.applyTo('elements', node => {
                emptyElement(node);

                SURVEY.forEach(key => {
                    try {
                        const finder = find(e => e.question_code === key),
                            elem = finder(elements),
                            proto = PlotElementFragment[key],
                            comp = new PlotElement(proto, elem),
                            frag = comp.fragment,
                            el = frag.el;

                        node.appendChild(el);
                        comp.on('answer', answer => {
                            const params = {
                                'user-id': uid,
                                'resource-id': rid,
                                'element': {
                                    'question_code': key,
                                    'answer_code': answer
                                }
                            };
                            this.sys.api.request('postPlotElement', params)
                                .then(r => comp.render(r.obj));
                        });

                        comp.on('reset', (element) => {
                            this.sys.api.request('deletePlotElement', {
                                'user-id': uid,
                                'resource-id': rid,
                                'element-id': element.id
                            }).then(r => comp.render());
                        });

                    }
                    catch (e) {
                        console.error(e);
                        throw (e);
                    }

                });

            });
        };
    }


    activate(options) {

        if (!this.map) {
            this.map = new TevMap();
            this.plot.applyTo('map', function(node){
                this.map.attach(node, []);
            }, this);
        }

        if (('rid' in options)
            && ('uid' in options)) {

            ['capakey', 'author', 'results', 'elements', 'links'].forEach(name => {
                this.plot.emptyNode(name);
            });

            const request = this.sys.api.request.bind(this.sys.api),
                uid = options.uid,
                rid = options.rid;

            this.sys.store.dispatch(new Action('app/plot@select', {
                'id': rid
            }));

            request('getPlot', {
                'user-id': uid,
                'resource-id': rid
            }).then(response => {
                // get synthetized info
                request('serviceCapakey', {'capakey': response.obj})
                    .then(this.updateMap.bind(this));

                this.plot.setText('capakey', response.obj.capakey);
                // get survey items
                request('listPlotElement', {
                    'user-id': uid,
                    'resource-id': rid
                }, {'fresh': true}).then(this.installSurvey(uid, rid));
            });

            UserModel
                .fetch(uid)
                .then(user => {
                    const ul = new UserLabel(user.data);
                    this.plot.applyTo('author', node => {
                        node.appendChild(ul.fragment.el);
                    });
                });


            if (this.linkList) {
                this.removeFragment(this.linkList.fragment);
            }
            this.linkList = new LinkList(options.rid, this.sys.api);
            this.addFragment(this.linkList.fragment, this.plot.node('links'));

        }
    }

}

export default Plot;
