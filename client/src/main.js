import 'babel-polyfill';
import _ from 'lodash';
import ApiClient from './sys/ApiClient';
import Sys from './sys';
import {configure as configureModels} from './sys/models';
import Navigation from './component/Navigation';

// apps
import Home from './home';
import Login from './login';
import User from './user';
import Linker from './user/Linker';
import Document from './document';
import Query from './query';
import Plot from './plot';
import Media from './media';
import FormPlot from './form/Plot';
import FormDoc from './form/Document';
import FormMedia from './form/Media';
import PlotMap from './sys/PlotMap';

// a webpack helper - yep, ugly
require('../styles/main.scss');

function main () {
    const body = document.querySelector('body'), nav = document.createElement('header'), viewport = document.createElement('div'), foot = document.createElement('footer');
    nav.setAttribute('class', 'navigation');
    viewport.setAttribute('class', 'viewport');
    foot.setAttribute('class', 'footer');

    body.appendChild(nav);
    body.appendChild(viewport);
    body.appendChild(foot);

    const api = new ApiClient();
    const apiUrl = window.location.origin + '/api/v1/swagger.json';
    api.start(apiUrl);
    configureModels(api);
    const sys = new Sys(viewport, api);

    const navigation = new Navigation(nav);

    sys.registerApp(Home);
    sys.registerApp(Login);
    sys.registerApp(User);
    sys.registerApp(Linker);
    sys.registerApp(Document);
    sys.registerApp(Query);
    sys.registerApp(Plot);
    sys.registerApp(Media);
    sys.registerApp(FormPlot);
    sys.registerApp(FormDoc);
    sys.registerApp(FormMedia);
    sys.registerApp(PlotMap);
    sys.boot();
}
export default main();
