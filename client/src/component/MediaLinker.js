import Component from '../proto/Component';
import F from '../fragment/MediaLinker';

class MediaLinker extends Component {

    constructor (opts) {
        super(F);
        this.render(opts);
    }

    mkLink (opts) {
        const model = opts.model,
            event = opts.link ? 'unlink' : 'link',
            hide = opts.link ? 'link' : 'unlink';

        this.fragment.applyTo(event, node => {
            node.style.display = 'block';
            node.addEventListener('click', e => {
                this.emit(event, model);
            });
        });
        this.fragment.applyTo(hide, node =>{
            node.style.display = 'none';
        });
    }

    render (opts) {
        this.fragment
            .setAttributes('name', {
                'href': `/user/${opts.model.user_id}/media/${opts.model.id}`
            })
            .appendText('name', opts.model.name)
            .applyTo('image', (node) => node.setAttribute('src', opts.model.url) );

        this.mkLink(opts);
    }
}

export default MediaLinker;
