import _ from 'lodash';
import Component from '../proto/Component';
import F from '../fragment/UserLabel';

class UserLabel extends Component {
    constructor(user) {
        super(F);
        this.render(user);
    }

    render(data) {
        const frag = this.fragment;
        frag.setAttributes('url', {'href': `/user/${data.id}`})
            .appendText('name', data.name);
    }
}

export default UserLabel;
