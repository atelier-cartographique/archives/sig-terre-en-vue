import _ from 'lodash';
import Component from '../proto/Component';
import F from '../fragment/Media';
import Store from '../sys/Store';

class Media extends Component {
    constructor () {
        super(F);
    }

    get paths () {
        return {
            'app.media' : 'mediaSelect',
            'api.medias': 'mediaSelect',
        };
    }

    mediaSelect(action) {
        this.mediaId = Store.get('app.media.id');
        if (this.mediaId) {
            this.media = _.find(Store.get('api.medias'), {id: this.mediaId});
        }
        if (this.media) {
            this.fragment
                .setText('name', this.media.name)
                .applyTo('image', (node) => {
                    node.setAttribute('src', this.media.url);
                });
        }
    }
}


export default Media;
