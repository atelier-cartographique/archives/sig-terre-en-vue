import _ from 'lodash';
import Component from '../proto/Component';
import Action from '../proto/Action';
import F from '../fragment/LinkList';
import Link from '../fragment/Link';
import Store from '../sys/Store';

import DocumentThumbnail from '../component/DocumentThumbnail';
import QueryThumbnail from '../component/QueryThumbnail';
import MediaThumbnail from '../component/MediaThumbnail';

const RESOURCE_TYPE = {
    'document': DocumentThumbnail,
    'query': QueryThumbnail,
    'media': MediaThumbnail
};

class LinkList extends Component{
    constructor(targetId, api) {
        super(F);
        this.targetId = targetId;
        this.api = api;
        api.request('listLink', {
            'target-id': targetId
        }, {
            'fresh': true
        }).then(this.renderLinks.bind(this));
    }

    get paths () {
        return {
            'comp.link': 'select'
        };
    }

    select(action) {

    }

    renderLinks(response) {
        const resources = response.obj;
        this.fragment.applyTo('links', node => {
            _.forEach(resources, resource => {
                const comp = new RESOURCE_TYPE[resource.type](resource);
                node.appendChild(comp.fragment.el);
            });
        });

        this.fragment.applyTo('newLink', node => {
            node.setAttribute('href', `/link/${this.targetId}`);
        });
    }

}

export default LinkList;
