import _ from 'lodash';
import Component from '../proto/Component';
import F from '../fragment/Document';
import Store from '../sys/Store';

class Document extends Component {
    constructor () {
        super(F);
    }

    get paths () {
        return {
            'app.document.id' : 'docSelect',
            'app.document.user' : 'userSelect',
        };
    }

    docSelect(action) {
        this.docId = Store.get('app.document.id');
        if (this.docId) {
            this.doc = _.find(Store.get('api.documents'), {id: this.docId});
        }
        if (this.doc) {
            const md = this.doc.transformed;
            this.fragment
                .setText('name', this.doc.name)
                .applyTo('body', (node) => {
                    node.innerHTML = md;
                });



            const userId = Store.get('sys.user.id');
            if (userId && (userId === this.doc.user_id)) {
                this.fragment.setAttributes('editLink', {
                    href: `/form/document/${this.doc.id}`
                });
            }
            else {
                this.fragment.setAttributes('editLink', {
                    'class': 'hidden'
                });
            }
        }
    }

    userSelect (action) {
        const uid = Store.get('app.document.user'),
            user = _.find(Store.get('api.users'), {id: uid});
        if (user) {
            this.fragment
                .applyTo('author', node => {
                    node.setAttribute('href', `/user/${user.id}`);
                })
                .setText('author', user.name);
        }
    }
}


export default Document;
