import {find, filter, forEach, concat} from 'lodash/fp';
import Component from '../proto/Component';
import F from '../fragment/LinkerInfo';
import Store from '../sys/Store';
import DocumentLinker from './DocumentLinker';
import PlotLinker from './PlotLinker';
import MediaLinker from './MediaLinker';
import {resourceUrl} from '../helpers';

function filterUserId (uid) {
    return filter({'user_id': uid});
}

function findId (id) {
    return find({'id': id});
}

function findSourceId (sid) {
    return find({'source_id': sid});
}


function findResource (rid) {
    const plots = Store.get('api.plots'),
        documents = Store.get('api.documents'),
        medias = Store.get('api.medias');
    const models = concat(plots, documents, medias);
    const results = findId(rid)(models);
    return results;
}


const linkerPrototypes = [
    ['documents', DocumentLinker],
    ['medias', MediaLinker],
    ['plots', PlotLinker]
];

class LinkerInfo extends Component {
    constructor (api) {
        super(F);
        this.api = api;
    }

    get paths () {
        return {
            'app.linker' : 'linkerSelect',
            // 'api.documents': 'docChange',
            // 'api.plots': 'plotChange',
            // 'api.medias': 'mediaChange',
        };
    }

    linkerSelect(action) {
        this.resourceId = Store.get('app.linker.id');
        const model = findResource(this.resourceId);
        if (model) {
            this.fragment
                .setText('targetName', model.name)
                .setAttributes('targetLink', {href: resourceUrl(model)});

            forEach(rec => {
                const [type, Proto] = rec;
                this.update(type, Proto);
            }, linkerPrototypes);
        }
    }

    deleteLink (model, comp) {
        this.api.request('deleteLink', {
            'target-id': this.resourceId,
            'source-id': model.id
        }).then(response => {
            this._links = null;
            comp.mkLink({model});
        });
    }

    createLink (model, comp) {
        this.api.request('postLink', {
            'target-id': this.resourceId,
            'source-id': model.id
        }).then(response => {
            this._links = null;
            comp.mkLink({model, link:response.obj});
        });
    }

    withData (fn) {
        if (this.resourceId) {
            const user = Store.get('sys.user');
            if (!user) {
                throw (new Error('NotLogged'));
            }

            if (!this._links) {
                if (this._pendings) {
                    this._pendings.push(fn);
                }
                else {
                    this._pendings = [fn];
                    const options = {'fresh': true},
                        params = {'target-id': this.resourceId};
                    this.api.request('listLink', params, options).then(response => {
                        this._links = response.obj;
                        this._pendings.forEach(pfn =>
                            pfn(user.id, this.resourceId, response.obj));
                        this._pendings= null;
                    });
                }
            }
            else {
                fn(user.id, this.resourceId, this._links);
            }
        }

    }

    update (typeName, CompPrototype) {

        this.withData((uid, rid, links) => {
            const models = Store.get(`api.${typeName}`, []),
                append = node => {
                    filterUserId(uid)(models)
                        .forEach(model => {
                            const link = findId(model.id)(links),
                                opts = { model, link },
                                comp = new CompPrototype(opts);
                            node.appendChild(comp.fragment.el);

                            comp.on('unlink', model =>
                                            this.deleteLink(model, comp));
                            comp.on('link', model =>
                                            this.createLink(model, comp));
                        });
                };

            this.fragment
                .emptyNode(typeName)
                .applyTo(typeName, append);
        });
    }

    // docChange() {
    //     this.update('documents', DocumentLinker);
    // }
    //
    // mediaChange() {
    //     this.update('medias', MediaLinker);
    // }
    //
    // plotChange() {
    //     this.update('plots', PlotLinker);
    // }
}


export default LinkerInfo;
