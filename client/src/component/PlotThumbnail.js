import Component from '../proto/Component';
import F from '../fragment/PlotThumbnail';

class PlotThumbnail extends Component{
    constructor(plot) {
        super(F);
        this.render(plot);
    }

    render(plot) {
        this.fragment
            .setAttributes('name', {
                'href': `/user/${plot.user_id}/plot/${plot.id}`
            })
            .appendText('name', plot.name);
    }
}

export default PlotThumbnail;
