import _ from 'lodash';
import Component from '../proto/Component';
import F from '../fragment/UserInfo';
import Store from '../sys/Store';
import DocumentThumbnail from './DocumentThumbnail';
import QueryThumbnail from './QueryThumbnail';
import PlotThumbnail from './PlotThumbnail';
import MediaThumbnail from './MediaThumbnail';


class UserInfo extends Component {
    constructor () {
        super(F);
    }

    get paths () {
        return {
            'app.user' : 'userSelect',
            'api.users': 'userSelect',
            'api.documents': 'docChange',
            'api.queries': 'queryChange',
            'api.plots': 'plotChange',
            'api.medias': 'mediaChange',
        };
    }

    userSelect(action) {
        this.userId = Store.get('app.user.id');
        if (this.userId) {
            this.user = _.find(Store.get('api.users'), {id: this.userId});
            this.fragment.setText('name', !!this.user ? this.user.name : '');
        }
    }

    docChange() {
        if (!this.userId) {
            return;
        }
        const uid = this.userId, docs = Store.get('api.documents', []);
        const append = node => {
            _.forEach(_.filter(docs, {'user_id': uid}), doc => {
                const documentComp = new DocumentThumbnail(doc);
                node.appendChild(documentComp.fragment.el);
            });
        };
        this.fragment
            .emptyNode('documents')
            .applyTo('documents', append);
    }

    queryChange() {
        if (!this.userId) {
            return;
        }
        const uid = this.userId, queries = Store.get('api.queries', []);
        const append = node => {
            _.forEach(_.filter(queries, {'user_id': uid}), query => {
                const queryComp = new QueryThumbnail(query);
                node.appendChild(queryComp.fragment.el);
            });
        };
        this.fragment
            .emptyNode('queries')
            .applyTo('queries', append);
    }

    mediaChange() {
        if (!this.userId) {
            return;
        }
        const uid = this.userId, medias = Store.get('api.medias', []);
        const append = node => {
            _.forEach(_.filter(medias, {'user_id': uid}), (media) => {
                const mediaComp = new MediaThumbnail(media);
                node.appendChild(mediaComp.fragment.el);
            });
        };
        this.fragment
            .emptyNode('medias')
            .applyTo('medias', append);
    }

    plotChange() {
        if (!this.userId) {
            return;
        }
        const uid = this.userId, plots = Store.get('api.plots', []);
        const append = node => {
            _.forEach(_.filter(plots, {'user_id': uid}), plot => {
                const plotComp = new PlotThumbnail(plot);
                node.appendChild(plotComp.fragment.el);
            });
        };
        this.fragment
            .emptyNode('plots')
            .applyTo('plots', append);
    }
}


export default UserInfo;
