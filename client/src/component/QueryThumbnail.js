import _ from 'lodash';
import Component from '../proto/Component';
import F from '../fragment/QueryThumbnail';

class QueryThumbnail extends Component{
    constructor(query) {
        super(F);
        this.render(query);
    }

    render(query) {
        this.fragment
            .setAttributes('name', {
                'href': `/user/${query.user_id}/query/${query.id}`
            })
            .appendText('name', query.name);
        if (false /* query.geo_column */) {
            // TODO: implement map
        }
        else {
            this.fragment
                .appendText('body', query.description, {tag:'p'})
                .appendText('body', query.body, {
                    tag:'code',
                    attributes: {class:'code'}});
        }
    }
}

export default QueryThumbnail;
