import _ from 'lodash';
import Component from '../proto/Component';
import F from '../fragment/MediaThumbnail';

class MediaThumbnail extends Component{
    constructor(media) {
        super(F);
        this.render(media);
    }

    render(media) {
        this.fragment
            .setAttributes('name', {
                'href': `/user/${media.user_id}/media/${media.id}`
            })
            .appendText('name', media.name)
            .applyTo('image', (node) => node.setAttribute('src', media.url) );
    }
}

export default MediaThumbnail;
