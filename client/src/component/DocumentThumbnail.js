import _ from 'lodash';
import Component from '../proto/Component';
import F from '../fragment/DocumentThumbnail';

class DocumentThumbnail extends Component{
    constructor(doc) {
        super(F);
        this.render(doc);
    }

    render(doc) {
        this.fragment
            .setAttributes('name', {
                'href': `/user/${doc.user_id}/document/${doc.id}`
            })
            .appendText('name', doc.name)
            .appendText('body', doc.body);
    }
}

export default DocumentThumbnail;
