import _ from 'lodash';
import Component from '../proto/Component';
import F from '../fragment/Query';
import Store from '../sys/Store';

class Query extends Component {
    constructor(tmap) {
        super(F);
        this.map = tmap;
    }

    get paths () {
        return {
            'app.query' : 'querySelect',
            'app.query.result' : 'queryResult',
            'api.queries': 'querySelect'
        };
    }

    querySelect(action) {
        this.queryId = Store.get('app.query.id');
        if (this.queryId) {
            this.map.source.clear();
            this.query = _.find(Store.get('api.queries'), {id: this.queryId});
        }
        if (this.query) {
            this.fragment.setText('name', this.query.name);
            this.fragment.setText('description', this.query.description);
        }
    }

    queryResult(action) {
        const result = Store.get('app.query.result');
        if (result && result.results) {
            if (result.geom) {
                const geom = result.geom, results = result.results;
                for (let i = 0 ; i < results.length; i++) {
                    this.map.addGeometry(results[i][geom]);
                }
                this.map.fitSource();
            }
        }
    }
}



export default Query;
