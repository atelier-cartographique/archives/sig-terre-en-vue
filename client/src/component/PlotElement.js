import {forEach} from 'lodash/fp';
import Fragment from '../proto/Fragment';
import ResetFragmentMixin from '../fragment/PlotElementReset';
import Component from '../proto/Component';
import {addClass, removeClass, getData, removeElement} from '../helpers';
import Store from '../sys/Store';
import moment from 'moment';

function setClose (node) {
    addClass(removeClass(node, 'open'), 'close');
}

function setOpen (node) {
    addClass(removeClass(node, 'close'), 'open');
}

const ResetFragment = Fragment(ResetFragmentMixin);


class PlotElement extends Component{

    constructor (FragmentClass, element) {
        super(FragmentClass);
        this.render(element);
    }

    appendReset (element) {
        const frag = new ResetFragment();
        this.fragment.applyTo('element', node => {
            node.insertBefore(frag.el, node.firstChild);
        });
        frag.applyTo('reset', node => {
            node.addEventListener('click', event => this.emit('reset', element));
        });
        this.resetFragment = frag;
    }

    render (element) {
        if (undefined === element) {
            return this.renderOpen();
        }
        const date = moment(element.ts).calendar(),
            user = Store.get('sys.user');
        this.fragment
            .applyTo('element', setClose)
            .setText('date', date)
            .applyTo('options', node => {
                const selector = `[data-option=${element.answer_code}]`;
                const el = node.querySelector(selector);
                if (el) {
                    addClass(el, 'selected');
                }
            });

        if (user) {
            this.appendReset(element);
        }
    }


    renderOpen () {
        if (this.resetFragment) {
            this.resetFragment.clear();
            this.resetFragment = null;
        }
        this.fragment
            .applyTo('element', setOpen)
            .setText('date', '')
            .applyTo('options', node => {
                forEach(el => {
                    removeClass(el, 'selected');
                    el.addEventListener('click', e => {
                        this.emit('answer', getData(el, 'option'));
                    });
                })(node.querySelectorAll('[data-option]'));
            });
    }

}

export default PlotElement;
