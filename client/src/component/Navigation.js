import _ from 'lodash';
import Component from '../proto/Component';
import F from '../fragment/Navigation';
import Store from '../sys/Store';
import {removeClass, addClass, hasClass} from '../helpers';


const toggle = (cond, mask) => node => {
    if (mask) {
        if (cond) {
            addClass(node, 'hidden');
        }
        else {
            removeClass(node, 'hidden');
        }
    }
    else {
        if (cond) {
            removeClass(node, 'hidden');
        }
        else {
            addClass(node, 'hidden');
        }
    }
};

class Navigation extends Component {
    constructor(element) {
        super(F);
        this.user = Store.get('sys.user');
        this.render();
        this.setListeners();
        element.appendChild(this.fragment.el);
    }

    get paths () {
        return  {
            'sys.user': 'userChange'
        };
    }

    userChange() {
        this.user = Store.get('sys.user');
        this.render();
    }

    setListeners () {
        const frag = this.fragment;
        frag.applyTo('toolsToggle', node => {
            node.addEventListener('click', event => {
                frag.applyTo('tools', tools => {
                    toggle(!hasClass(tools, 'hidden'), true)(tools);
                });
            });
        });
    }

    render() {
        const frag = this.fragment,
            user = this.user,
            logged = !!(user && user.id);

        frag.applyTo('login', toggle(logged, true));
        frag.applyTo('logout', toggle(logged, false));
        // frag.applyTo('tools', node => addClass(node, 'hidden'));
    }
}


export default Navigation;
