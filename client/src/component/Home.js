import _ from 'lodash';
import Component from '../proto/Component';
import F from '../fragment/Home';
import Store from '../sys/Store';
import UserLabel from './UserLabel';

class Home extends Component {
    constructor(users) {
        super(F);
        this.render(users || []);
    }

    render() {
        const users = Store.get('api.users', []), fragment = this.fragment;
        fragment
            .emptyNode('users')
            .applyTo('users', node => {
                _.forEach(users, user => {
                    const label = new UserLabel(user), frag = label.fragment;
                    node.appendChild(frag.el);
                });
            });
    }
}

Home.prototype.paths = {
    'api.users': 'render'
};

export default Home;
