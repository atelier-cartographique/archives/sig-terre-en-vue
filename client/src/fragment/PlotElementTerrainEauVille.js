

// PlotElementTerrainEauVille fragment mixin

var  PlotElementTerrainEauVille = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n152 = document.createElement("div");
    n152.setAttribute("class", "plot-element terrain");
    n152.setAttribute("data-anchor", "element");
elements.push(n152)
root.appendChild(n152);
anchors["element"] = n152;
n152.appendChild(document.createTextNode("        "));

let n153 = document.createElement("h3");
elements.push(n153)
n152.appendChild(n153);
n153.appendChild(document.createTextNode("Access a l'eau de ville"));
n152.appendChild(document.createTextNode("        "));

let n154 = document.createElement("span");
    n154.setAttribute("class", "date");
    n154.setAttribute("data-anchor", "date");
elements.push(n154)
n152.appendChild(n154);
anchors["date"] = n154;
n152.appendChild(document.createTextNode("        "));

let n155 = document.createElement("div");
    n155.setAttribute("class", "options");
    n155.setAttribute("data-anchor", "options");
elements.push(n155)
n152.appendChild(n155);
anchors["options"] = n155;
n155.appendChild(document.createTextNode("            "));

let n156 = document.createElement("span");
    n156.setAttribute("data-option", "oui");
elements.push(n156)
n155.appendChild(n156);
n156.appendChild(document.createTextNode("oui"));
n155.appendChild(document.createTextNode("            "));

let n157 = document.createElement("span");
    n157.setAttribute("data-option", "non");
elements.push(n157)
n155.appendChild(n157);
n157.appendChild(document.createTextNode("non"));
n155.appendChild(document.createTextNode("        "));
n152.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementTerrainEauVille;

