

// UserInfo fragment mixin

var  UserInfo = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("name");
names.push("queries");
names.push("documents");
names.push("plots");
names.push("medias");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n39 = document.createElement("h1");
    n39.setAttribute("data-anchor", "name");
    n39.setAttribute("class", "username-big");
elements.push(n39)
root.appendChild(n39);
anchors["name"] = n39;
root.appendChild(document.createTextNode("    "));

let n40 = document.createElement("div");
    n40.setAttribute("data-anchor", "queries");
    n40.setAttribute("class", "list-queries resources");
elements.push(n40)
root.appendChild(n40);
anchors["queries"] = n40;
n40.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n41 = document.createElement("div");
    n41.setAttribute("data-anchor", "documents");
    n41.setAttribute("class", "list-document resources");
elements.push(n41)
root.appendChild(n41);
anchors["documents"] = n41;
n41.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n42 = document.createElement("div");
    n42.setAttribute("data-anchor", "plots");
    n42.setAttribute("class", "list-plot resources");
elements.push(n42)
root.appendChild(n42);
anchors["plots"] = n42;
n42.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n43 = document.createElement("div");
    n43.setAttribute("data-anchor", "medias");
    n43.setAttribute("class", "list-media resources");
elements.push(n43)
root.appendChild(n43);
anchors["medias"] = n43;
n43.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default UserInfo;

