

// FormDocument fragment mixin

var  FormDocument = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("name");
names.push("body");
names.push("submit");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n309 = document.createElement("h1");
elements.push(n309)
root.appendChild(n309);
n309.appendChild(document.createTextNode("Formulaire document"));
root.appendChild(document.createTextNode("    "));

let n310 = document.createElement("fieldset");
elements.push(n310)
root.appendChild(n310);
n310.appendChild(document.createTextNode("    "));

let n311 = document.createElement("legend");
elements.push(n311)
n310.appendChild(n311);
n311.appendChild(document.createTextNode("titre"));
n310.appendChild(document.createTextNode("        "));

let n312 = document.createElement("input");
    n312.setAttribute("data-anchor", "name");
    n312.setAttribute("type", "text");
    n312.setAttribute("class", "large");
    n312.setAttribute("name", "name");
    n312.setAttribute("value", "");
    n312.setAttribute("required", "None");
elements.push(n312)
n310.appendChild(n312);
anchors["name"] = n312;
n310.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n313 = document.createElement("fieldset");
elements.push(n313)
root.appendChild(n313);
n313.appendChild(document.createTextNode("        "));

let n314 = document.createElement("legend");
elements.push(n314)
n313.appendChild(n314);
n314.appendChild(document.createTextNode("contenu"));
n313.appendChild(document.createTextNode("        "));

let n315 = document.createElement("textarea");
    n315.setAttribute("data-anchor", "body");
    n315.setAttribute("name", "body");
    n315.setAttribute("rows", "24");
    n315.setAttribute("cols", "72");
elements.push(n315)
n313.appendChild(n315);
anchors["body"] = n315;
n313.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("	"));

let n316 = document.createElement("div");
    n316.setAttribute("class", "button-box");
elements.push(n316)
root.appendChild(n316);
n316.appendChild(document.createTextNode("		"));

let n317 = document.createElement("div");
    n317.setAttribute("class", "button");
    n317.setAttribute("data-anchor", "submit");
elements.push(n317)
n316.appendChild(n317);
anchors["submit"] = n317;
n317.appendChild(document.createTextNode("Save"));
n316.appendChild(document.createTextNode("	"));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default FormDocument;

