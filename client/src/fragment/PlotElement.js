

// PlotElement fragment mixin

var  PlotElement = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("question");
names.push("answer");
names.push("submit");
//> end of generated code
    return names;
};


build () {
    let anchors = this.anchors;
    let root = this.el;

//< generated code - if anything wrong in there, check the template first

let n524 = document.createElement("div");
    n524.setAttribute("class", "plot-element");
    n524.setAttribute("data-anchor", "element");
root.appendChild(n524);
anchors["element"] = n524;

let n525 = document.createElement("span");
    n525.setAttribute("class", "date");
    n525.setAttribute("data-anchor", "date");
n524.appendChild(n525);
anchors["date"] = n525;

let n526 = document.createElement("div");
    n526.setAttribute("class", "question");
    n526.setAttribute("data-anchor", "question");
n524.appendChild(n526);
anchors["question"] = n526;

let n527 = document.createElement("div");
    n527.setAttribute("class", "answer");
    n527.setAttribute("data-anchor", "answer");
n524.appendChild(n527);
anchors["answer"] = n527;

let n528 = document.createElement("div");
    n528.setAttribute("class", "button-block");
n524.appendChild(n528);

let n529 = document.createElement("span");
    n529.setAttribute("class", "button button-submit");
    n529.setAttribute("data-anchor", "submit");
n528.appendChild(n529);
anchors["submit"] = n529;
n529.appendChild(document.createTextNode("submit"));
//> end of generated code

    return this;
};

}

export default PlotElement;

