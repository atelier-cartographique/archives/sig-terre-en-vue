

// PlotElementContexteHabitat fragment mixin

var  PlotElementContexteHabitat = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n245 = document.createElement("div");
    n245.setAttribute("class", "plot-element contexte");
    n245.setAttribute("data-anchor", "element");
elements.push(n245)
root.appendChild(n245);
anchors["element"] = n245;
n245.appendChild(document.createTextNode("        "));

let n246 = document.createElement("h3");
elements.push(n246)
n245.appendChild(n246);
n246.appendChild(document.createTextNode("Proximité d'habitations"));
n245.appendChild(document.createTextNode("        "));

let n247 = document.createElement("span");
    n247.setAttribute("class", "date");
    n247.setAttribute("data-anchor", "date");
elements.push(n247)
n245.appendChild(n247);
anchors["date"] = n247;
n245.appendChild(document.createTextNode("        "));

let n248 = document.createElement("div");
    n248.setAttribute("class", "options");
    n248.setAttribute("data-anchor", "options");
elements.push(n248)
n245.appendChild(n248);
anchors["options"] = n248;
n248.appendChild(document.createTextNode("            "));

let n249 = document.createElement("span");
    n249.setAttribute("data-option", "immediat");
elements.push(n249)
n248.appendChild(n249);
n249.appendChild(document.createTextNode("immediat ("));
n249.appendChild(document.createTextNode("<"));
n249.appendChild(document.createTextNode(" 100m)"));
n248.appendChild(document.createTextNode("            "));

let n250 = document.createElement("span");
    n250.setAttribute("data-option", "proche");
elements.push(n250)
n248.appendChild(n250);
n250.appendChild(document.createTextNode("proche ("));
n250.appendChild(document.createTextNode("<"));
n250.appendChild(document.createTextNode(" 500m)"));
n248.appendChild(document.createTextNode("            "));

let n251 = document.createElement("span");
    n251.setAttribute("data-option", "lointain");
elements.push(n251)
n248.appendChild(n251);
n251.appendChild(document.createTextNode("loin (au-dela)"));
n248.appendChild(document.createTextNode("        "));
n245.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementContexteHabitat;

