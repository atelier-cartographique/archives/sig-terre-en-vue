

// PlotElementTerrainSoleil fragment mixin

var  PlotElementTerrainSoleil = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n188 = document.createElement("div");
    n188.setAttribute("class", "plot-element terrain");
    n188.setAttribute("data-anchor", "element");
elements.push(n188)
root.appendChild(n188);
anchors["element"] = n188;
n188.appendChild(document.createTextNode("        "));

let n189 = document.createElement("h3");
elements.push(n189)
n188.appendChild(n189);
n189.appendChild(document.createTextNode("Exposition au soleil"));
n188.appendChild(document.createTextNode("        "));

let n190 = document.createElement("span");
    n190.setAttribute("class", "date");
    n190.setAttribute("data-anchor", "date");
elements.push(n190)
n188.appendChild(n190);
anchors["date"] = n190;
n188.appendChild(document.createTextNode("        "));

let n191 = document.createElement("div");
    n191.setAttribute("class", "options");
    n191.setAttribute("data-anchor", "options");
elements.push(n191)
n188.appendChild(n191);
anchors["options"] = n191;
n191.appendChild(document.createTextNode("            "));

let n192 = document.createElement("span");
    n192.setAttribute("data-option", "bon");
elements.push(n192)
n191.appendChild(n192);
n192.appendChild(document.createTextNode("bonne (3/4 terrain à midi)"));
n191.appendChild(document.createTextNode("            "));

let n193 = document.createElement("span");
    n193.setAttribute("data-option", "moyen");
elements.push(n193)
n191.appendChild(n193);
n193.appendChild(document.createTextNode("moyenne (entre 3/4 et 1/2 terrain à midi)"));
n191.appendChild(document.createTextNode("            "));

let n194 = document.createElement("span");
    n194.setAttribute("data-option", "mauvais");
elements.push(n194)
n191.appendChild(n194);
n194.appendChild(document.createTextNode("mauvaise (<1/2 terrain à midi)"));
n191.appendChild(document.createTextNode("        "));
n188.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementTerrainSoleil;

