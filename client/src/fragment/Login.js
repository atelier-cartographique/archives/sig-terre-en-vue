

// Login fragment mixin

var  Login = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first

//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first

let n278 = document.createElement("section");
elements.push(n278)
root.appendChild(n278);

let n279 = document.createElement("form");
    n279.setAttribute("name", "login");
    n279.setAttribute("method", "post");
    n279.setAttribute("accept-charset", "utf-8");
elements.push(n279)
n278.appendChild(n279);
n279.appendChild(document.createTextNode("    "));

let n280 = document.createElement("fieldset");
elements.push(n280)
n279.appendChild(n280);
n280.appendChild(document.createTextNode("    "));

let n281 = document.createElement("legend");
elements.push(n281)
n280.appendChild(n281);
n281.appendChild(document.createTextNode("login"));
n280.appendChild(document.createTextNode("        "));

let n282 = document.createElement("p");
elements.push(n282)
n280.appendChild(n282);
n282.appendChild(document.createTextNode("            "));

let n283 = document.createElement("input");
    n283.setAttribute("type", "email");
    n283.setAttribute("name", "email");
    n283.setAttribute("placeholder", "yourname@email.com");
    n283.setAttribute("required", "None");
elements.push(n283)
n282.appendChild(n283);
n283.appendChild(document.createTextNode("        "));
n282.appendChild(document.createTextNode("        "));

let n284 = document.createElement("p");
elements.push(n284)
n282.appendChild(n284);
n284.appendChild(document.createTextNode("            "));

let n285 = document.createElement("input");
    n285.setAttribute("type", "password");
    n285.setAttribute("name", "password");
    n285.setAttribute("placeholder", "password");
    n285.setAttribute("required", "None");
elements.push(n285)
n284.appendChild(n285);
n285.appendChild(document.createTextNode("        "));
n284.appendChild(document.createTextNode("    "));
n282.appendChild(document.createTextNode("    "));

let n286 = document.createElement("input");
    n286.setAttribute("type", "submit");
    n286.setAttribute("name", "login");
    n286.setAttribute("value", "submit");
elements.push(n286)
n282.appendChild(n286);
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default Login;

