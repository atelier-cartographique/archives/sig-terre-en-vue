

// PlotElementContexteType fragment mixin

var  PlotElementContexteType = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n237 = document.createElement("div");
    n237.setAttribute("class", "plot-element contexte");
    n237.setAttribute("data-anchor", "element");
elements.push(n237)
root.appendChild(n237);
anchors["element"] = n237;
n237.appendChild(document.createTextNode("        "));

let n238 = document.createElement("h3");
elements.push(n238)
n237.appendChild(n238);
n238.appendChild(document.createTextNode("Type de quartier"));
n237.appendChild(document.createTextNode("        "));

let n239 = document.createElement("span");
    n239.setAttribute("class", "date");
    n239.setAttribute("data-anchor", "date");
elements.push(n239)
n237.appendChild(n239);
anchors["date"] = n239;
n237.appendChild(document.createTextNode("        "));

let n240 = document.createElement("div");
    n240.setAttribute("class", "options");
    n240.setAttribute("data-anchor", "options");
elements.push(n240)
n237.appendChild(n240);
anchors["options"] = n240;
n240.appendChild(document.createTextNode("            "));

let n241 = document.createElement("span");
    n241.setAttribute("data-option", "residentiel");
elements.push(n241)
n240.appendChild(n241);
n241.appendChild(document.createTextNode("résidentiel"));
n240.appendChild(document.createTextNode("            "));

let n242 = document.createElement("span");
    n242.setAttribute("data-option", "industriel");
elements.push(n242)
n240.appendChild(n242);
n242.appendChild(document.createTextNode("industriel"));
n240.appendChild(document.createTextNode("            "));

let n243 = document.createElement("span");
    n243.setAttribute("data-option", "bureau");
elements.push(n243)
n240.appendChild(n243);
n243.appendChild(document.createTextNode("bureau"));
n240.appendChild(document.createTextNode("            "));

let n244 = document.createElement("span");
    n244.setAttribute("data-option", "mixte");
elements.push(n244)
n240.appendChild(n244);
n244.appendChild(document.createTextNode("mixte"));
n240.appendChild(document.createTextNode("        "));
n237.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementContexteType;

