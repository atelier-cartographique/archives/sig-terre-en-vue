

// PlotElementTerrainPollution fragment mixin

var  PlotElementTerrainPollution = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n167 = document.createElement("div");
    n167.setAttribute("class", "plot-element terrain");
    n167.setAttribute("data-anchor", "element");
elements.push(n167)
root.appendChild(n167);
anchors["element"] = n167;
n167.appendChild(document.createTextNode("        "));

let n168 = document.createElement("h3");
elements.push(n168)
n167.appendChild(n168);
n168.appendChild(document.createTextNode("Présence de pollutions visibles"));
n167.appendChild(document.createTextNode("        "));

let n169 = document.createElement("span");
    n169.setAttribute("class", "date");
    n169.setAttribute("data-anchor", "date");
elements.push(n169)
n167.appendChild(n169);
anchors["date"] = n169;
n167.appendChild(document.createTextNode("        "));

let n170 = document.createElement("div");
    n170.setAttribute("class", "options");
    n170.setAttribute("data-anchor", "options");
elements.push(n170)
n167.appendChild(n170);
anchors["options"] = n170;
n170.appendChild(document.createTextNode("            "));

let n171 = document.createElement("span");
    n171.setAttribute("data-option", "oui");
elements.push(n171)
n170.appendChild(n171);
n171.appendChild(document.createTextNode("oui"));
n170.appendChild(document.createTextNode("            "));

let n172 = document.createElement("span");
    n172.setAttribute("data-option", "non");
elements.push(n172)
n170.appendChild(n172);
n172.appendChild(document.createTextNode("non"));
n170.appendChild(document.createTextNode("        "));
n167.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementTerrainPollution;

