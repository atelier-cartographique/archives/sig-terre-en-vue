

// MediaLinker fragment mixin

var  MediaLinker = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("link");
names.push("unlink");
names.push("name");
names.push("image");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n10 = document.createElement("div");
    n10.setAttribute("class", "thumbnail-resource thumbnail-document");
elements.push(n10)
root.appendChild(n10);
n10.appendChild(document.createTextNode("        "));

let n11 = document.createElement("div");
    n11.setAttribute("class", "button-linker link");
    n11.setAttribute("data-anchor", "link");
elements.push(n11)
n10.appendChild(n11);
anchors["link"] = n11;
n11.appendChild(document.createTextNode("            Lier"));
n11.appendChild(document.createTextNode("        "));
n10.appendChild(document.createTextNode("        "));

let n12 = document.createElement("div");
    n12.setAttribute("class", "button-linker unlink");
    n12.setAttribute("data-anchor", "unlink");
elements.push(n12)
n10.appendChild(n12);
anchors["unlink"] = n12;
n12.appendChild(document.createTextNode("            Délier"));
n12.appendChild(document.createTextNode("        "));
n10.appendChild(document.createTextNode("        "));

let n13 = document.createElement("div");
    n13.setAttribute("class", "thumbnail-title");
elements.push(n13)
n10.appendChild(n13);
n13.appendChild(document.createTextNode("            "));

let n14 = document.createElement("a");
    n14.setAttribute("data-anchor", "name");
    n14.setAttribute("href", "");
elements.push(n14)
n13.appendChild(n14);
anchors["name"] = n14;
n13.appendChild(document.createTextNode("        "));
n10.appendChild(document.createTextNode("        "));

let n15 = document.createElement("div");
    n15.setAttribute("class", "thumbnail-content");
elements.push(n15)
n10.appendChild(n15);
n15.appendChild(document.createTextNode("            "));

let n16 = document.createElement("img");
    n16.setAttribute("src", "x");
    n16.setAttribute("data-anchor", "image");
elements.push(n16)
n15.appendChild(n16);
anchors["image"] = n16;
n15.appendChild(document.createTextNode("        "));
n10.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default MediaLinker;

