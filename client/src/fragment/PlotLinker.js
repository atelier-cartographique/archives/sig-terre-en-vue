

// PlotLinker fragment mixin

var  PlotLinker = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("link");
names.push("unlink");
names.push("name");
names.push("body");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n17 = document.createElement("div");
    n17.setAttribute("class", "thumbnail-resource thumbnail-plot");
elements.push(n17)
root.appendChild(n17);
n17.appendChild(document.createTextNode("        "));

let n18 = document.createElement("div");
    n18.setAttribute("class", "button-linker link");
    n18.setAttribute("data-anchor", "link");
elements.push(n18)
n17.appendChild(n18);
anchors["link"] = n18;
n18.appendChild(document.createTextNode("            Lier"));
n18.appendChild(document.createTextNode("        "));
n17.appendChild(document.createTextNode("        "));

let n19 = document.createElement("div");
    n19.setAttribute("class", "button-linker unlink");
    n19.setAttribute("data-anchor", "unlink");
elements.push(n19)
n17.appendChild(n19);
anchors["unlink"] = n19;
n19.appendChild(document.createTextNode("            Délier"));
n19.appendChild(document.createTextNode("        "));
n17.appendChild(document.createTextNode("        "));

let n20 = document.createElement("div");
    n20.setAttribute("class", "thumbnail-title");
elements.push(n20)
n17.appendChild(n20);
n20.appendChild(document.createTextNode("            "));

let n21 = document.createElement("a");
    n21.setAttribute("data-anchor", "name");
    n21.setAttribute("href", "");
elements.push(n21)
n20.appendChild(n21);
anchors["name"] = n21;
n20.appendChild(document.createTextNode("        "));
n17.appendChild(document.createTextNode("        "));

let n22 = document.createElement("div");
    n22.setAttribute("data-anchor", "body");
    n22.setAttribute("class", "thumbnail-content");
elements.push(n22)
n17.appendChild(n22);
anchors["body"] = n22;
n17.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotLinker;

