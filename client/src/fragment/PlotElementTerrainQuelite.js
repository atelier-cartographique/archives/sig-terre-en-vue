

// PlotElementTerrainQuelite fragment mixin

var  PlotElementTerrainQuelite = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    let anchors = this.anchors;
    let root = this.el;

//< generated code - if anything wrong in there, check the template first

let n5355 = document.createElement("div");
    n5355.setAttribute("class", "plot-element terrain");
    n5355.setAttribute(",", "None");
    n5355.setAttribute("data-anchor", "element");
root.appendChild(n5355);
anchors["element"] = n5355;

let n5356 = document.createElement("h3");
n5355.appendChild(n5356);
n5356.appendChild(document.createTextNode("Estimation de la qualite du sol"));

let n5357 = document.createElement("span");
    n5357.setAttribute("class", "date");
    n5357.setAttribute("data-anchor", "date");
n5355.appendChild(n5357);
anchors["date"] = n5357;

let n5358 = document.createElement("div");
    n5358.setAttribute("class", "options");
    n5358.setAttribute("data-anchor", "options");
n5355.appendChild(n5358);
anchors["options"] = n5358;

let n5359 = document.createElement("span");
    n5359.setAttribute("data-option", "bon");
n5358.appendChild(n5359);
n5359.appendChild(document.createTextNode("bonne (présence d&#x27;une couverture végétale, sol profond, de couleur brun/noir, avec des verres de terre au premier coup de pelle)"));

let n5360 = document.createElement("span");
    n5360.setAttribute("data-option", "moyen");
n5358.appendChild(n5360);
n5360.appendChild(document.createTextNode("moyenne"));

let n5361 = document.createElement("span");
    n5361.setAttribute("data-option", "mauvais");
n5358.appendChild(n5361);
n5361.appendChild(document.createTextNode("mauvaise (pas de couverture végétale, faible profondeur, couleur claire, pas de traces de vie dans le sol)"));
//> end of generated code

    return this;
};

}

export default PlotElementTerrainQuelite;

