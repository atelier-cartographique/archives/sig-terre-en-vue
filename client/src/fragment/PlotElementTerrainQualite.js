

// PlotElementTerrainQualite fragment mixin

var  PlotElementTerrainQualite = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n173 = document.createElement("div");
    n173.setAttribute("class", "plot-element terrain");
    n173.setAttribute("data-anchor", "element");
elements.push(n173)
root.appendChild(n173);
anchors["element"] = n173;
n173.appendChild(document.createTextNode("        "));

let n174 = document.createElement("h3");
elements.push(n174)
n173.appendChild(n174);
n174.appendChild(document.createTextNode("Estimation de la qualite du sol"));
n173.appendChild(document.createTextNode("        "));

let n175 = document.createElement("span");
    n175.setAttribute("class", "date");
    n175.setAttribute("data-anchor", "date");
elements.push(n175)
n173.appendChild(n175);
anchors["date"] = n175;
n173.appendChild(document.createTextNode("        "));

let n176 = document.createElement("div");
    n176.setAttribute("class", "options");
    n176.setAttribute("data-anchor", "options");
elements.push(n176)
n173.appendChild(n176);
anchors["options"] = n176;
n176.appendChild(document.createTextNode("            "));

let n177 = document.createElement("span");
    n177.setAttribute("data-option", "bon");
elements.push(n177)
n176.appendChild(n177);
n177.appendChild(document.createTextNode("bonne (présence d'une couverture végétale, sol profond, de couleur brun/noir, avec des verres de terre au premier coup de pelle)"));
n176.appendChild(document.createTextNode("            "));

let n178 = document.createElement("span");
    n178.setAttribute("data-option", "moyen");
elements.push(n178)
n176.appendChild(n178);
n178.appendChild(document.createTextNode("moyenne"));
n176.appendChild(document.createTextNode("            "));

let n179 = document.createElement("span");
    n179.setAttribute("data-option", "mauvais");
elements.push(n179)
n176.appendChild(n179);
n179.appendChild(document.createTextNode("mauvaise (pas de couverture végétale, faible profondeur, couleur claire, pas de traces de vie dans le sol)"));
n176.appendChild(document.createTextNode("        "));
n173.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementTerrainQualite;

