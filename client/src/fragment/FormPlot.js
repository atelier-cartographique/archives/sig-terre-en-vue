

// FormPlot fragment mixin

var  FormPlot = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("map");
names.push("results");
names.push("submit");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n83 = document.createElement("section");
    n83.setAttribute("class", "form-help");
elements.push(n83)
root.appendChild(n83);
n83.appendChild(document.createTextNode("        "));

let n84 = document.createElement("p");
elements.push(n84)
n83.appendChild(n84);
n84.appendChild(document.createTextNode("            Ce formulaire vous permet d'initier une étude de parcelle. Pour ce faire, naviguez dans la carte jusque la parcelle visée, et cliquez."));
n84.appendChild(document.createTextNode("            Une fois que les informations relatives à cette parcelle s'affichent, validez en cliquant sur le bouton "));

let n85 = document.createElement("em");
elements.push(n85)
n84.appendChild(n85);
n85.appendChild(document.createTextNode("Créer une nouvelle parcelle"));
n84.appendChild(document.createTextNode(" pour accéder au questionnaire."));
n84.appendChild(document.createTextNode("        "));
n83.appendChild(document.createTextNode("        "));

let n86 = document.createElement("p");
elements.push(n86)
n83.appendChild(n86);
n86.appendChild(document.createTextNode("            Les parcelles de couleur "));

let n87 = document.createElement("span");
    n87.setAttribute("style", "color:red");
elements.push(n87)
n86.appendChild(n87);
n87.appendChild(document.createTextNode("rouge"));
n86.appendChild(document.createTextNode(" sont celles qui sont déjà l'objet d'une fiche."));
n86.appendChild(document.createTextNode("            Les zones "));

let n88 = document.createElement("span");
    n88.setAttribute("style", "color:green");
elements.push(n88)
n86.appendChild(n88);
n88.appendChild(document.createTextNode("vertes"));
n86.appendChild(document.createTextNode(" sont des zones d'intérêt identifées par terre-en-vue."));
n86.appendChild(document.createTextNode("        "));
n83.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n89 = document.createElement("section");
elements.push(n89)
root.appendChild(n89);
n89.appendChild(document.createTextNode("        "));

let n90 = document.createElement("div");
    n90.setAttribute("data-anchor", "map");
    n90.setAttribute("class", "map query-map");
elements.push(n90)
n89.appendChild(n90);
anchors["map"] = n90;
n89.appendChild(document.createTextNode("        "));

let n91 = document.createElement("div");
    n91.setAttribute("data-anchor", "results");
elements.push(n91)
n89.appendChild(n91);
anchors["results"] = n91;
n89.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n92 = document.createElement("section");
    n92.setAttribute("class", "button-box");
elements.push(n92)
root.appendChild(n92);
n92.appendChild(document.createTextNode("        "));

let n93 = document.createElement("div");
    n93.setAttribute("class", "button");
    n93.setAttribute("data-anchor", "submit");
elements.push(n93)
n92.appendChild(n93);
anchors["submit"] = n93;
n93.appendChild(document.createTextNode("Créer une nouvelle parcelle"));
n92.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default FormPlot;

