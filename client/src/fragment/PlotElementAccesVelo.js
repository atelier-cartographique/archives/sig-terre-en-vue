

// PlotElementAccesVelo fragment mixin

var  PlotElementAccesVelo = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n231 = document.createElement("div");
    n231.setAttribute("class", "plot-element acces");
    n231.setAttribute("data-anchor", "element");
elements.push(n231)
root.appendChild(n231);
anchors["element"] = n231;
n231.appendChild(document.createTextNode("        "));

let n232 = document.createElement("h3");
elements.push(n232)
n231.appendChild(n232);
n232.appendChild(document.createTextNode("Facilité d'accès à vélo"));
n231.appendChild(document.createTextNode("        "));

let n233 = document.createElement("span");
    n233.setAttribute("class", "date");
    n233.setAttribute("data-anchor", "date");
elements.push(n233)
n231.appendChild(n233);
anchors["date"] = n233;
n231.appendChild(document.createTextNode("        "));

let n234 = document.createElement("div");
    n234.setAttribute("class", "options");
    n234.setAttribute("data-anchor", "options");
elements.push(n234)
n231.appendChild(n234);
anchors["options"] = n234;
n234.appendChild(document.createTextNode("            "));

let n235 = document.createElement("span");
    n235.setAttribute("data-option", "oui");
elements.push(n235)
n234.appendChild(n235);
n235.appendChild(document.createTextNode("oui"));
n234.appendChild(document.createTextNode("            "));

let n236 = document.createElement("span");
    n236.setAttribute("data-option", "non");
elements.push(n236)
n234.appendChild(n236);
n236.appendChild(document.createTextNode("non"));
n234.appendChild(document.createTextNode("        "));
n231.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementAccesVelo;

