

// Document fragment mixin

var  Document = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("name");
names.push("author");
names.push("editLink");
names.push("body");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n298 = document.createElement("h1");
    n298.setAttribute("data-anchor", "name");
elements.push(n298)
root.appendChild(n298);
anchors["name"] = n298;
root.appendChild(document.createTextNode("    "));

let n299 = document.createElement("span");
    n299.setAttribute("class", "author");
elements.push(n299)
root.appendChild(n299);
n299.appendChild(document.createTextNode("authored by "));

let n300 = document.createElement("a");
    n300.setAttribute("data-anchor", "author");
    n300.setAttribute("href", "#");
elements.push(n300)
n299.appendChild(n300);
anchors["author"] = n300;
root.appendChild(document.createTextNode("    "));

let n301 = document.createElement("div");
    n301.setAttribute("class", "button-box");
elements.push(n301)
root.appendChild(n301);
n301.appendChild(document.createTextNode("        "));

let n302 = document.createElement("a");
    n302.setAttribute("href", "#");
    n302.setAttribute("data-anchor", "editLink");
elements.push(n302)
n301.appendChild(n302);
anchors["editLink"] = n302;
n302.appendChild(document.createTextNode("edit"));
n301.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n303 = document.createElement("div");
    n303.setAttribute("class", "content-inner");
elements.push(n303)
root.appendChild(n303);
n303.appendChild(document.createTextNode("        "));

let n304 = document.createElement("article");
    n304.setAttribute("data-anchor", "body");
    n304.setAttribute("class", "document-section");
elements.push(n304)
n303.appendChild(n304);
anchors["body"] = n304;
n304.appendChild(document.createTextNode("        "));
n303.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default Document;

