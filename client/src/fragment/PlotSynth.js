

// PlotSynth fragment mixin

var  PlotSynth = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("map");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n104 = document.createElement("div");
    n104.setAttribute("class", "query-map");
    n104.setAttribute("data-anchor", "map");
elements.push(n104)
root.appendChild(n104);
anchors["map"] = n104;
n104.appendChild(document.createTextNode(" "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotSynth;

