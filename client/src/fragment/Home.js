

// Home fragment mixin

var  Home = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("users");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first

let n29 = document.createElement("section");
    n29.setAttribute("class", "body-section");
elements.push(n29)
root.appendChild(n29);
n29.appendChild(document.createTextNode("    "));

let n30 = document.createElement("h1");
elements.push(n30)
n29.appendChild(n30);
n30.appendChild(document.createTextNode("SIG agriculture urbaine en région Bruxelles-Capitale"));
n29.appendChild(document.createTextNode("    "));

let n31 = document.createElement("p");
elements.push(n31)
n29.appendChild(n31);
n31.appendChild(document.createTextNode("    Pour soutenir le développement de l'agriculture urbaine en Région de Bruxelles-Capitale (RBC), nous pensons qu'il est indispensable d'avoir un outil permettant d'identifier le potentiel de terres utilisables pour l'agriculture et de mettre en évidence les opportunités qui se présentent d'y installer des porteurs de projet."));
n31.appendChild(document.createTextNode("    "));
n29.appendChild(document.createTextNode("    "));

let n32 = document.createElement("p");
elements.push(n32)
n29.appendChild(n32);
n32.appendChild(document.createTextNode("    Pour y parvenir, nous aimerions inviter tout un chacun à accomplir ce travail de façon collaborative. C'est la raison pour laquelle nous avons créé cette plateforme web qui vous invite à parcourir le terrain et à y rentrer une série d'informations relative au potentiel d'installation d'agriculteurs sur les parcelles de la Région de Bruxelles-Capitale. Cela nous permettra de faciliter les installations d'agriculteurs dans la ville."));
n32.appendChild(document.createTextNode("    "));
n29.appendChild(document.createTextNode("    "));

let n33 = document.createElement("p");
elements.push(n33)
n29.appendChild(n33);
n33.appendChild(document.createTextNode("    L'esprit est de rendre l'information la plus disponible au plus grand nombre, de façon à faciliter les collaborations entre “prospecteurs” et de construire autant de fils narratifs qu'il existe de potentiels. La plateforme est aussi une invitation à s'investir de la question de l'accès à la terre à Bruxelles. Elle sera à ce propos un lieu où nous pourrons rendre public les résultats de nos recherches de terres."));
n33.appendChild(document.createTextNode("    "));

let n34 = document.createElement("section");
    n34.setAttribute("class", "body-section");
elements.push(n34)
root.appendChild(n34);
n34.appendChild(document.createTextNode("    Voir "));

let n35 = document.createElement("a");
    n35.setAttribute("href", "/plotmap");
elements.push(n35)
n34.appendChild(n35);
n35.appendChild(document.createTextNode("la carte"));
n34.appendChild(document.createTextNode(" de recherche des sites potentiels."));

let n36 = document.createElement("section");
    n36.setAttribute("class", "body-section");
elements.push(n36)
root.appendChild(n36);
n36.appendChild(document.createTextNode("    "));

let n37 = document.createElement("h2");
elements.push(n37)
n36.appendChild(n37);
n37.appendChild(document.createTextNode("Liste des ambassadeurs"));
n36.appendChild(document.createTextNode("    "));

let n38 = document.createElement("div");
    n38.setAttribute("data-anchor", "users");
    n38.setAttribute("class", "user-list");
elements.push(n38)
n36.appendChild(n38);
anchors["users"] = n38;
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default Home;

