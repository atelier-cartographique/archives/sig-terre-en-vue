

// PlotElementAccesTransport fragment mixin

var  PlotElementAccesTransport = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n225 = document.createElement("div");
    n225.setAttribute("class", "plot-element acces");
    n225.setAttribute("data-anchor", "element");
elements.push(n225)
root.appendChild(n225);
anchors["element"] = n225;
n225.appendChild(document.createTextNode("        "));

let n226 = document.createElement("h3");
elements.push(n226)
n225.appendChild(n226);
n226.appendChild(document.createTextNode("Accès en transport en commun"));
n225.appendChild(document.createTextNode("        "));

let n227 = document.createElement("span");
    n227.setAttribute("class", "date");
    n227.setAttribute("data-anchor", "date");
elements.push(n227)
n225.appendChild(n227);
anchors["date"] = n227;
n225.appendChild(document.createTextNode("        "));

let n228 = document.createElement("div");
    n228.setAttribute("class", "options");
    n228.setAttribute("data-anchor", "options");
elements.push(n228)
n225.appendChild(n228);
anchors["options"] = n228;
n228.appendChild(document.createTextNode("            "));

let n229 = document.createElement("span");
    n229.setAttribute("data-option", "oui");
elements.push(n229)
n228.appendChild(n229);
n229.appendChild(document.createTextNode("oui ("));
n229.appendChild(document.createTextNode("<"));
n229.appendChild(document.createTextNode(" 300m)"));
n228.appendChild(document.createTextNode("            "));

let n230 = document.createElement("span");
    n230.setAttribute("data-option", "non");
elements.push(n230)
n228.appendChild(n230);
n230.appendChild(document.createTextNode("non"));
n228.appendChild(document.createTextNode("        "));
n225.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementAccesTransport;

