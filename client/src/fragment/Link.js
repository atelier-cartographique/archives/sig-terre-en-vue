

// Link fragment mixin

var  Link = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("target");
names.push("content");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n265 = document.createElement("div");
    n265.setAttribute("class", "link");
elements.push(n265)
root.appendChild(n265);
n265.appendChild(document.createTextNode("        "));

let n266 = document.createElement("a");
    n266.setAttribute("data-anchor", "target");
    n266.setAttribute("href", "#");
elements.push(n266)
n265.appendChild(n266);
anchors["target"] = n266;
n265.appendChild(document.createTextNode("        "));

let n267 = document.createElement("div");
    n267.setAttribute("data-anchor", "content");
    n267.setAttribute("class", "link-content");
elements.push(n267)
n265.appendChild(n267);
anchors["content"] = n267;
n267.appendChild(document.createTextNode("        "));
n265.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default Link;

