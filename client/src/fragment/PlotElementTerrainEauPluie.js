

// PlotElementTerrainEauPluie fragment mixin

var  PlotElementTerrainEauPluie = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n144 = document.createElement("div");
    n144.setAttribute("class", "plot-element terrain");
    n144.setAttribute("data-anchor", "element");
elements.push(n144)
root.appendChild(n144);
anchors["element"] = n144;
n144.appendChild(document.createTextNode("        "));

let n145 = document.createElement("h3");
elements.push(n145)
n144.appendChild(n145);
n145.appendChild(document.createTextNode("Possibilité de récupérer l'eau de pluie"));
n144.appendChild(document.createTextNode("        "));

let n146 = document.createElement("span");
    n146.setAttribute("class", "date");
    n146.setAttribute("data-anchor", "date");
elements.push(n146)
n144.appendChild(n146);
anchors["date"] = n146;
n144.appendChild(document.createTextNode("        "));

let n147 = document.createElement("div");
    n147.setAttribute("class", "options");
    n147.setAttribute("data-anchor", "options");
elements.push(n147)
n144.appendChild(n147);
anchors["options"] = n147;
n147.appendChild(document.createTextNode("            "));

let n148 = document.createElement("span");
    n148.setAttribute("data-option", "citerne");
elements.push(n148)
n147.appendChild(n148);
n148.appendChild(document.createTextNode("citerne"));
n147.appendChild(document.createTextNode("            "));

let n149 = document.createElement("span");
    n149.setAttribute("data-option", "mare");
elements.push(n149)
n147.appendChild(n149);
n149.appendChild(document.createTextNode("mare"));
n147.appendChild(document.createTextNode("            "));

let n150 = document.createElement("span");
    n150.setAttribute("data-option", "etang");
elements.push(n150)
n147.appendChild(n150);
n150.appendChild(document.createTextNode("etang"));
n147.appendChild(document.createTextNode("            "));

let n151 = document.createElement("span");
    n151.setAttribute("data-option", "autre");
elements.push(n151)
n147.appendChild(n151);
n151.appendChild(document.createTextNode("autre"));
n147.appendChild(document.createTextNode("        "));
n144.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementTerrainEauPluie;

