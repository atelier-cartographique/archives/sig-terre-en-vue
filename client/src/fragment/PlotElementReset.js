

// PlotElementReset fragment mixin

var  PlotElementReset = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("reset");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n135 = document.createElement("div");
    n135.setAttribute("class", "reset");
elements.push(n135)
root.appendChild(n135);
n135.appendChild(document.createTextNode("        "));

let n136 = document.createElement("span");
    n136.setAttribute("data-anchor", "reset");
elements.push(n136)
n135.appendChild(n136);
anchors["reset"] = n136;
n136.appendChild(document.createTextNode("x"));
n135.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementReset;

