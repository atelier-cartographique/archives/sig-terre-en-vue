

// DocumentThumbnail fragment mixin

var  DocumentThumbnail = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("name");
names.push("body");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n305 = document.createElement("div");
    n305.setAttribute("class", "thumbnail-resource thumbnail-document");
elements.push(n305)
root.appendChild(n305);
n305.appendChild(document.createTextNode("        "));

let n306 = document.createElement("div");
    n306.setAttribute("class", "thumbnail-title");
elements.push(n306)
n305.appendChild(n306);
n306.appendChild(document.createTextNode("            "));

let n307 = document.createElement("a");
    n307.setAttribute("data-anchor", "name");
    n307.setAttribute("href", "");
elements.push(n307)
n306.appendChild(n307);
anchors["name"] = n307;
n306.appendChild(document.createTextNode("        "));
n305.appendChild(document.createTextNode("        "));

let n308 = document.createElement("div");
    n308.setAttribute("data-anchor", "body");
    n308.setAttribute("class", "thumbnail-content");
elements.push(n308)
n305.appendChild(n308);
anchors["body"] = n308;
n305.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default DocumentThumbnail;

