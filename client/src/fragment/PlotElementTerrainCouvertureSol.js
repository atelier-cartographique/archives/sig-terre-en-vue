

// PlotElementTerrainCouvertureSol fragment mixin

var  PlotElementTerrainCouvertureSol = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n137 = document.createElement("div");
    n137.setAttribute("class", "plot-element terrain");
    n137.setAttribute("data-anchor", "element");
elements.push(n137)
root.appendChild(n137);
anchors["element"] = n137;
n137.appendChild(document.createTextNode("        "));

let n138 = document.createElement("h3");
elements.push(n138)
n137.appendChild(n138);
n138.appendChild(document.createTextNode("Couverture du sol"));
n137.appendChild(document.createTextNode("        "));

let n139 = document.createElement("span");
    n139.setAttribute("class", "date");
    n139.setAttribute("data-anchor", "date");
elements.push(n139)
n137.appendChild(n139);
anchors["date"] = n139;
n137.appendChild(document.createTextNode("        "));

let n140 = document.createElement("div");
    n140.setAttribute("class", "options");
    n140.setAttribute("data-anchor", "options");
elements.push(n140)
n137.appendChild(n140);
anchors["options"] = n140;
n140.appendChild(document.createTextNode("            "));

let n141 = document.createElement("span");
    n141.setAttribute("data-option", "pelouse");
elements.push(n141)
n140.appendChild(n141);
n141.appendChild(document.createTextNode("pelouse"));
n140.appendChild(document.createTextNode("            "));

let n142 = document.createElement("span");
    n142.setAttribute("data-option", "arbuste");
elements.push(n142)
n140.appendChild(n142);
n142.appendChild(document.createTextNode("arbustes"));
n140.appendChild(document.createTextNode("            "));

let n143 = document.createElement("span");
    n143.setAttribute("data-option", "arbre");
elements.push(n143)
n140.appendChild(n143);
n143.appendChild(document.createTextNode("arbres"));
n140.appendChild(document.createTextNode("        "));
n137.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementTerrainCouvertureSol;

