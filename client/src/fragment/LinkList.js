

// LinkList fragment mixin

var  LinkList = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("links");
names.push("newLink");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n287 = document.createElement("div");
    n287.setAttribute("data-anchor", "links");
    n287.setAttribute("class", "link-block");
elements.push(n287)
root.appendChild(n287);
anchors["links"] = n287;
n287.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n288 = document.createElement("div");
    n288.setAttribute("class", "button-block");
elements.push(n288)
root.appendChild(n288);
n288.appendChild(document.createTextNode("    	"));

let n289 = document.createElement("a");
    n289.setAttribute("data-anchor", "newLink");
    n289.setAttribute("href", "#");
elements.push(n289)
n288.appendChild(n289);
anchors["newLink"] = n289;
n289.appendChild(document.createTextNode("Lier ou délier vos images, documents ou parcelles à cette ressource."));
n288.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default LinkList;

