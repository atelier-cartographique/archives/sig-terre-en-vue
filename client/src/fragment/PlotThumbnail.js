

// PlotThumbnail fragment mixin

var  PlotThumbnail = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("name");
names.push("body");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n94 = document.createElement("div");
    n94.setAttribute("class", "thumbnail-resource thumbnail-plot");
elements.push(n94)
root.appendChild(n94);
n94.appendChild(document.createTextNode("        "));

let n95 = document.createElement("div");
    n95.setAttribute("class", "thumbnail-title");
elements.push(n95)
n94.appendChild(n95);
n95.appendChild(document.createTextNode("            "));

let n96 = document.createElement("a");
    n96.setAttribute("data-anchor", "name");
    n96.setAttribute("href", "");
elements.push(n96)
n95.appendChild(n96);
anchors["name"] = n96;
n95.appendChild(document.createTextNode("        "));
n94.appendChild(document.createTextNode("        "));

let n97 = document.createElement("div");
    n97.setAttribute("data-anchor", "body");
    n97.setAttribute("class", "thumbnail-content");
elements.push(n97)
n94.appendChild(n97);
anchors["body"] = n97;
n94.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotThumbnail;

