

// MediaThumbnail fragment mixin

var  MediaThumbnail = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("name");
names.push("image");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n273 = document.createElement("div");
    n273.setAttribute("class", "thumbnail-resource thumbnail-media");
elements.push(n273)
root.appendChild(n273);
n273.appendChild(document.createTextNode("        "));

let n274 = document.createElement("div");
    n274.setAttribute("class", "thumbnail-title");
elements.push(n274)
n273.appendChild(n274);
n274.appendChild(document.createTextNode("            "));

let n275 = document.createElement("a");
    n275.setAttribute("data-anchor", "name");
    n275.setAttribute("href", "");
elements.push(n275)
n274.appendChild(n275);
anchors["name"] = n275;
n274.appendChild(document.createTextNode("        "));
n273.appendChild(document.createTextNode("        "));

let n276 = document.createElement("div");
    n276.setAttribute("class", "thumbnail-content");
elements.push(n276)
n273.appendChild(n276);
n276.appendChild(document.createTextNode("            "));

let n277 = document.createElement("img");
    n277.setAttribute("src", "");
    n277.setAttribute("data-anchor", "image");
elements.push(n277)
n276.appendChild(n277);
anchors["image"] = n277;
n276.appendChild(document.createTextNode("        "));
n273.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default MediaThumbnail;

