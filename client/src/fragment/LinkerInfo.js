

// LinkerInfo fragment mixin

var  LinkerInfo = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("targetLink");
names.push("targetName");
names.push("documents");
names.push("plots");
names.push("medias");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n23 = document.createElement("div");
    n23.setAttribute("class", "");
elements.push(n23)
root.appendChild(n23);
n23.appendChild(document.createTextNode("        Gérez vos liens à:"));
n23.appendChild(document.createTextNode("        "));

let n24 = document.createElement("a");
    n24.setAttribute("data-anchor", "targetLink");
    n24.setAttribute("href", "#");
elements.push(n24)
n23.appendChild(n24);
anchors["targetLink"] = n24;
n24.appendChild(document.createTextNode("            "));

let n25 = document.createElement("span");
    n25.setAttribute("data-anchor", "targetName");
elements.push(n25)
n24.appendChild(n25);
anchors["targetName"] = n25;
n24.appendChild(document.createTextNode("        "));
n23.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n26 = document.createElement("div");
    n26.setAttribute("data-anchor", "documents");
    n26.setAttribute("class", "list-document resources");
elements.push(n26)
root.appendChild(n26);
anchors["documents"] = n26;
n26.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n27 = document.createElement("div");
    n27.setAttribute("data-anchor", "plots");
    n27.setAttribute("class", "list-plot resources");
elements.push(n27)
root.appendChild(n27);
anchors["plots"] = n27;
n27.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n28 = document.createElement("div");
    n28.setAttribute("data-anchor", "medias");
    n28.setAttribute("class", "list-media resources");
elements.push(n28)
root.appendChild(n28);
anchors["medias"] = n28;
n28.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default LinkerInfo;

