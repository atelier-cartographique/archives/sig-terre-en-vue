

// PlotElementContexteCommerce fragment mixin

var  PlotElementContexteCommerce = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n258 = document.createElement("div");
    n258.setAttribute("class", "plot-element contexte");
    n258.setAttribute("data-anchor", "element");
elements.push(n258)
root.appendChild(n258);
anchors["element"] = n258;
n258.appendChild(document.createTextNode("        "));

let n259 = document.createElement("h3");
elements.push(n259)
n258.appendChild(n259);
n259.appendChild(document.createTextNode("Présence de petits commerces"));
n258.appendChild(document.createTextNode("        "));

let n260 = document.createElement("span");
    n260.setAttribute("class", "date");
    n260.setAttribute("data-anchor", "date");
elements.push(n260)
n258.appendChild(n260);
anchors["date"] = n260;
n258.appendChild(document.createTextNode("        "));

let n261 = document.createElement("div");
    n261.setAttribute("class", "options");
    n261.setAttribute("data-anchor", "options");
elements.push(n261)
n258.appendChild(n261);
anchors["options"] = n261;
n261.appendChild(document.createTextNode("            "));

let n262 = document.createElement("span");
    n262.setAttribute("data-option", "peu");
elements.push(n262)
n261.appendChild(n262);
n262.appendChild(document.createTextNode("peu"));
n261.appendChild(document.createTextNode("            "));

let n263 = document.createElement("span");
    n263.setAttribute("data-option", "moyen");
elements.push(n263)
n261.appendChild(n263);
n263.appendChild(document.createTextNode("moyen"));
n261.appendChild(document.createTextNode("            "));

let n264 = document.createElement("span");
    n264.setAttribute("data-option", "beaucoup");
elements.push(n264)
n261.appendChild(n264);
n264.appendChild(document.createTextNode("beaucoup"));
n261.appendChild(document.createTextNode("        "));
n258.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementContexteCommerce;

