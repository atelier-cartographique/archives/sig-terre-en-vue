

// PlotElementAccesParking fragment mixin

var  PlotElementAccesParking = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n218 = document.createElement("div");
    n218.setAttribute("class", "plot-element acces");
    n218.setAttribute("data-anchor", "element");
elements.push(n218)
root.appendChild(n218);
anchors["element"] = n218;
n218.appendChild(document.createTextNode("        "));

let n219 = document.createElement("h3");
elements.push(n219)
n218.appendChild(n219);
n219.appendChild(document.createTextNode("Proximité de places de parking"));
n218.appendChild(document.createTextNode("        "));

let n220 = document.createElement("span");
    n220.setAttribute("class", "date");
    n220.setAttribute("data-anchor", "date");
elements.push(n220)
n218.appendChild(n220);
anchors["date"] = n220;
n218.appendChild(document.createTextNode("        "));

let n221 = document.createElement("div");
    n221.setAttribute("class", "options");
    n221.setAttribute("data-anchor", "options");
elements.push(n221)
n218.appendChild(n221);
anchors["options"] = n221;
n221.appendChild(document.createTextNode("            "));

let n222 = document.createElement("span");
    n222.setAttribute("data-option", "immediat");
elements.push(n222)
n221.appendChild(n222);
n222.appendChild(document.createTextNode("immediat ("));
n222.appendChild(document.createTextNode("<"));
n222.appendChild(document.createTextNode(" 50m)"));
n221.appendChild(document.createTextNode("            "));

let n223 = document.createElement("span");
    n223.setAttribute("data-option", "proche");
elements.push(n223)
n221.appendChild(n223);
n223.appendChild(document.createTextNode("proche ("));
n223.appendChild(document.createTextNode("<"));
n223.appendChild(document.createTextNode(" 200m)"));
n221.appendChild(document.createTextNode("            "));

let n224 = document.createElement("span");
    n224.setAttribute("data-option", "lointain");
elements.push(n224)
n221.appendChild(n224);
n224.appendChild(document.createTextNode("loin (au-dela)"));
n221.appendChild(document.createTextNode("        "));
n218.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementAccesParking;

