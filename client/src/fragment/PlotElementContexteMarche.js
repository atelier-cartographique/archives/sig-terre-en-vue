

// PlotElementContexteMarche fragment mixin

var  PlotElementContexteMarche = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n252 = document.createElement("div");
    n252.setAttribute("class", "plot-element contexte");
    n252.setAttribute("data-anchor", "element");
elements.push(n252)
root.appendChild(n252);
anchors["element"] = n252;
n252.appendChild(document.createTextNode("        "));

let n253 = document.createElement("h3");
elements.push(n253)
n252.appendChild(n253);
n253.appendChild(document.createTextNode("Présence d'un marché hebdomadaire"));
n252.appendChild(document.createTextNode("        "));

let n254 = document.createElement("span");
    n254.setAttribute("class", "date");
    n254.setAttribute("data-anchor", "date");
elements.push(n254)
n252.appendChild(n254);
anchors["date"] = n254;
n252.appendChild(document.createTextNode("        "));

let n255 = document.createElement("div");
    n255.setAttribute("class", "options");
    n255.setAttribute("data-anchor", "options");
elements.push(n255)
n252.appendChild(n255);
anchors["options"] = n255;
n255.appendChild(document.createTextNode("            "));

let n256 = document.createElement("span");
    n256.setAttribute("data-option", "oui");
elements.push(n256)
n255.appendChild(n256);
n256.appendChild(document.createTextNode("oui"));
n255.appendChild(document.createTextNode("            "));

let n257 = document.createElement("span");
    n257.setAttribute("data-option", "non");
elements.push(n257)
n255.appendChild(n257);
n257.appendChild(document.createTextNode("non"));
n255.appendChild(document.createTextNode("        "));
n252.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementContexteMarche;

