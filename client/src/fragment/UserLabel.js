

// UserLabel fragment mixin

var  UserLabel = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("url");
names.push("name");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n58 = document.createElement("div");
    n58.setAttribute("class", "user-label");
elements.push(n58)
root.appendChild(n58);
n58.appendChild(document.createTextNode("            "));

let n59 = document.createElement("a");
    n59.setAttribute("data-anchor", "url");
    n59.setAttribute("href", "");
elements.push(n59)
n58.appendChild(n59);
anchors["url"] = n59;
n59.appendChild(document.createTextNode("               "));

let n60 = document.createElement("span");
    n60.setAttribute("data-anchor", "name");
elements.push(n60)
n59.appendChild(n60);
anchors["name"] = n60;
n59.appendChild(document.createTextNode("           "));
n58.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default UserLabel;

