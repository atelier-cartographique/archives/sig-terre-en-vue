

// Query fragment mixin

var  Query = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("name");
names.push("map");
names.push("description");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n290 = document.createElement("h1");
    n290.setAttribute("data-anchor", "name");
elements.push(n290)
root.appendChild(n290);
anchors["name"] = n290;
root.appendChild(document.createTextNode("    "));

let n291 = document.createElement("div");
    n291.setAttribute("class", "content-inner");
elements.push(n291)
root.appendChild(n291);
n291.appendChild(document.createTextNode("        "));

let n292 = document.createElement("div");
    n292.setAttribute("data-anchor", "map");
    n292.setAttribute("class", "map");
elements.push(n292)
n291.appendChild(n292);
anchors["map"] = n292;
n292.appendChild(document.createTextNode("        "));
n291.appendChild(document.createTextNode("        "));

let n293 = document.createElement("article");
    n293.setAttribute("data-anchor", "description");
    n293.setAttribute("class", "document-section");
elements.push(n293)
n291.appendChild(n293);
anchors["description"] = n293;
n293.appendChild(document.createTextNode("        "));
n291.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default Query;

