

// PlotElementInfraCloture fragment mixin

var  PlotElementInfraCloture = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n202 = document.createElement("div");
    n202.setAttribute("class", "plot-element infra");
    n202.setAttribute("data-anchor", "element");
elements.push(n202)
root.appendChild(n202);
anchors["element"] = n202;
n202.appendChild(document.createTextNode("        "));

let n203 = document.createElement("h3");
elements.push(n203)
n202.appendChild(n203);
n203.appendChild(document.createTextNode("Présence de clôtures à bétail"));
n202.appendChild(document.createTextNode("        "));

let n204 = document.createElement("span");
    n204.setAttribute("class", "date");
    n204.setAttribute("data-anchor", "date");
elements.push(n204)
n202.appendChild(n204);
anchors["date"] = n204;
n202.appendChild(document.createTextNode("        "));

let n205 = document.createElement("div");
    n205.setAttribute("class", "options");
    n205.setAttribute("data-anchor", "options");
elements.push(n205)
n202.appendChild(n205);
anchors["options"] = n205;
n205.appendChild(document.createTextNode("            "));

let n206 = document.createElement("span");
    n206.setAttribute("data-option", "non");
elements.push(n206)
n205.appendChild(n206);
n206.appendChild(document.createTextNode("non"));
n205.appendChild(document.createTextNode("            "));

let n207 = document.createElement("span");
    n207.setAttribute("data-option", "grillage");
elements.push(n207)
n205.appendChild(n207);
n207.appendChild(document.createTextNode("grillage"));
n205.appendChild(document.createTextNode("            "));

let n208 = document.createElement("span");
    n208.setAttribute("data-option", "barbele");
elements.push(n208)
n205.appendChild(n208);
n208.appendChild(document.createTextNode("barbelé"));
n205.appendChild(document.createTextNode("            "));

let n209 = document.createElement("span");
    n209.setAttribute("data-option", "electrique");
elements.push(n209)
n205.appendChild(n209);
n209.appendChild(document.createTextNode("fil éléctrifié"));
n205.appendChild(document.createTextNode("            "));

let n210 = document.createElement("span");
    n210.setAttribute("data-option", "autre");
elements.push(n210)
n205.appendChild(n210);
n210.appendChild(document.createTextNode("autre"));
n205.appendChild(document.createTextNode("        "));
n202.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementInfraCloture;

