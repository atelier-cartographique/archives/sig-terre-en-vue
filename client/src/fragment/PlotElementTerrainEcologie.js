

// PlotElementTerrainEcologie fragment mixin

var  PlotElementTerrainEcologie = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n158 = document.createElement("div");
    n158.setAttribute("class", "plot-element terrain");
    n158.setAttribute("data-anchor", "element");
elements.push(n158)
root.appendChild(n158);
anchors["element"] = n158;
n158.appendChild(document.createTextNode("        "));

let n159 = document.createElement("h3");
elements.push(n159)
n158.appendChild(n159);
n159.appendChild(document.createTextNode("Présence d'éléments d'un maillage écologique"));
n158.appendChild(document.createTextNode("        "));

let n160 = document.createElement("span");
    n160.setAttribute("class", "date");
    n160.setAttribute("data-anchor", "date");
elements.push(n160)
n158.appendChild(n160);
anchors["date"] = n160;
n158.appendChild(document.createTextNode("        "));

let n161 = document.createElement("div");
    n161.setAttribute("class", "options");
    n161.setAttribute("data-anchor", "options");
elements.push(n161)
n158.appendChild(n161);
anchors["options"] = n161;
n161.appendChild(document.createTextNode("            "));

let n162 = document.createElement("span");
    n162.setAttribute("data-option", "haie");
elements.push(n162)
n161.appendChild(n162);
n162.appendChild(document.createTextNode("haies vives"));
n161.appendChild(document.createTextNode("            "));

let n163 = document.createElement("span");
    n163.setAttribute("data-option", "foret");
elements.push(n163)
n161.appendChild(n163);
n163.appendChild(document.createTextNode("lisiere de foret"));
n161.appendChild(document.createTextNode("            "));

let n164 = document.createElement("span");
    n164.setAttribute("data-option", "eau");
elements.push(n164)
n161.appendChild(n164);
n164.appendChild(document.createTextNode("point d'eau"));
n161.appendChild(document.createTextNode("            "));

let n165 = document.createElement("span");
    n165.setAttribute("data-option", "herbe");
elements.push(n165)
n161.appendChild(n165);
n165.appendChild(document.createTextNode("bande enherbee"));
n161.appendChild(document.createTextNode("            "));

let n166 = document.createElement("span");
    n166.setAttribute("data-option", "neant");
elements.push(n166)
n161.appendChild(n166);
n166.appendChild(document.createTextNode("neant"));
n161.appendChild(document.createTextNode("        "));
n158.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementTerrainEcologie;

