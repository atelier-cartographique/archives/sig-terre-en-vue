

// Media fragment mixin

var  Media = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("name");
names.push("image");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n1 = document.createElement("h1");
    n1.setAttribute("data-anchor", "name");
elements.push(n1)
root.appendChild(n1);
anchors["name"] = n1;
root.appendChild(document.createTextNode("    "));

let n2 = document.createElement("div");
    n2.setAttribute("class", "content-inner");
elements.push(n2)
root.appendChild(n2);
n2.appendChild(document.createTextNode("        "));

let n3 = document.createElement("img");
    n3.setAttribute("class", "content-image");
    n3.setAttribute("data-anchor", "image");
    n3.setAttribute("src", "");
    n3.setAttribute("alt", "");
elements.push(n3)
n2.appendChild(n3);
anchors["image"] = n3;
n2.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default Media;

