

// PlotElementTerrainRelief fragment mixin

var  PlotElementTerrainRelief = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n180 = document.createElement("div");
    n180.setAttribute("class", "plot-element terrain");
    n180.setAttribute("data-anchor", "element");
elements.push(n180)
root.appendChild(n180);
anchors["element"] = n180;
n180.appendChild(document.createTextNode("        "));

let n181 = document.createElement("h3");
elements.push(n181)
n180.appendChild(n181);
n181.appendChild(document.createTextNode("Relief"));
n180.appendChild(document.createTextNode("        "));

let n182 = document.createElement("span");
    n182.setAttribute("class", "date");
    n182.setAttribute("data-anchor", "date");
elements.push(n182)
n180.appendChild(n182);
anchors["date"] = n182;
n180.appendChild(document.createTextNode("        "));

let n183 = document.createElement("div");
    n183.setAttribute("class", "options");
    n183.setAttribute("data-anchor", "options");
elements.push(n183)
n180.appendChild(n183);
anchors["options"] = n183;
n183.appendChild(document.createTextNode("            "));

let n184 = document.createElement("span");
    n184.setAttribute("data-option", "plat");
elements.push(n184)
n183.appendChild(n184);
n184.appendChild(document.createTextNode("plat"));
n183.appendChild(document.createTextNode("            "));

let n185 = document.createElement("span");
    n185.setAttribute("data-option", "failble");
elements.push(n185)
n183.appendChild(n185);
n185.appendChild(document.createTextNode("faible pente"));
n183.appendChild(document.createTextNode("            "));

let n186 = document.createElement("span");
    n186.setAttribute("data-option", "fort");
elements.push(n186)
n183.appendChild(n186);
n186.appendChild(document.createTextNode("forte pente"));
n183.appendChild(document.createTextNode("            "));

let n187 = document.createElement("span");
    n187.setAttribute("data-option", "accident");
elements.push(n187)
n183.appendChild(n187);
n187.appendChild(document.createTextNode("accidente"));
n183.appendChild(document.createTextNode("        "));
n180.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementTerrainRelief;

