

// PlotMap fragment mixin

var  PlotMap = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("map");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n98 = document.createElement("section");
    n98.setAttribute("class", "body-section");
elements.push(n98)
root.appendChild(n98);
n98.appendChild(document.createTextNode("        "));

let n99 = document.createElement("p");
elements.push(n99)
n98.appendChild(n99);
n99.appendChild(document.createTextNode("            La carte présente les parcelles pour lesquelles une étude a été initiée."));
n99.appendChild(document.createTextNode("        "));
n98.appendChild(document.createTextNode("        "));

let n100 = document.createElement("p");
elements.push(n100)
n98.appendChild(n100);
n100.appendChild(document.createTextNode("            Pour accéder au résultat de l'étude sur une parcelle, cliquez dessus."));
n100.appendChild(document.createTextNode("        "));
n98.appendChild(document.createTextNode("        "));

let n101 = document.createElement("p");
elements.push(n101)
n98.appendChild(n101);
n101.appendChild(document.createTextNode("            Pour contribuer à l'étude sur une parcelle ou pour initier d'autres études, veuillez contacter "));

let n102 = document.createElement("a");
    n102.setAttribute("href", "mailto:info@terre-en-vue.be");
elements.push(n102)
n101.appendChild(n102);
n102.appendChild(document.createTextNode("Terre-en-vue"));
n101.appendChild(document.createTextNode("."));
n101.appendChild(document.createTextNode("        "));
n98.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n103 = document.createElement("div");
    n103.setAttribute("class", "query-map");
    n103.setAttribute("data-anchor", "map");
elements.push(n103)
root.appendChild(n103);
anchors["map"] = n103;
n103.appendChild(document.createTextNode(" "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotMap;

