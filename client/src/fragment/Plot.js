

// Plot fragment mixin

var  Plot = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("capakey");
names.push("map");
names.push("author");
names.push("results");
names.push("elements");
names.push("links");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n67 = document.createElement("h1");
elements.push(n67)
root.appendChild(n67);
n67.appendChild(document.createTextNode("Étude de parcelle"));
root.appendChild(document.createTextNode("    "));

let n68 = document.createElement("p");
elements.push(n68)
root.appendChild(n68);
n68.appendChild(document.createTextNode("        Identifiant cadastre: "));

let n69 = document.createElement("span");
    n69.setAttribute("data-anchor", "capakey");
elements.push(n69)
n68.appendChild(n69);
anchors["capakey"] = n69;
n68.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n70 = document.createElement("section");
    n70.setAttribute("class", "plot-head");
elements.push(n70)
root.appendChild(n70);
n70.appendChild(document.createTextNode("        "));

let n71 = document.createElement("div");
    n71.setAttribute("class", "plot-map");
    n71.setAttribute("data-anchor", "map");
elements.push(n71)
n70.appendChild(n71);
anchors["map"] = n71;
n71.appendChild(document.createTextNode(" "));
n70.appendChild(document.createTextNode("        "));

let n72 = document.createElement("div");
    n72.setAttribute("class", "plot-meta");
elements.push(n72)
n70.appendChild(n72);
n72.appendChild(document.createTextNode("            "));

let n73 = document.createElement("div");
    n73.setAttribute("class", "");
elements.push(n73)
n72.appendChild(n73);
n73.appendChild(document.createTextNode("                "));

let n74 = document.createElement("a");
    n74.setAttribute("href", "/plotmap");
elements.push(n74)
n73.appendChild(n74);
n74.appendChild(document.createTextNode("Carte générale des parcelles"));
n73.appendChild(document.createTextNode("            "));
n72.appendChild(document.createTextNode("            "));

let n75 = document.createElement("div");
    n75.setAttribute("class", "author");
elements.push(n75)
n72.appendChild(n75);
n75.appendChild(document.createTextNode("Étude initiée par "));

let n76 = document.createElement("span");
    n76.setAttribute("data-anchor", "author");
elements.push(n76)
n75.appendChild(n76);
anchors["author"] = n76;
n75.appendChild(document.createTextNode(" "));
n72.appendChild(document.createTextNode("            "));

let n77 = document.createElement("div");
    n77.setAttribute("class", "results");
    n77.setAttribute("data-anchor", "results");
elements.push(n77)
n72.appendChild(n77);
anchors["results"] = n77;
n77.appendChild(document.createTextNode(" "));
n72.appendChild(document.createTextNode("        "));
n70.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("    "));

let n78 = document.createElement("section");
    n78.setAttribute("class", "plot-body");
elements.push(n78)
root.appendChild(n78);
n78.appendChild(document.createTextNode("        "));

let n79 = document.createElement("div");
    n79.setAttribute("class", "elements-block");
elements.push(n79)
n78.appendChild(n79);
n79.appendChild(document.createTextNode("            "));

let n80 = document.createElement("p");
elements.push(n80)
n79.appendChild(n80);
n80.appendChild(document.createTextNode("                Pouvez-vous répondre à une ou plusieurs des questions suivantes?"));
n80.appendChild(document.createTextNode("            "));
n79.appendChild(document.createTextNode("            "));

let n81 = document.createElement("div");
    n81.setAttribute("class", "elements");
    n81.setAttribute("data-anchor", "elements");
elements.push(n81)
n79.appendChild(n81);
anchors["elements"] = n81;
n81.appendChild(document.createTextNode(" "));
n79.appendChild(document.createTextNode("        "));
n78.appendChild(document.createTextNode("        "));

let n82 = document.createElement("div");
    n82.setAttribute("class", "links");
    n82.setAttribute("data-anchor", "links");
elements.push(n82)
n78.appendChild(n82);
anchors["links"] = n82;
n82.appendChild(document.createTextNode(" "));
n78.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default Plot;

