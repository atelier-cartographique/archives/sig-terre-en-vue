

// PlotInfo fragment mixin

var  PlotInfo = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("municipality.fr");
names.push("municipality.nl");
names.push("area");
names.push("affectation.fr");
names.push("affectation.nl");
names.push("pac_yes");
names.push("pac_no");
names.push("reserve_naturelle_yes");
names.push("reserve_naturelle_no");
names.push("natura2000_yes");
names.push("natura2000_no");
names.push("proprietaire");
names.push("pollution");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first

let n105 = document.createElement("div");
    n105.setAttribute("class", "plot-info");
elements.push(n105)
root.appendChild(n105);
n105.appendChild(document.createTextNode("    "));

let n106 = document.createElement("h2");
elements.push(n106)
n105.appendChild(n106);
n106.appendChild(document.createTextNode("Commune"));
n105.appendChild(document.createTextNode("    "));

let n107 = document.createElement("p");
elements.push(n107)
n105.appendChild(n107);
n107.appendChild(document.createTextNode("        "));

let n108 = document.createElement("span");
    n108.setAttribute("data-anchor", "municipality.fr");
elements.push(n108)
n107.appendChild(n108);
anchors["municipality.fr"] = n108;
n107.appendChild(document.createTextNode(" - "));

let n109 = document.createElement("span");
    n109.setAttribute("data-anchor", "municipality.nl");
elements.push(n109)
n107.appendChild(n109);
anchors["municipality.nl"] = n109;
n107.appendChild(document.createTextNode("    "));
n105.appendChild(document.createTextNode("    "));

let n110 = document.createElement("h2");
elements.push(n110)
n105.appendChild(n110);
n110.appendChild(document.createTextNode("Superficie"));
n105.appendChild(document.createTextNode("    "));

let n111 = document.createElement("p");
elements.push(n111)
n105.appendChild(n111);
n111.appendChild(document.createTextNode("        "));

let n112 = document.createElement("span");
    n112.setAttribute("data-anchor", "area");
elements.push(n112)
n111.appendChild(n112);
anchors["area"] = n112;
n111.appendChild(document.createTextNode(" ha"));
n111.appendChild(document.createTextNode("    "));
n105.appendChild(document.createTextNode("    "));

let n113 = document.createElement("h2");
elements.push(n113)
n105.appendChild(n113);
n113.appendChild(document.createTextNode("Affectation"));
n105.appendChild(document.createTextNode("    "));

let n114 = document.createElement("p");
elements.push(n114)
n105.appendChild(n114);
n114.appendChild(document.createTextNode("        "));

let n115 = document.createElement("span");
    n115.setAttribute("data-anchor", "affectation.fr");
elements.push(n115)
n114.appendChild(n115);
anchors["affectation.fr"] = n115;
n114.appendChild(document.createTextNode(" - "));

let n116 = document.createElement("span");
    n116.setAttribute("data-anchor", "affectation.nl");
elements.push(n116)
n114.appendChild(n116);
anchors["affectation.nl"] = n116;
n114.appendChild(document.createTextNode("    "));
n105.appendChild(document.createTextNode("    "));

let n117 = document.createElement("h2");
elements.push(n117)
n105.appendChild(n117);
n117.appendChild(document.createTextNode("déclaration P.A.C."));
n105.appendChild(document.createTextNode("    "));

let n118 = document.createElement("p");
elements.push(n118)
n105.appendChild(n118);
n118.appendChild(document.createTextNode("        "));

let n119 = document.createElement("span");
    n119.setAttribute("data-anchor", "pac_yes");
elements.push(n119)
n118.appendChild(n119);
anchors["pac_yes"] = n119;
n119.appendChild(document.createTextNode("Oui"));
n118.appendChild(document.createTextNode("        "));

let n120 = document.createElement("span");
    n120.setAttribute("data-anchor", "pac_no");
elements.push(n120)
n118.appendChild(n120);
anchors["pac_no"] = n120;
n120.appendChild(document.createTextNode("Non"));
n118.appendChild(document.createTextNode("    "));
n105.appendChild(document.createTextNode("    "));

let n121 = document.createElement("h2");
elements.push(n121)
n105.appendChild(n121);
n121.appendChild(document.createTextNode("Reserve naturelle"));
n105.appendChild(document.createTextNode("    "));

let n122 = document.createElement("p");
elements.push(n122)
n105.appendChild(n122);
n122.appendChild(document.createTextNode("        "));

let n123 = document.createElement("span");
    n123.setAttribute("data-anchor", "reserve_naturelle_yes");
elements.push(n123)
n122.appendChild(n123);
anchors["reserve_naturelle_yes"] = n123;
n123.appendChild(document.createTextNode("Oui"));
n122.appendChild(document.createTextNode("        "));

let n124 = document.createElement("span");
    n124.setAttribute("data-anchor", "reserve_naturelle_no");
elements.push(n124)
n122.appendChild(n124);
anchors["reserve_naturelle_no"] = n124;
n124.appendChild(document.createTextNode("Non"));
n122.appendChild(document.createTextNode("    "));
n105.appendChild(document.createTextNode("    "));

let n125 = document.createElement("h2");
elements.push(n125)
n105.appendChild(n125);
n125.appendChild(document.createTextNode("Natura 2000"));
n105.appendChild(document.createTextNode("    "));

let n126 = document.createElement("p");
elements.push(n126)
n105.appendChild(n126);
n126.appendChild(document.createTextNode("            "));

let n127 = document.createElement("span");
    n127.setAttribute("data-anchor", "natura2000_yes");
elements.push(n127)
n126.appendChild(n127);
anchors["natura2000_yes"] = n127;
n127.appendChild(document.createTextNode("Oui"));
n126.appendChild(document.createTextNode("            "));

let n128 = document.createElement("span");
    n128.setAttribute("data-anchor", "natura2000_no");
elements.push(n128)
n126.appendChild(n128);
anchors["natura2000_no"] = n128;
n128.appendChild(document.createTextNode("Non"));
n126.appendChild(document.createTextNode("    "));
n105.appendChild(document.createTextNode("    "));
/**
 *  <p>
 *         habitat: <span data-anchor="natura2000_habitat"></span>
 *     </p> 
 */
n105.appendChild(document.createTextNode("    "));

let n129 = document.createElement("h2");
elements.push(n129)
n105.appendChild(n129);
n129.appendChild(document.createTextNode("Nature du propriétaire"));
n105.appendChild(document.createTextNode("    "));

let n130 = document.createElement("p");
elements.push(n130)
n105.appendChild(n130);
n130.appendChild(document.createTextNode("        "));

let n131 = document.createElement("span");
    n131.setAttribute("data-anchor", "proprietaire");
elements.push(n131)
n130.appendChild(n131);
anchors["proprietaire"] = n131;
n130.appendChild(document.createTextNode("    "));
n105.appendChild(document.createTextNode("    "));

let n132 = document.createElement("h2");
elements.push(n132)
n105.appendChild(n132);
n132.appendChild(document.createTextNode("Pollution"));
n105.appendChild(document.createTextNode("    "));

let n133 = document.createElement("p");
elements.push(n133)
n105.appendChild(n133);
n133.appendChild(document.createTextNode("        "));

let n134 = document.createElement("span");
    n134.setAttribute("data-anchor", "pollution");
elements.push(n134)
n133.appendChild(n134);
anchors["pollution"] = n134;
n133.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotInfo;

