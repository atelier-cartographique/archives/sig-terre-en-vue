

// PlotInfoEdit fragment mixin

var  PlotInfoEdit = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first

//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
/**
 * 
 *     <section class="terrain">
 *         <h2>Terrain</h2>
 *         <div data-anchor="terrain.couverture_sol">
 *             <h3>Couverture du sol</h3>
 *             <span data-option="pelouse">pelouse</span>
 *             <span data-option="arbuste">arbustes</span>
 *             <span data-option="arbre">arbres</span>
 *         </div>
 * 
 *         <div data-anchor="terrain.relief">
 *             <h3>Relief</h3>
 *             <span data-option="plat">plat</span>
 *             <span data-option="failble">faible pente</span>
 *             <span data-option="fort">forte pente</span>
 *             <span data-option="accident">accidente</span>
 *         </div>
 * 
 *         <div data-anchor="terrain.qualite">
 *             <h3>Estimation de la qualite du sol</h3>
 *             <span data-option="bon">bonne (présence d'une couverture végétale, sol profond, de couleur brun/noir, avec des verres de terre au premier coup de pelle)</span>
 *             <span data-option="moyen">moyenne</span>
 *             <span data-option="mauvais">mauvaise (pas de couverture végétale, faible profondeur, couleur claire, pas de traces de vie dans le sol)</span>
 *         </div>
 * 
 *         <div data-anchor="terrain.eau_ville">
 *             <h3>Acces a l'eau de ville</h3>
 *             <span data-option="oui">oui</span>
 *             <span data-option="non">non</span>
 *         </div>
 * 
 *         <div data-anchor="terrain.eau_pluie">
 *             <h3>Possibilité de récupérer l'eau de pluie</h3>
 *             <span data-option="citerne">citerne</span>
 *             <span data-option="mare">mare</span>
 *             <span data-option="etang">etang</span>
 *             <span data-option="autre">autre</span>
 *         </div>
 * 
 *         <div data-anchor="terrain.ecologie">
 *             <h3>Présence d'éléments d'un maillage écologique</h3>
 *             <span data-option="haie">haies vives</span>
 *             <span data-option="foret">lisiere de fopret</span>
 *             <span data-option="eau">point d'eau</span>
 *             <span data-option="herbe">bande enherbee</span>
 *             <span data-option="neant">neant</span>
 *         </div>
 * 
 *         <div data-anchor="terrain.pollution">
 *             <h3>Présence de pollutions visibles </h3>
 *             <span data-option="oui">oui</span>
 *             <span data-option="non">non</span>
 *         </div>
 * 
 *         <div data-anchor="terrain.soleil">
 *             <h3>Exposition au soleil </h3>
 *             <span data-option="bon">bonne (3/4 terrain à midi)</span>
 *             <span data-option="moyen">moyenne (entre 3/4 et 1/2 terrain à midi)</span>
 *             <span data-option="mauvais">mauvaise (&lt;1/2 terrain à midi)</span>
 *         </div>
 *     </section> 
 */
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotInfoEdit;

