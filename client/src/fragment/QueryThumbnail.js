

// QueryThumbnail fragment mixin

var  QueryThumbnail = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("name");
names.push("map");
names.push("body");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first

let n268 = document.createElement("div");
    n268.setAttribute("class", "thumbnail-resource thumbnail-query");
elements.push(n268)
root.appendChild(n268);
n268.appendChild(document.createTextNode("    "));

let n269 = document.createElement("div");
    n269.setAttribute("class", "thumbnail-title");
elements.push(n269)
n268.appendChild(n269);
n269.appendChild(document.createTextNode("        "));

let n270 = document.createElement("a");
    n270.setAttribute("data-anchor", "name");
    n270.setAttribute("href", "");
elements.push(n270)
n269.appendChild(n270);
anchors["name"] = n270;
n269.appendChild(document.createTextNode("    "));
n268.appendChild(document.createTextNode("    "));

let n271 = document.createElement("div");
    n271.setAttribute("data-anchor", "map");
    n271.setAttribute("class", "thumbnail-map map");
elements.push(n271)
n268.appendChild(n271);
anchors["map"] = n271;
n271.appendChild(document.createTextNode("    "));
n268.appendChild(document.createTextNode("    "));

let n272 = document.createElement("div");
    n272.setAttribute("data-anchor", "body");
    n272.setAttribute("class", "thumbnail-content code-block");
elements.push(n272)
n268.appendChild(n272);
anchors["body"] = n272;
n272.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default QueryThumbnail;

