

// PlotElementAccesTaille fragment mixin

var  PlotElementAccesTaille = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n211 = document.createElement("div");
    n211.setAttribute("class", "plot-element acces");
    n211.setAttribute("data-anchor", "element");
elements.push(n211)
root.appendChild(n211);
anchors["element"] = n211;
n211.appendChild(document.createTextNode("        "));

let n212 = document.createElement("h3");
elements.push(n212)
n211.appendChild(n212);
n212.appendChild(document.createTextNode("Taille de l'accès à la voirie"));
n211.appendChild(document.createTextNode("        "));

let n213 = document.createElement("span");
    n213.setAttribute("class", "date");
    n213.setAttribute("data-anchor", "date");
elements.push(n213)
n211.appendChild(n213);
anchors["date"] = n213;
n211.appendChild(document.createTextNode("        "));

let n214 = document.createElement("div");
    n214.setAttribute("class", "options");
    n214.setAttribute("data-anchor", "options");
elements.push(n214)
n211.appendChild(n214);
anchors["options"] = n214;
n214.appendChild(document.createTextNode("            "));

let n215 = document.createElement("span");
    n215.setAttribute("data-option", "petit");
elements.push(n215)
n214.appendChild(n215);
n215.appendChild(document.createTextNode("petit (piéton)"));
n214.appendChild(document.createTextNode("            "));

let n216 = document.createElement("span");
    n216.setAttribute("data-option", "moyen");
elements.push(n216)
n214.appendChild(n216);
n216.appendChild(document.createTextNode("moyen (motoculteur)"));
n214.appendChild(document.createTextNode("            "));

let n217 = document.createElement("span");
    n217.setAttribute("data-option", "grand");
elements.push(n217)
n214.appendChild(n217);
n217.appendChild(document.createTextNode("grand (voiture ou tracteur)"));
n214.appendChild(document.createTextNode("        "));
n211.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementAccesTaille;

