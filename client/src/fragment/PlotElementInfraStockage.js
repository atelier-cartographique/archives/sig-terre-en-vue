

// PlotElementInfraStockage fragment mixin

var  PlotElementInfraStockage = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("element");
names.push("date");
names.push("options");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n195 = document.createElement("div");
    n195.setAttribute("class", "plot-element infra");
    n195.setAttribute("data-anchor", "element");
elements.push(n195)
root.appendChild(n195);
anchors["element"] = n195;
n195.appendChild(document.createTextNode("        "));

let n196 = document.createElement("h3");
elements.push(n196)
n195.appendChild(n196);
n196.appendChild(document.createTextNode("Présence d'un cabanon/lieu de stockage de matériel potentiellement utilisable"));
n195.appendChild(document.createTextNode("        "));

let n197 = document.createElement("span");
    n197.setAttribute("class", "date");
    n197.setAttribute("data-anchor", "date");
elements.push(n197)
n195.appendChild(n197);
anchors["date"] = n197;
n195.appendChild(document.createTextNode("        "));

let n198 = document.createElement("div");
    n198.setAttribute("class", "options");
    n198.setAttribute("data-anchor", "options");
elements.push(n198)
n195.appendChild(n198);
anchors["options"] = n198;
n198.appendChild(document.createTextNode("            "));

let n199 = document.createElement("span");
    n199.setAttribute("data-option", "oui");
elements.push(n199)
n198.appendChild(n199);
n199.appendChild(document.createTextNode("oui"));
n198.appendChild(document.createTextNode("            "));

let n200 = document.createElement("span");
    n200.setAttribute("data-option", "non");
elements.push(n200)
n198.appendChild(n200);
n200.appendChild(document.createTextNode("non"));
n198.appendChild(document.createTextNode("            "));

let n201 = document.createElement("span");
    n201.setAttribute("data-option", "potentiel");
elements.push(n201)
n198.appendChild(n201);
n201.appendChild(document.createTextNode("non, mais espace disponible"));
n198.appendChild(document.createTextNode("        "));
n195.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default PlotElementInfraStockage;

