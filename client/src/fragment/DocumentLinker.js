

// DocumentLinker fragment mixin

var  DocumentLinker = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("link");
names.push("unlink");
names.push("name");
names.push("body");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n4 = document.createElement("div");
    n4.setAttribute("class", "thumbnail-resource thumbnail-document");
elements.push(n4)
root.appendChild(n4);
n4.appendChild(document.createTextNode("        "));

let n5 = document.createElement("div");
    n5.setAttribute("class", "button-linker link");
    n5.setAttribute("data-anchor", "link");
elements.push(n5)
n4.appendChild(n5);
anchors["link"] = n5;
n5.appendChild(document.createTextNode("            Lier"));
n5.appendChild(document.createTextNode("        "));
n4.appendChild(document.createTextNode("        "));

let n6 = document.createElement("div");
    n6.setAttribute("class", "button-linker unlink");
    n6.setAttribute("data-anchor", "unlink");
elements.push(n6)
n4.appendChild(n6);
anchors["unlink"] = n6;
n6.appendChild(document.createTextNode("            Délier"));
n6.appendChild(document.createTextNode("        "));
n4.appendChild(document.createTextNode("        "));

let n7 = document.createElement("div");
    n7.setAttribute("class", "thumbnail-title");
elements.push(n7)
n4.appendChild(n7);
n7.appendChild(document.createTextNode("            "));

let n8 = document.createElement("a");
    n8.setAttribute("data-anchor", "name");
    n8.setAttribute("href", "");
elements.push(n8)
n7.appendChild(n8);
anchors["name"] = n8;
n7.appendChild(document.createTextNode("        "));
n4.appendChild(document.createTextNode("        "));

let n9 = document.createElement("div");
    n9.setAttribute("data-anchor", "body");
    n9.setAttribute("class", "thumbnail-content");
elements.push(n9)
n4.appendChild(n9);
anchors["body"] = n9;
n4.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default DocumentLinker;

