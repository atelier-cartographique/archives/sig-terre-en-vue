

// FormMedia fragment mixin

var  FormMedia = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("media");
names.push("submit");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("	"));

let n61 = document.createElement("h1");
elements.push(n61)
root.appendChild(n61);
n61.appendChild(document.createTextNode("Ajouter une image"));
root.appendChild(document.createTextNode("    "));

let n62 = document.createElement("fieldset");
elements.push(n62)
root.appendChild(n62);
n62.appendChild(document.createTextNode("    "));

let n63 = document.createElement("legend");
elements.push(n63)
n62.appendChild(n63);
n63.appendChild(document.createTextNode("Sélectionnez une image depuis votre système."));
n62.appendChild(document.createTextNode("        "));

let n64 = document.createElement("input");
    n64.setAttribute("data-anchor", "media");
    n64.setAttribute("type", "file");
    n64.setAttribute("class", "large");
    n64.setAttribute("name", "media");
    n64.setAttribute("value", "");
    n64.setAttribute("required", "None");
elements.push(n64)
n62.appendChild(n64);
anchors["media"] = n64;
n62.appendChild(document.createTextNode("    "));
root.appendChild(document.createTextNode("	"));

let n65 = document.createElement("div");
    n65.setAttribute("class", "button-box");
elements.push(n65)
root.appendChild(n65);
n65.appendChild(document.createTextNode("		"));

let n66 = document.createElement("div");
    n66.setAttribute("class", "button");
    n66.setAttribute("data-anchor", "submit");
elements.push(n66)
n65.appendChild(n66);
anchors["submit"] = n66;
n66.appendChild(document.createTextNode("Ajouter"));
n65.appendChild(document.createTextNode("	"));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default FormMedia;

