

// Navigation fragment mixin

var  Navigation = Base => class extends Base {


names () {
    let names = new Array();
//< generated code - if anything wrong in there, check the template first
names.push("title");
names.push("author");
names.push("login");
names.push("logout");
names.push("tools");
//> end of generated code
    return names;
};


build () {
    const anchors = this.anchors;
    const root = this.el;
    const elements = [];

//< generated code - if anything wrong in there, check the template first
root.appendChild(document.createTextNode("    "));

let n44 = document.createElement("header");
    n44.setAttribute("class", "top-bar");
elements.push(n44)
root.appendChild(n44);
n44.appendChild(document.createTextNode("        "));

let n45 = document.createElement("div");
    n45.setAttribute("class", "top-bar-logo");
elements.push(n45)
n44.appendChild(n45);
n45.appendChild(document.createTextNode("            "));

let n46 = document.createElement("a");
    n46.setAttribute("data-anchor", "title");
    n46.setAttribute("href", "/");
elements.push(n46)
n45.appendChild(n46);
anchors["title"] = n46;
n46.appendChild(document.createTextNode("Bruxelles ma terre"));
n45.appendChild(document.createTextNode("        "));
n44.appendChild(document.createTextNode("        "));

let n47 = document.createElement("div");
    n47.setAttribute("class", "top-bar-context");
elements.push(n47)
n44.appendChild(n47);
n47.appendChild(document.createTextNode("            "));

let n48 = document.createElement("span");
    n48.setAttribute("data-anchor", "author");
elements.push(n48)
n47.appendChild(n48);
anchors["author"] = n48;
n47.appendChild(document.createTextNode("        "));
n44.appendChild(document.createTextNode("        "));

let n49 = document.createElement("div");
    n49.setAttribute("class", "top-bar-login");
elements.push(n49)
n44.appendChild(n49);
n49.appendChild(document.createTextNode("            "));

let n50 = document.createElement("a");
    n50.setAttribute("data-anchor", "login");
    n50.setAttribute("href", "/login");
    n50.setAttribute("class", "tool-link");
elements.push(n50)
n49.appendChild(n50);
anchors["login"] = n50;
n50.appendChild(document.createTextNode("login"));
n49.appendChild(document.createTextNode("            "));

let n51 = document.createElement("span");
    n51.setAttribute("data-anchor", "logout");
elements.push(n51)
n49.appendChild(n51);
anchors["logout"] = n51;
n51.appendChild(document.createTextNode("                "));

let n52 = document.createElement("span");
    n52.setAttribute("class", "tools");
elements.push(n52)
n51.appendChild(n52);
n52.appendChild(document.createTextNode("                    "));
/**
 *  <span data-anchor="toolsToggle"
 *                           class="tool-link tools-toggle">
 *                         +
 *                     </span> 
 */
n52.appendChild(document.createTextNode("                    "));

let n53 = document.createElement("span");
    n53.setAttribute("data-anchor", "tools");
elements.push(n53)
n52.appendChild(n53);
anchors["tools"] = n53;
n53.appendChild(document.createTextNode("                        "));

let n54 = document.createElement("a");
    n54.setAttribute("href", "/form/plot");
    n54.setAttribute("class", "tool-link");
elements.push(n54)
n53.appendChild(n54);
n54.appendChild(document.createTextNode("parcelle"));
n53.appendChild(document.createTextNode("                        "));

let n55 = document.createElement("a");
    n55.setAttribute("href", "/form/media");
    n55.setAttribute("class", "tool-link");
elements.push(n55)
n53.appendChild(n55);
n55.appendChild(document.createTextNode("image"));
n53.appendChild(document.createTextNode("                        "));

let n56 = document.createElement("a");
    n56.setAttribute("href", "/form/document");
    n56.setAttribute("class", "tool-link");
elements.push(n56)
n53.appendChild(n56);
n56.appendChild(document.createTextNode("document"));
n53.appendChild(document.createTextNode("                        "));
/**
 *  <a href="/form/query"  class="tool-link">query</a> 
 */
n53.appendChild(document.createTextNode("                    "));
n52.appendChild(document.createTextNode("                "));
n51.appendChild(document.createTextNode("                "));

let n57 = document.createElement("a");
    n57.setAttribute("href", "/logout");
    n57.setAttribute("class", "tool-link");
elements.push(n57)
n51.appendChild(n57);
n57.appendChild(document.createTextNode("logout"));
n51.appendChild(document.createTextNode("            "));
n49.appendChild(document.createTextNode("        "));
n44.appendChild(document.createTextNode("    "));
//> end of generated code

    Object.defineProperty(this, 'clear', {
        value: function () {
            elements.forEach((element) => {
                if (element && element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            })
        }
    })
    return this;
};

}

export default Navigation;

