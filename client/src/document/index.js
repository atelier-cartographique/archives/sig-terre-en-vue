import _ from 'lodash';
import Action from '../proto/Action';
import App from '../proto/App';
import DocComp from '../component/Document';
import LinkList from '../component/LinkList';

class Doc extends App {
    constructor(...args) {
        super(...args);
        this.doc = new DocComp();
        this.linkList;
        this.addFragment(this.doc.fragment);
    }

    get appName () {
        return 'document';
    }

    get urlPattern () {
        return '/user/:uid/document/:rid';
    }

    activate(options) {
        if (('rid' in options)
            && ('uid' in options)) {
            if (this.linkList) {
                this.removeFragment(this.linkList.fragment);
            }
            this.linkList = new LinkList(options.rid, this.sys.api);
            this.addFragment(this.linkList.fragment);


            this.sys.api.request('getDocument', {
                'user-id': options.uid,
                'resource-id': options.rid
            }).then(response => {
                this.sys.store.dispatch(new Action('app/document@select', {
                    'id': options.rid
                }));
            });

            this.sys.api.request('getUser', {
                'user-id': options.uid
            }).then(response => {
                this.sys.store.dispatch(new Action('app/document@selectUser', {
                    'id': options.uid
                }));
            });


        }
    }
}

export default Doc;
