/**
 * sys/models
 */

import Type from '../proto/Type';


let api;
function configure (apiClient) {
    api = apiClient;
}

function commonOperations (typeName) {
    return ['get', 'list', 'post', 'update'].map(p => p + typeName);
}

class User extends Type {

    static fetch (id) {
        const params = { 'user-id': id };
        return api.request('getUser', params)
                  .then(response => {
                      return new User(response.obj);
                  });
    }

}

// class User extends Type {
//
//     static fetch (id) {
//         const params = { 'user-id': id };
//         return api.request('getUser', params)
//                   .then(response => {
//                       return new User(response.obj);
//                   });
//     }
//
// }


export {
    configure,
    User
};
