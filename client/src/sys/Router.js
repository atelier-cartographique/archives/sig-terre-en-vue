
import page from 'page';

export function start() {
    page.start({
        'click': true,
        'popstate': true,
        'dispatch': true, // [from docs] If you wish to load serve initial content from the server you likely will want to set dispatch to false.
        'hashbang': false,
        'decodeURLComponents': true
    });
};

export function navigate (...args) {
    page.show(...args);
};

export function addRoute() {
    if (arguments.length > 1) {
        page.apply(page, arguments);
    }
};
