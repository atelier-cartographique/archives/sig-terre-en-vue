import _ from 'lodash';
import helpers from '../helpers';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import Action from '../proto/Action';
import Emitter from '../proto/Emitter';
import Reducer from '../proto/Reducer';
import {REDUCERS, INITIAL_STATE} from './reducers';


function logger (mw) {
    const fn = next => {
        const fa = action => {
            // console.log(action);
            next(action);
        };
        return fa;
    };
    return fn;
}

function serializeAction (mw) {
    const fn = next => {
        const fa = action => {
            next(action.toPlainObject());
        };
        return fa;
    };
    return fn;
}

const middlewares = applyMiddleware(
    logger,
    serializeAction
);


class Transmitter extends Emitter {
    constructor () {
        super();
    }
}

const transmitter = new Transmitter();

function transmit(path, action) {
    console.log('transmit', path);
    transmitter.emit(path, action);
}

let combined;

function proxyReducer (previousState, action) {
    if (combined) {
        return combined(previousState, action);
    }
    if (!previousState) {
        return INITIAL_STATE;
    }
    return previousState;
}

(function (rdcrs) {
    // this thing is supposed to set a unique reducer per domain
    rdcrs.forEach(r => {
        console.log(`reducer ${r.path}`);
    });
    const prepared0 = _.reduce(rdcrs, (acc, r) => {
        if (!(r instanceof Reducer)) {
            console.error('hmm, not a reducer');
            return acc;
        };
        if (!(r.domain in acc)) {
            acc[r.domain] = [];
        }
        acc[r.domain].push(r.reducer(transmit, INITIAL_STATE));
        return acc;
    }, {});

    const prepared = _.reduce(prepared0, (acc, fns, domain) => {
        acc[domain] = function domainReducer (state0, action) {
            if (action.domain === domain) {
                return _.reduce(fns, (state, fn) => fn(state, action), state0);
            }
            if (!state0) {
                return INITIAL_STATE[domain];
            }
            return state0;
        };
        console.log('combined', domain, fns.length);
        return acc;
    }, {});
    combined = combineReducers(prepared);

})(REDUCERS);


const store = createStore(proxyReducer, INITIAL_STATE, middlewares);

export function get(p, defaults) {
    return _.result(store.getState(), p, _.result(INITIAL_STATE, p, defaults));
};

export function dispatch(action) {
    store.dispatch(action);
};

export function subscribe(path, fn) {
    console.log('store.subscribe', path);
    transmitter.on(path, fn);
    const unsubscribe = () => {
        transmitter.removeListener(path, fn);
    };
    return unsubscribe;
};

export default {
    'get': get,
    'dispatch': dispatch,
    'subscribe': subscribe
};
