import _ from 'lodash';
import {px} from '../helpers';
import {start, addRoute} from './Router';
import Store from './Store';
import ComponentFactory from './ComponentFactory';



function moveFragment (name, node, initial, final, duration) {
    let mover = (ender) => {
        let start = null;
        const interval = Math.abs(final - initial);
        let step = (ts) => {
            if (null === start) {
                start = ts;
            }
            const t = ts - start,
                elapsed =  t / duration,
                offset = interval * elapsed,
                current = initial < final ? initial + offset : initial - offset;
            // console.log('move', name, initial, final, offset, current);
            node.style.left = px(current);
            if (t > duration) {
                node.style.left = px(final);
                if (ender) {
                    ender();
                }
            }
            else {
                window.requestAnimationFrame(step);
            }
        };
        window.requestAnimationFrame(step);
    };
    return mover;
}


class Sys {
    constructor(viewport, api) {
        this.viewport = viewport;
        this.apps = {};
        this.currentApp = null;
        Object.defineProperty(this, 'store', {'value': Store});
        Object.defineProperty(this, 'api', {'value': api});

    }

    boot() {
        start();
    }

    fragmentOutView(fragment, name) {
        const rect = this.viewport.getBoundingClientRect();
        fragment.style.zIndex = 0;
        return moveFragment(name, fragment, 0, rect.width, 300);
    }

    fragmentInView(fragment, name) {
        const rect = this.viewport.getBoundingClientRect();
        fragment.style.top = px(0);
        fragment.style.right = px(0);
        fragment.style.bottom = px(0);
        fragment.style.left = px(rect.width);
        fragment.style.zIndex = 10;
        return moveFragment(name, fragment, rect.width, 0, 300);
    }

    registerApp(Proto) {
        const app = new Proto(this),
            name = app.getName(),
            node = app.getNode(),
            rect = this.viewport.getBoundingClientRect();

        node.style.width = px(rect.width);
        node.style.height = px(rect.height);

        this.apps[name] = app;
        this.viewport.appendChild(node);
        this.fragmentOutView(node, name)();

        addRoute(app.getUrl(), ctx => {
            this.openApp(name, ctx.params || {});
        });
    }

    closeApp(name) {
        const app = this.apps[name];
        app.deactivate();
        this.currentApp = null;
        return this.fragmentOutView(app.getNode(), name);
    }

    openApp(name, options) {
        const currentApp = this.currentApp, app = this.apps[name], fragment = app.getNode();

    // console.log('OPEN', name, options);
        let closer = null;
        if (currentApp && (name !== currentApp)) {
            closer = this.closeApp(currentApp);
        }
        this.fragmentInView(fragment, name)(closer);
        app.activate(options);
        this.currentApp = name;
    }
}

export default Sys;
