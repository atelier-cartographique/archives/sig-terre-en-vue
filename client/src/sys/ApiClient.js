import {find} from 'lodash/fp';
import h from '../helpers';
import Swagger from 'swagger-client';
import HTTPClient from '../proto/HTTPClient';
import Store from './Store';
import Emitter from '../proto/Emitter';
import Reducer from '../proto/Reducer';
import Action from '../proto/Action';


const DOMAIN = 'api';

function actionName (type) {
    return `${DOMAIN}/${type}`;
}

function requestStartAction (name) {
    return (new Action(actionName(`${name}@start`), {
        'id': 'id'
    }));
}

function requestEnd (name, id, response) {
    return (new Action(actionName('request'), {
        'id': id,
        'name': name,
        'response': response
    }));
}

function requestEndAction (name, id, response) {
    return (new Action(actionName(`${name}@end`), {
        'id': id,
        'response': response
    }));
}

function requestFailAction (name, id, err) {
    return (new Action(actionName(`${name}@fail`), {
        'id': id,
        'error': err
    }));
}

function mkCacheKey (name, obj) {
    const ho = JSON.stringify(obj);
    return `${name}#${ho}`;
}

class Client extends Emitter {

    start(url) {
        const setApi = function (api) {
            Object.defineProperty(this, 'swagger', {'value': api});
            Object.defineProperty(this, 'api', {'value': api});
            Object.defineProperty(this, '_cache', {'value': new Map()});
            this.isReady = true;
            this.emit('ready');
        };

        const swagger = new Swagger({
            url,
            usePromise: true,
            client: new HTTPClient()
        }).then(setApi.bind(this));
    }

    getOperation (name) {
        const apis = this.api.apis.default,
            op = apis.apis[name],
            fn = apis[name];
        return [op, fn];
    }

    request(name, data={}, options={}) {

        if (!this.isReady) {
            const p = new Promise((resolve, reject) => {
                this.once('ready', () => {
                    return this.request(name, data).then(resolve, reject);
                });
            });
            return p;
        }

        const id = _.uniqueId('api.');
        const [op, request] = this.getOperation(name);

        if (op) {
            let isGet = 'GET' === op.method.toUpperCase();

            const success = response => {
                if (isGet) {
                    this._cache.set(mkCacheKey(name, data), response);
                }
                Store.dispatch(requestEnd(name, id, response));
                Store.dispatch(requestEndAction(name, id, response));
                return response;
            };

            const failure = err => {
                console.error('request failed', name, err);
                Store.dispatch(requestFailAction(name, id, err));
                return err;
            };

            if (isGet && !options.fresh) {
                const cachedData = this._cache.get(mkCacheKey(name, data));
                if (cachedData) {
                    isGet = false;
                    return Promise.resolve(success(cachedData));
                }
            }
            return request(data).then(success, failure);
        }

        throw (new Error(`Unknown API request: ${name}`));
    }

    getDomain() {
        return DOMAIN;
    }
}

export default Client;
