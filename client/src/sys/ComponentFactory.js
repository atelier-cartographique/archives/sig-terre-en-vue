import Component from '../proto/Component';
import Fragment from '../proto/Fragment';

const fragments = {
    'HOME': require('../fragment/Home'),
    'USERLABEL': require('../fragment/UserLabel'),
    'USERINFO': require('../fragment/UserInfo'),
    'DOCUMENTTHUMBNAIL': require('../fragment/DocumentThumbnail'),
    'QUERYTHUMBNAIL': require('../fragment/QueryThumbnail'),
};

export default name => {
    const uname = name.toUpperCase(name), F = Fragment.create(fragments[uname]), T = Component.create(() => {}, F);
    return T;
};
