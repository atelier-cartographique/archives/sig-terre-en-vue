import {assign, filter, uniqBy} from 'lodash/fp';
import {setProperty} from '../helpers';
import Reducer from '../proto/Reducer';


const REDUCERS = [];
const INITIAL_STATE = {
    'api': {
        'request': {},
        'users': [],
        'documents': [],
        'queries': [],
        'medias': [],
        'plots': [],
        'links': []
    },

    'sys': {
        'user': {
            'id': null
        }
    },

    'app': {
        'user': {
            'ready': false,
            'id': null
        },
        'linker': {
            'ready': false,
            'id': null
        },
        'document': {
            'ready': false,
            'id': null
        },
        'query': {
            'ready': false,
            'id': null,
            'result': null
        },
        'media': {
            'ready': false,
            'id': null
        },
        'plot': {
            'id': null,
            'elements': []
        },
        'form': {
            'rid': null
        }
    },

    'comp': {
        'link': {}
    }
};


function filterId (id) {
    return filter(item => item.id !== id);
}

/**
 **  API REDUCERS
 */


function apiRequest (state, action) {
    if (action.is('api', 'request')) {
        state = assign({}, state);
        state.request = action.payload;

        return state;
    }
}
REDUCERS.push(new Reducer(apiRequest, 'api.request'));

function apiGetReducer (type, collectionName) {
    const actionName = `get${type}@end`;
    function r (state, action) {
        if (action.is('api', actionName)) {
            state = assign({}, state);
            const response = action.payload.response,
                obj = response.obj,
                collection = state[collectionName];
            collection.push(obj);
            state[collectionName] = uniqBy('id', collection);
            console.log(`store.${actionName}`);
            return state;
        }
    };
    REDUCERS.push(new Reducer(r, `api.${collectionName}`));
}

function apiListReducer (type, collectionName) {
    const actionName = `list${type}@end`;
    function r (state, action) {
        if (action.is('api', actionName)) {
            state = assign({}, state);
            const response = action.payload.response,
                collection = response.obj.concat(state[collectionName]);
            state[collectionName] = uniqBy('id', collection);
            console.log(`store.${actionName}`);
            return state;
        }
    }
    REDUCERS.push(new Reducer(r, `api.${collectionName}`));
}

const Types = [
    ['Document', 'documents'],
    ['User', 'users'],
    ['Media', 'medias'],
    ['Plot', 'plots'],
    ['Query', 'queries']
];

Types.forEach(t => {
    const [type, collectionName] = t;
    apiGetReducer(type, collectionName);
    apiListReducer(type, collectionName);
});
//
// function getUser (state, action) {
//     if (action.is('api', 'getUser@end')) {
//         state = assign({}, state);
//         const response = action.payload.response;
//         state.users.push(response.obj);
//
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(getUser, 'api.users'));
//
// function listUser (state, action) {
//     if (action.is('api', 'listUser@end')) {
//         state = assign({}, state);
//         const response = action.payload.response,
//             users = response.obj.concat(state.users);
//         state.users = uniqBy('id', users);
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(listUser, 'api.users'));
//
// function listDocument (state, action) {
//     if (action.is('api', 'listDocument@end')) {
//         state = assign({}, state);
//         const response = action.payload.response,
//             documents = response.obj.concat(state.documents);
//         state.documents = uniqBy('id', documents);
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(listDocument, 'api.documents'));
//
// function listQuery (state, action) {
//     if (action.is('api', 'listQuery@end')) {
//         state = assign({}, state);
//         const response = action.payload.response,
//             queries = response.obj.concat(state.queries);
//         state.queries = uniqBy('id', queries);
//
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(listQuery, 'api.queries'));
//
// function listMedia (state, action) {
//     if (action.is('api', 'listMedia@end')) {
//         state = assign({}, state);
//         const response = action.payload.response,
//             medias = response.obj.concat(state.medias);
//         state.medias = uniqBy('id', medias);
//
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(listMedia, 'api.medias'));
//
// function listPlot (state, action) {
//     if (action.is('api', 'listPlot@end')) {
//         state = assign({}, state);
//         const response = action.payload.response,
//             plots = response.obj.concat(state.plots);
//         state.medias = uniqBy('id', plots);
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(listPlot, 'api.plots'));
//
// function listLink (state, action) {
//     if (action.is('api', 'listLink@end')) {
//         state = assign({}, state);
//         const response = action.payload.response,
//             links = response.obj.concat(state.links);
//         state.links = uniqBy('id', links);
//
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(listLink, 'api.links'));
//
//
// function getPlot (state, action) {
//     if (action.is('api', 'getPlot@end')) {
//         state = assign({}, state);
//         const response = action.payload.response,
//             plot = response.obj,
//             plots = filterId(plot.id)(state.plots);
//         plots.push(plot);
//         state.plots = plots;
//
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(getDocument, 'api.plots'));
//
//
// function getDocument (state, action) {
//     if (action.is('api', 'getDocument@end')) {
//         state = assign({}, state);
//         const response = action.payload.response,
//             doc = response.obj,
//             documents = filterId(doc.id)(state.documents);
//         documents.push(doc);
//         state.documents = documents;
//
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(getDocument, 'api.documents'));
//
// function getQuery (state, action) {
//     if (action.is('api', 'getQuery@end')) {
//         state = assign({}, state);
//         const response = action.payload.response,
//             query = response.obj,
//             queries = filterId(query.id)(state.queries);
//         queries.push(query);
//         state.queries = queries;
//
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(getQuery, 'api.queries'));
//
// function getMedia (state, action) {
//     if (action.is('api', 'getMedia@end')) {
//         state = assign({}, state);
//         const response = action.payload.response,
//             media = response.obj,
//             medias = filterId(media.id)(state.medias);
//         medias.push(media);
//         state.medias = medias;
//
//         return state;
//     }
// }
// REDUCERS.push(new Reducer(getMedia, 'api.medias'));

/**
 * APP REDUCERS
 */

function userSelect (state, action) {
    if (action.is('app', 'user@select')) {
        state = assign({}, state);
        setProperty(state, 'user', {
            'ready': false,
            'id': action.payload.id
        });

        return state;
    }
}
REDUCERS.push(new Reducer(userSelect, 'app.user'));

function userLinkerSelect (state, action) {
    if (action.is('app', 'linker@select')) {
        state = assign({}, state);
        setProperty(state, 'linker', {
            'ready': false,
            'id': action.payload.id
        });

        return state;
    }
}
REDUCERS.push(new Reducer(userLinkerSelect, 'app.linker'));



function documentSelect (state, action) {
    if (action.is('app', 'document@select')) {
        state = assign({}, state);
        state.document.id = action.payload.id;
        return state;
    }
}
REDUCERS.push(new Reducer(documentSelect, 'app.document.id'));

function documentUserSelect (state, action) {
    if (action.is('app', 'document@selectUser')) {
        state = assign({}, state);
        state.document.user = action.payload.id;
        return state;
    }
}
REDUCERS.push(new Reducer(documentUserSelect, 'app.document.user'));



function querySelect (state, action) {
    if (action.is('app', 'query@select')) {
        state = assign({}, state);
        setProperty(state, 'query', {
            'ready': false,
            'id': action.payload.id
        });

        return state;
    }
}
REDUCERS.push(new Reducer(querySelect, 'app.query'));


function queryResult (state, action) {
    if (action.is('app', 'query@result')) {
        state = assign({}, state);
        setProperty(state, 'query', {
            'id': state.query.id,
            'ready': true,
            'result': action.payload
        });

        return state;
    }
}
REDUCERS.push(new Reducer(queryResult, 'app.query.result'));


function mediaSelect (state, action) {
    if (action.is('app', 'media@select')) {
        state = assign({}, state);
        setProperty(state, 'media', {
            'ready': false,
            'id': action.payload.id
        });

        return state;
    }
}
REDUCERS.push(new Reducer(mediaSelect, 'app.media'));


function formEdit (state, action) {
    if (action.is('app', 'form@edit')) {
        state = assign({}, state);
        state.form.resource = action.payload;

        return state;
    }
}
REDUCERS.push(new Reducer(formEdit, 'app.form'));

/**
 * COMPONENT REDUCERS
 */

function linkSelect (state, action) {
    if (action.is('comp', 'link@select')) {
        state = assign({}, state);
        state.link[action.payload.id] = false;

        return state;
    }
}
REDUCERS.push(new Reducer(linkSelect, 'comp.link'));

function linkSelected (state, action) {
    if (action.is('comp', 'link@selected')) {
        state = assign({}, state);
        state.link[action.payload.id] = true;

        return state;
    }
}
REDUCERS.push(new Reducer(linkSelected, 'comp.link'));

/**
 * SYS REDUCERS
 */

function sysAction (state, action) {
    return state;
}
REDUCERS.push(new Reducer(sysAction, 'sys'));


function login (state, action) {
    if (action.is('sys', 'login')) {
        state = assign({}, state);
        state.user = action.payload;

        return state;
    }
}
REDUCERS.push(new Reducer(login, 'sys.user'));


export {REDUCERS, INITIAL_STATE};
// export var INITIAL_STATE = Object.freeze(INITIAL_STATE);
