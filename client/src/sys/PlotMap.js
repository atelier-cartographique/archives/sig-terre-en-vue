import {pick, head, join} from 'lodash/fp';
import Action from '../proto/Action';
import App from '../proto/App';
import Fragment from '../proto/Fragment';
import PlotMapF from '../fragment/PlotMap';
import TevMap from '../proto/Map';
import ol from '../../vendors/ol/ol-debug.js';
import {navigate} from '../sys/Router';

const PF = Fragment(PlotMapF);
const mkProps = pick(['capakey', 'id', 'user_id']);
const mkPath = (uid, pid) => join('/')(['/user', uid, 'plot', pid]);

class PlotMap extends App {
    constructor(...args) {
        super(...args);
        this.plotMap = new PF();
        this.addFragment(this.plotMap);
    }

    get appName () {
        return 'plotmap';
    }

    get urlPattern () {
        return '/plotmap';
    }

    activate(options) {
        if (!this.map) {
            this.map = new TevMap();
            this.plotMap.applyTo('map', function(node){
                this.map.attach(node, []);
                const select = new ol.interaction.Select();
                select.on('select', event => {
                    const feature = event.target.getFeatures().pop(),
                        props = feature.getProperties();
                    navigate(mkPath(props.user_id, props.id));
                });
                this.map.olMap.addInteraction(select);
            }, this);

            const request = this.sys.api.request.bind(this.sys.api);
            request('listAllPlot', {}, {'fresh': true})
                .then(response => {
                    const obj = response.obj;
                    this.map.source.clear();
                    obj.forEach(plot => {
                        this.map.addGeometry(plot.geometry, mkProps(plot));
                    });
                    this.map.fitSource();
                });
        }
    }
}

export default PlotMap;
