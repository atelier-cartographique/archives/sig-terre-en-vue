
import Emitter from './Emitter';
import _ from 'lodash';
import {stamp, getStamp, removeElement} from '../helpers';


function appToClassName (app) {
    return app.getName().replace('.', '-');
}


class App  extends Emitter {
    constructor (sys) {
        if  (!sys) {
            throw (new Error('A Sys instance is required'));
        }
        super();
        Object.defineProperty(this, 'sys', {'value': sys});
        this.rootElement = document.createElement('section');
        this.rootElement.setAttribute('class',
                                'app content ' + appToClassName(this));

    }

    getName () {
        return _.result(this, 'appName');
    }

    getUrl () {
        const upat = _.result(this, 'urlPattern');
        if (upat) {
            return upat;
        }
        return `/${this.getName()}`;
    }

    activate () {
        this.emit('activate');
    }

    deactivate () {
        this.emit('deactivate');
    }

    getNode () {
        return this.rootElement;
    }

    addFragment (frag, rootElement) {
        if (!this.fragelementlist) {
            Object.defineProperty(this, 'fragelementlist', {'value':{}});
        }
        const key = stamp(frag),
            elements = [],
            root = rootElement || this.rootElement;
        _.forEach(frag.el.childNodes, node => {
            elements.push(node);
        });
        root.appendChild(frag.el);
        this.fragelementlist[key] = elements;
    }

    removeFragment (frag) {
        if (frag) {
            const key = getStamp(frag),
                elements = this.fragelementlist[key];
            _.forEach(elements, removeElement);
            delete this.fragelementlist[key];
        }
    }

    onStateChange (state) {
        // just saves an IF, it's supposed to be overwritten
    }

};

export default App;
