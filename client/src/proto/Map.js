
import Emitter from './Emitter';
import proj4 from '../../vendors/proj4.js';
import openLayers from '../../vendors/ol/ol-debug.js';
import {removeElement, addClass, removeClass, appendText, isKeyCode , selectRole, select, getData, _} from '../helpers';

export const GeoJSONFormat = new openLayers.format.GeoJSON();

(function addProjections (ol, p4) {
    ol.proj.setProj4(p4);

    p4.defs('EPSG:31370',
        '+proj=lcc +lat_1=51.16666723333333 +lat_2=49.8333339 +lat_0=90 +lon_0=4.367486666666666 +x_0=150000.013 +y_0=5400088.438 +ellps=intl +towgs84=-106.8686,52.2978,-103.7239,-0.3366,0.457,-1.8422,-1.2747 +units=m +no_defs');

    ol.proj.addProjection(new ol.proj.Projection({
        'code': 'EPSG:31370',
        'extent': [14697.30, 22635.80, 291071.84, 246456.18]
    }));
})(openLayers, proj4);


function getStyle (ol) {

    const image = new ol.style.Circle({
        radius: 5,
        fill: null,
        stroke: new ol.style.Stroke({color: 'red', width: 1})
    });

    const styles = {
        'Point': new ol.style.Style({
            image
        }),
        'LineString': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'green',
                width: 1
            })
        }),
        'MultiLineString': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'green',
                width: 1
            })
        }),
        'MultiPoint': new ol.style.Style({
            image
        }),
        'MultiPolygon': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'rgb(0, 158, 224)',
                width: 1
            }),
            fill: new ol.style.Fill({
                color: 'rgba(0, 158, 224, 0.1)'
            })
        }),
        'Polygon': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'rgb(0, 158, 224)',
                width: 1
            }),
            fill: new ol.style.Fill({
                color: 'rgba(0, 158, 224, 0.1)'
            })
        }),
        'GeometryCollection': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'magenta',
                width: 2
            }),
            fill: new ol.style.Fill({
                color: 'magenta'
            }),
            image: new ol.style.Circle({
                radius: 10,
                fill: null,
                stroke: new ol.style.Stroke({
                    color: 'magenta'
                })
            })
        }),
        'Circle': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'red',
                width: 2
            }),
            fill: new ol.style.Fill({
                color: 'rgba(255,0,0,0.2)'
            })
        })
    };

    function styleFunction (feature) {
        return styles[feature.getGeometry().getType()];
    }

    return styleFunction;
}


function getFeatureLayer (ol, features=[]) {
    const proj = ol.proj.get('EPSG:31370');
    const geojsonObject = {
        'type': 'FeatureCollection',
        'crs': {
            'type': 'name',
            'properties': {
                // 'name': 'EPSG:3857'
                'name': 'EPSG:31370'
            }
        },
        'features': features
    };

    const vectorSource = new ol.source.Vector({
        'projection': proj,
        'features': GeoJSONFormat.readFeatures(geojsonObject)
    });

    const vectorLayer = new ol.layer.Vector({
        source: vectorSource,
        style: getStyle(ol)
    });

    return [vectorLayer, vectorSource];
}

function getBaseLayer (ol) {

    const proj = ol.proj.get('EPSG:31370');
    const source = new ol.source.ImageWMS({
        'params': {'LAYERS': 'mrbc'},
        'projection': proj,
        'url': '/service',
    });
    // var source = new ol.source.ImageWMS({
    //     'params': {'LAYERS': 'ortho'},
    //     'projection': proj,
    //     'ratio': 1,
    //     'url': 'http://tev.local/wms',
    // });

    return (new ol.layer.Image({
        'source': source,
        'extent': [140992.873, 161264.524, 158205.373, 178369.174]
    }));
}

const DEFAULT_SOURCE = Symbol('tev.map.DEFAULT_SOURCE');

class TevMap extends Emitter {

    getSource (sourceIdentifier, olMap) {
        if (!(sourceIdentifier in this.sources)) {
            // create one
            const [layer, source] = getFeatureLayer(openLayers);
            this.sources[sourceIdentifier] = source;
            olMap.addLayer(layer);
        }
        return this.sources[sourceIdentifier];
    }

    withMap (fn) {
        if (!this.olMap) {
            return this.once('attach', () => this.fn(this.olMap));
        }
        fn(this.olMap);
    }

    withSource (fn, sourceIdentifier=DEFAULT_SOURCE) {
        this.withMap(olMap => {
            fn(this.getSource(sourceIdentifier, olMap));
        });
    }

    addGeometry(geometry, props={}, sourceIdentifier=DEFAULT_SOURCE) {
        const data = {
            'type': 'Feature',
            'crs': { 'type': 'name', 'properties': { 'name': 'EPSG:31370' } },
            'geometry': geometry,
            'properties': props
        };
        const feature = GeoJSONFormat.readFeature(data);
        this.withMap(olMap => {
            this.getSource(sourceIdentifier, olMap)
                .addFeature(feature);
        });
        return feature;
    }

    fitSource (sourceIdentifier=DEFAULT_SOURCE) {
        this.withMap(olMap => {
            if (!(sourceIdentifier in this.sources)) return;

            const source = this.getSource(sourceIdentifier, olMap),
                geoExtent = source.getExtent(),
                center = openLayers.extent.getCenter(geoExtent);
            if (Number.isNaN(center[0]) || Number.isNaN(center[1])) {
                return;
            }
            try {
                olMap.getView().fit(geoExtent, olMap.getSize());
            }
            catch (err) {
                console.error(err);
            }
        });
    }

    attach(elem, features) {
        const baseLayer = getBaseLayer(openLayers),
            [layer, source] = getFeatureLayer(openLayers, features);

        let geoExtent = source.getExtent();
        let center = openLayers.extent.getCenter(geoExtent);

        if (Number.isNaN(center[0]) || Number.isNaN(center[1])) {
            geoExtent = [140992.873, 161264.524, 158205.373, 178369.174];
            center = openLayers.extent.getCenter(geoExtent);
        }

        const map = new openLayers.Map({
            // layers: [baseLayer],
            'layers': [baseLayer, layer],
            'target': elem,
            'view': new openLayers.View({
                'projection': ol.proj.get('EPSG:31370'),
                center,
                zoom: 13
            })
        });

        map.updateSize();
        const [mapWidth, mapHeight] = map.getSize();
        if (0 === mapWidth || 0 === mapHeight) {
            map.once('change:size', () => {
                map.getView().fit(geoExtent, map.getSize());
            });
        }
        else {
            map.getView().fit(geoExtent, map.getSize());
        }


        Object.defineProperty(this, 'sources', {'value': {
            [DEFAULT_SOURCE]: source
        }});
        Object.defineProperty(this, 'source', {
            'value': this.sources[DEFAULT_SOURCE]
        });
        Object.defineProperty(this, 'olMap', {'value': map});

        this.emit('attach');
    }
}

export default TevMap;
