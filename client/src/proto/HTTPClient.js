import _ from 'lodash';
import request from 'superagent';
import 'whatwg-fetch';


function formatResponse (res) {
    if (res.bodyUsed) {
        throw (new Error('BodyUsed'));
    }
    let resolver = (resolve, reject) => {
        res.text()
            .then((text) => {
                let response = {}, possibleObj;
                try {
                    possibleObj = JSON.parse(text);
                    possibleObj = _.isString(possibleObj) ? null : possibleObj;
                } catch(e) {
                    reject(e);
                }
                response.obj = _.isObject(possibleObj) ? possibleObj : null;
                response.status = res.status;
                response.statusText = res.statusText;
                response.data = text;
                resolve(response);
            });
    };
    return (new Promise(resolver));
}

class FetchHttpClient {
    constructor() {
        this.type = 'FetchHttpClient';
    }

    execute (obj) {
        const method = obj.method.toUpperCase();
        const headers = obj.headers || {};
        let options = {};

        if (obj.body) {
            let body = obj.body;
            if (_.isObject(body)) {
                if (body instanceof FormData) {
                    options.body = body;
                    delete headers['Content-Type'];
                }
                else {
                    options.body = JSON.stringify(body);
                }
            }
            else {
                options.body = body;
            }
        }

        options.method = method;
        options.headers = headers;
        options.mode = 'same-origin';
        options.credentials = 'same-origin';

        if (obj && obj.on) {
            fetch(obj.url, options)
                .then(formatResponse)
                .then((response) => {
                    if (obj.on.response) {
                        obj.on.response(response);
                    }
                });
        }
        else {
            return fetch(obj.url, options).then(formatResponse);
        }
    }
}

// class SuperagentHttpClient {
//     constructor() {
//         this.type = 'SuperagentHttpClient';
//     }
//
//     execute(obj) {
//         let method = obj.method.toLowerCase();
//
//         if (method === 'delete') {
//             method = 'del';
//         }
//         const headers = obj.headers || {};
//         const r = request[method](obj.url);
//
//         if (obj.enableCookies) {
//             r.withCredentials();
//         }
//
//         if(obj.body) {
//             if(_.isObject(obj.body)) {
//                 const contentType = obj.headers['Content-Type'] || '';
//                 if (contentType.indexOf('multipart/form-data') === 0) {
//                     delete headers['Content-Type'];
//                     if({}.toString.apply(obj.body) === '[object FormData]') {
//                         const itr = obj.body.keys();
//                         while(true) {
//                             const v = itr.next();
//                             if(v.done) {
//                                 break;
//                             }
//                             const key = v.value;
//                             var value = obj.body.get(key);
//                             console.log({}.toString.apply(value));
//                             if({}.toString.apply(value) === '[object File]') {
//                                 r.attach(key, value);
//                             }
//                             else {
//                                 r.field(key, value);
//                             }
//                         }
//                     }
//                     else {
//                         var keyname;
//                         for (var keyname in obj.body) {
//                             var value = obj.body[keyname];
//                             r.field(keyname, value);
//                         }
//                     }
//                 }
//                 else if (_.isObject(obj.body)) {
//                     obj.body = JSON.stringify(obj.body);
//                     r.send(obj.body);
//                 }
//             }
//             else {
//                 r.send(obj.body);
//             }
//         }
//
//         let name;
//         for (name in headers) {
//             r.set(name, headers[name]);
//         }
//
//         if(typeof r.buffer === 'function') {
//             r.buffer(); // force superagent to populate res.text with the raw response data
//         }
//
//         r.end((err, res) => {
//             res = res || {
//                 status: 0,
//                 headers: {error: 'no response from server'}
//             };
//             const response = {
//                 url: obj.url,
//                 method: obj.method,
//                 headers: res.headers
//             };
//             let cb;
//
//             if (!err && res.error) {
//                 err = res.error;
//             }
//
//             if (err && obj.on && obj.on.error) {
//                 response.errObj = err;
//                 response.status = res ? res.status : 500;
//                 response.statusText = res ? res.text : err.message;
//                 if(res.headers && res.headers['content-type']) {
//                     if(res.headers['content-type'].indexOf('application/json') >= 0) {
//                         try {
//                             response.obj = JSON.parse(response.statusText);
//                         }
//                         catch (e) {
//                             response.obj = null;
//                         }
//                     }
//                 }
//                 cb = obj.on.error;
//             } else if (res && obj.on && obj.on.response) {
            //     let possibleObj;
            //
            //     // Already parsed by by superagent?
            //     if(res.body && Object.keys(res.body).length > 0) {
            //         possibleObj = res.body;
            //     } else {
            //         try {
            //             possibleObj = jsyaml.safeLoad(res.text);
            //             // can parse into a string... which we don't need running around in the system
            //             possibleObj = (typeof possibleObj === 'string') ? null : possibleObj;
            //         } catch(e) {
            //             helpers.log('cannot parse JSON/YAML content');
            //         }
            //     }
            //
            //     // null means we can't parse into object
            //     response.obj = (typeof possibleObj === 'object') ? possibleObj : null;
            //
            //     response.status = res.status;
            //     response.statusText = res.text;
            //     cb = obj.on.response;
            // }
            // response.data = response.statusText;
//
//             if (cb) {
//                 cb(response);
//             }
//         });
//     }
// }

export default FetchHttpClient;
