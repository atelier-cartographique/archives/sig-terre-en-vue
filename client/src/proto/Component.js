
import Emitter from './Emitter';
import Fragment from './Fragment';
import Version from './Version';
import {subscribe} from '../sys/Store';
import _ from 'lodash';

const proto = {
    'paths': {},
};


class Component extends Emitter {
    constructor (FragmentMixin) {
        super();
        const F = Fragment(FragmentMixin);
        const fragment = new F();
        const version = new Version();
        Object.defineProperty(this, 'fragment', {'value': fragment});
        Object.defineProperty(this, 'state', {'value': version});

        if (this.paths) {
            const paths = this.paths;
            for (const path in paths) {
                const methodName = paths[path],
                    method = this[methodName].bind(this);
                subscribe(path, method);
            }
        }
    }
}

export default Component;
