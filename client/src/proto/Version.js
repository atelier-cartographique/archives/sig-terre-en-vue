
import _ from 'lodash';
import jsonpatch from 'fast-json-patch';
import Emitter from './Emitter';

function freeze (o) {
    Object.freeze(o);
    return o;
}

function copy (o) {
    return JSON.parse(JSON.stringify(o));
}

class Version extends Emitter {
    constructor(currentState, initialState, ops) {
        super();
        initialState = initialState || currentState || {};
        this.initialState = freeze(copy(initialState));
        this.currentState = freeze(copy(currentState || initialState));
        this.ops = ops || [];
    }

    head() {
        return copy(this.currentState);
    }

    get(path, def) {
        return copy(_.result(this.currentState, path, def));
    }

    push(o) {
        const state = copy(this.currentState), rdiff = jsonpatch.compare(o, state);
        this.ops.push(rdiff);
        this.currentState = freeze(copy(o));
        this.emit('change', copy(this.currentState), state);
        return this;
    }

    cloned() {
        const c = new Version(
            copy(this.currentState),
            copy(this.initialState),
            copy(this.ops)
        );
        return c;
    }

    rewinded() {
        const clone = this.cloned(), rdiff = clone.ops.pop(), source = copy(clone.currentState), backup = copy(clone.currentState);
        if (diff) {
            if (jsonpatch.apply(source, rdiff)) {
                clone.currentState = freeze(source);
            }
        }
        return clone;
    }
}

export default Version;
