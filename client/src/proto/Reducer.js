import _ from 'lodash';
import Store from '../sys/Store';
import Action from './Action';

class Reducer {
    constructor(fn, path) {
        this.fn = fn;
        this.domain = path.split('.')[0];
        this.path = path;
    }

    reducer(emit, initialState) {
        const fn = this.fn,
            domain = this.domain,
            path = this.path;

        if (!emit) {
            throw (new Error('MissingArgument emit'));
        }

        const red = function reducer (previousState, plainObjectAction) {
            let action, state;
            // console.log('reducer', fn.name);
            if ('domain' in plainObjectAction) {
                action = new Action(plainObjectAction);
                try {
                    state = fn(previousState, action);
                    if (state) {
                        _.defer(emit, path, action);
                    }
                }
                catch (err) {
                    console.error('something wrong when reducing with',
                                    action.domain, action.type, err.toString());
                }
            }
            else { //@@redux/[INIT, PROBE_UNKNOWN_ACTION]
                state = _.assign({}, initialState[domain]);
            }
            if (!state) {
                state = previousState;
                // throw (new Error('Undefined State off reducer'));
            }
            return state;
        };

        return red;
    }
}

export default Reducer;
