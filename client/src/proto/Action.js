import _ from 'lodash';

const BASE_DOMAIN = '#BASE';

class Action {
    constructor(name, payload) {
        let domain, type, id;
        if (_.isString(name)) {
            const comps = name.split('/');
            if (0 === comps.length) {
                throw (new Error('An Action needs a name'));
            }
            else if (1 === comps.length) {
                domain = BASE_DOMAIN;
                type = comps[0];
            }
            else {
                domain = comps[0];
                type = comps[1];
            }
            id = _.uniqueId('action.');
        }
        else {
            domain = name.domain;
            type = name.type;
            id = name.id;
            payload = name.payload;
        }

        Object.defineProperty(this, 'domain', {'value': domain});
        Object.defineProperty(this, 'type', {'value': type});
        Object.defineProperty(this, 'id', {'value': id});
        Object.defineProperty(this, 'payload', {'value': payload});
    }

    is(domain, type, id) {
        if (1 === arguments.length) {
            return (domain === this.domain);
        }
        else if (2 === arguments.length) {
            return ((domain === this.domain) && (type === this.type));
        }
        else if (3 === arguments.length) {
            return ((domain === this.domain) && (type === this.type) && (id === this.id));
        }
        return false;
    }

    toPlainObject() {
        return {
            'domain': this.domain,
            'type': this.type,
            'id': this.id,
            'payload': this.payload
        };
    }

    static dispatch(store, name, payload) {
        store.dispatch(new Action(name, payload));
    }

}


export default Action;
export {BASE_DOMAIN};
