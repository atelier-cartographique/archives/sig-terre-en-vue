/**
 * proto/Type
 *
 *
 */

import {assign} from 'lodash/fp';


const TypeData = Symbol('TypeData');

class Type {

    constructor (initialData) {
        Object.defineProperty(this, TypeData, {
            value: initialData,
            writable: true
        });
    }

    get data () {
        return assign({}, this[TypeData]);
    }

    set data (d) {
        this[TypeData] = assign({}, d);
    }
}

export default Type;
