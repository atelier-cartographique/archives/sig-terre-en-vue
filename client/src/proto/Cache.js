




class Cache {
    constructor () {
        this._data = {};
    }

    put (key, val) {
        this._data[key] = val;
    }

    get (key, defaultValue) {
        if (key in this._data) {
            return this._data[key];
        }
        return defaultValue;
    }

    has (key) {
        return (key in this._data);
    }
}

export default Cache;
