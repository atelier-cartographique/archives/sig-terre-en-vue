import _ from 'lodash';

function emptyElement (elem, keepChildren) {
    while (elem.firstChild) {
        removeElement(elem.firstChild, keepChildren);
    }
    return elem;
}

function removeElement (elem, keepChildren) {
    if (!keepChildren) {
        emptyElement(elem);
    }
    elem.parentNode.removeChild(elem);
    return elem;
}

class Fragment {
    constructor() {
        const element = document.createDocumentFragment();
        Object.defineProperty(this, 'el', {'value': element});
        this.anchors = {};
        this.build();

    }

    node(name) {
        if (!(name in this.anchors)) {
            console.warn('Fragment does not have node', name);
            return null;
        }
        return this.anchors[name];
    }

    applyTo(name, fn, ctx) {
        const node = this.node(name);
        if (node) {
            fn.call(ctx || this, node);
        }
        return this;
    }

    emptyNode(name) {
        return this.applyTo(name, emptyElement);
    }

    setAttributes(name, attrs) {
        return this.applyTo(name, function setAttributes(node){
            for (const key in attrs) {
                node.setAttribute(key, _.result(attrs, key));
            }
        });
    }

    /**
     * append a text node
     * @method appendText
     * @param  {String}   name    [description]
     * @param  {String}   txt     [description]
     * @param  {Object}   options {tag, attrs}
     * @return {Fragment}           this
     */
    appendText(name, txt, options) {
        if ((undefined === txt) || (null === txt)) {
            txt = '';
        }
        return this.applyTo(name, function appendText(node){
            const txtNode = document.createTextNode(txt.toString());
            if (!options) {
                node.appendChild(txtNode);
            }
            else {
                const elem = document.createElement(_.result(options, 'tag', 'p'));
                _.forEach(_.result(options, 'attributes', {'class':''}),
                            (v, k) => {
                                elem.setAttribute(k, v);
                            });
                elem.appendChild(txtNode);
                node.appendChild(elem);
            }
        });
    }

    setText(name, txt, options) {
        return this.emptyNode(name).appendText(name, txt, options);
    }

    hydrate(data) {
        let key = null;
        const fn = null;
        let val = null;
        const keys = _.keys(this.anchors);
        for (const i in keys) {
            key = keys[i];
            val = _.get(data, key);
            if (val !== undefined) {
                if (val instanceof Function) {
                    this.applyTo(key, fn);
                }
                else {
                    this.setText(key, val.toString());
                }
            }
        }
        return this;
    }

    build() {
        throw (new Error('NotImplemented'));
    }

    names() {
        throw (new Error('NotImplemented'));
    }
};

function create(mixin) {

    class T extends mixin(Fragment) {};
    return T;

};

export default create;
