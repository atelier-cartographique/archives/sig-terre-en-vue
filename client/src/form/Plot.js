import {pick, join} from 'lodash/fp';
import App from '../proto/App';
import {navigate} from '../sys/Router';
import Component from '../proto/Component';
import Fragment from '../proto/Fragment';
import PlotFragment from '../fragment/FormPlot';
import PlotInfoFragment from '../fragment/PlotInfo';
import TevMap, {GeoJSONFormat} from '../proto/Map';
import Store from '../sys/Store';
import {removeElement} from '../helpers';

const PF = Fragment(PlotFragment),
    PIF = Fragment(PlotInfoFragment);


const mkProps = pick(['capakey', 'id', 'user_id']);
const mkPath = (uid, pid) => join('/')(['/user', uid, 'plot', pid]);

function donePlotStyle (res) {
    return new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'rgb(255, 0, 28)',
            width: 1
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255, 0, 28, 0.1)'
        })
    });
}

function potentialPlotStyle (res) {
    return new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'rgb(0, 255, 28)',
            width: 1
        }),
        fill: new ol.style.Fill({
            color: 'rgba(0, 255, 28, 0.1)'
        })
    });
}


const donePlotSource = 'DonePlot';

class FormPlot extends App {
    constructor(...args) {
        super(...args);
        this.fragment = new PF();
        this.addFragment(this.fragment);
    }

    get appName () {
        return 'form.plot';
    }

    get urlPattern () {
        return '/form/plot/:rid?';
    }

    activate() {
        if (this.map) return;
        const tmap = new TevMap();

        const onClick = this.onMapClick.bind(this);
        this.fragment.applyTo('map', node => {
            tmap.attach(node, []);
            tmap.olMap.on('click', onClick);
            // tmap.olMap.on('singleclick', onClick);
        });
        this.map = tmap;

        const request = this.sys.api.request.bind(this.sys.api);
        request('listPotential')
            .then(response => {
                const results = response.obj.results;
                results.forEach(row => {
                    const geom = JSON.parse(row.geom);
                    tmap.addGeometry(geom, {}, 'ac_potentiel')
                        .setStyle(potentialPlotStyle);
                    // tmap.withSource(source => {
                    //     // source.addFeature(GeoJSONFormat.readFeatures(ac_potentiel));
                    //     // source.forEachFeature(feature => feature.setStyle(potentialPlotStyle));
                    // }, 'ac_potentiel');
                });
            });

        request('listAllPlot')
            .then(response => {
                const obj = response.obj;
                this.map.source.clear();
                obj.forEach(plot => {
                    this.map.addGeometry(plot.geometry,
                                         mkProps(plot), donePlotSource)
                        .setStyle(donePlotStyle);
                });
            });

        const savePlot = () => {
            if (!this.capakey) return;
            const user = Store.get('sys.user');
            if (user && user.id) {
                this.sys.api.request('postPlot', {
                    'user-id': user.id,
                    'plot': {
                        'capakey': this.capakey
                    }

                }).then(response => {
                    const user = Store.get('sys.user');
                    navigate(`/user/${user.id}/plot/${response.obj.id}`);
                });

            }
        };

        this.fragment.applyTo('submit', node => {
            node.addEventListener('click', savePlot);
        });
    }

    onMapClick(evt) {
        const checkIsDone = source => {
            const dones = source.getFeaturesAtCoordinate(evt.coordinate);
            if (dones.length > 0) {
                const feature = dones.pop(),
                    props = feature.getProperties();
                navigate(mkPath(props.user_id, props.id));
            }
            else {
                const cooordinates = {
                    'srid': 31370,
                    'longitude': evt.coordinate[0],
                    'latitude': evt.coordinate[1],
                };

                this.sys.api.request('serviceHit', {
                    'coordinates': cooordinates
                })
                .then(r => this.renderQuery(r));
            }
        };

        this.map.withSource(checkIsDone, donePlotSource);
    }


    updateInfo (node, info) {
        const frag = new PIF();
        const area = info.area / 10000,
            isPac = info.pac,
            isResNat = info.reserve_naturelle,
            isNat2000 = info.natura2000_station;

        frag.setText('municipality.fr', info.municipality.fr)
            .setText('municipality.nl', info.municipality.nl)
            .setText('area', area.toFixed(2))
            .setText('affectation.fr', info.affectation.fr)
            .setText('affectation.nl', info.affectation.nl)
            .applyTo('reserve_naturelle_yes', node => {
                if(!isResNat) removeElement(node);
            })
            .applyTo('reserve_naturelle_no', node => {
                if(isResNat) removeElement(node);
            })
            .applyTo('natura2000_yes', node => {
                if(!isNat2000) removeElement(node);
            })
            .applyTo('natura2000_no', node => {
                if(isNat2000) removeElement(node);
            })
            .applyTo('pac_yes', node => {
                if(!isPac) removeElement(node);
            })
            .applyTo('pac_no', node => {
                if(isPac) removeElement(node);
            })
            .applyTo('proprietaire', node => {
                if(info.proprietaire) {
                    frag.setText('proprietaire', info.proprietaire);
                }
                else {
                    frag.setText('proprietaire', 'nn');
                }
            })
            .applyTo('pollution', node => {
                if(info.pollution < 0) {
                    frag.setText('pollution', 'nn');
                }
                else {
                    frag.setText('pollution', info.pollution);
                }
            });

        node.appendChild(frag.el);
    }

    renderQuery(response) {
        const map = this.map,
            data = response.obj,
            frag = this.fragment;
        map.source.clear();
        map.addGeometry(data.geometry, data.properties);

        frag.emptyNode('results')
            .applyTo('results', node => {
                this.updateInfo(node, data.properties);
            });

        this.capakey = data.properties.capakey;
    }
}


export default FormPlot;
