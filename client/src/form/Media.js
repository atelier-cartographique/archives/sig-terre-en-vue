import _ from 'lodash';
import App from '../proto/App';
import {navigate} from '../sys/Router';
import Component from '../proto/Component';
import Fragment from '../proto/Fragment';
import FormMediaFragment from '../fragment/FormMedia';
import Store from '../sys/Store';

const DF = Fragment(FormMediaFragment);

class FormMedia extends App {
    constructor(...args) {
        super(...args);
        this.fragment = new DF();
        this.addFragment(this.fragment);

        this.fragment.applyTo('submit', node => {
            node.addEventListener('click', () => {this.saveMedia();});
        });
    }

    get appName () {
        return 'form.media';
    }

    get urlPattern () {
        return '/form/media/:rid?';
    }

    saveMedia () {
        const user = Store.get('sys.user');
        if (user && user.id) {
            const frag = this.fragment,
                media = frag.node('media'),
                file = media.files[0];

            this.sys.api.request('postMedia', {
                'user-id': user.id,
                'media': file
            })
            .then(response => {
                navigate(`/user/${user.id}/media/${response.obj.id}`);
            })
            .catch(err => console.error('gotcha', err));
        }
    }

    activate(options) {
        // this.fragment.node('name').value = null;
        // this.fragment.node('body').value = null;
        // this.rid = null;
        // if ('rid' in options) {
        //     const user = Store.get('sys.user');
        //     this.rid = options.rid;
        //     this.sys.api.request('getDocument', {
        //         'user-id': user.id,
        //         'resource-id': options.rid
        //     })
        //     .then(response => {
        //         this.fragment.hydrate(response.obj);
        //     });
        // }
    }
}


export default FormMedia;
