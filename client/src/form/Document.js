import _ from 'lodash';
import App from '../proto/App';
import {navigate} from '../sys/Router';
import Component from '../proto/Component';
import Fragment from '../proto/Fragment';
import FormDocumentFragment from '../fragment/FormDocument';
import TevMap from '../proto/Map';
import Store from '../sys/Store';

const DF = Fragment(FormDocumentFragment);

class FormDoc extends App {
    constructor(...args) {
        super(...args);
        this.fragment = new DF();
        this.addFragment(this.fragment);
        this.fragment.applyTo('submit', node => {
            node.addEventListener('click', () => {this.saveDocument();});
        });
    }

    get appName () {
        return 'form.doc';
    }

    get urlPattern () {
        return '/form/document/:rid?';
    }

    saveDocument () {
        const user = Store.get('sys.user');
        if (user && user.id) {
            const frag = this.fragment,
                name = frag.node('name').value,
                body = frag.node('body').value;

            const save = () => {
                if (!this.rid) {
                    return this.sys.api.request('postDocument', {
                        'user-id': user.id,
                        'document': { name, body }
                    });
                }
                else {
                    return this.sys.api.request('updateDocument', {
                        'user-id': user.id,
                        'resource-id': this.rid,
                        'document': { name, body }
                    });
                }
            };

            save().then(response => {
                navigate(`/user/${user.id}/document/${response.obj.id}`);
            })
            .catch(err => console.error('gotcha', err));
        }
    }

    activate(options) {
        this.fragment.node('name').value = null;
        this.fragment.node('body').value = null;
        this.rid = null;
        if ('rid' in options) {
            const user = Store.get('sys.user');
            this.rid = options.rid;
            this.sys.api.request('getDocument', {
                'user-id': user.id,
                'resource-id': options.rid
            })
            .then(response => {
                const doc = response.obj;
                this.fragment
                    .applyTo('name', node => node.value = doc.name)
                    .applyTo('body', node => node.value = doc.body);
            });
        }
    }
}


export default FormDoc;
