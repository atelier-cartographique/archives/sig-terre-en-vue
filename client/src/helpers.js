import _ from 'lodash';
export {_};

export function copy(data) {
    return JSON.parse(JSON.stringify(data));
};

const STAMP_KEY = '__stamp__';

export function stamp(obj) {
    if (STAMP_KEY in obj) {
        throw (new Error('ObjectAlreadyStamped'));
    }
    Object.defineProperty(obj, STAMP_KEY, {'value': _.uniqueId()});
    return obj[STAMP_KEY];
};

export function getStamp(obj) {
    if (STAMP_KEY in obj) {
        return obj[STAMP_KEY];
    }
    throw (new Error('ObjectNotStamped'));
};


export function setProperty (obj, path, val, sep) {
    const keys = path.split(sep || '.');
    if (1 === keys.length) {
        obj[path] = val;
    }
    else {
        const kl = keys.length;
        let currentDict = obj;
        let k;
        for (let i = 0; i < kl; i++) {
            k = keys[i];
            if ((i + 1) === kl) {
                currentDict[k] = val;
            }
            else {
                if (!(k in currentDict)) {
                    currentDict[k] = {};
                }
                else if (!_.isObject(currentDict[k])) {
                    currentDict[k] = {};
                }
                currentDict = currentDict[k];
            }
        }
    }
    return obj;
};

export function resourceUrl (model) {
    return `/user/${model.user_id}/${model.type}/${model.id}`;
};

// DOM


export function select (what, name, all) {
    const selector = `[data-${what}="${name}"]`;
    if (all) {
        return document.querySelectorAll(selector);
    }
    return document.querySelector(selector);
};

export function selectRole(name, all) {
    return exports.select('role', name, all);
};

export function getData(elem, name) {
    return elem.getAttribute(`data-${name}`);
};

export function setAttributes(elem, attrs) {
    _.each(attrs, (val, k) => {
        elem.setAttribute(k, val);
    });
    return elem;
};

export function addClass(elem, c) {
    const ecStr = elem.getAttribute('class');
    const ec = ecStr ? ecStr.split(' ') : [];
    ec.push(c);
    elem.setAttribute('class', _.uniq(ec).join(' '));
    return elem;
};

export function removeClass(elem, c) {
    const ecStr = elem.getAttribute('class');
    const ec = ecStr ? ecStr.split(' ') : [];
    elem.setAttribute('class', _.without(ec, c).join(' '));
    return elem;
};

export function toggleClass(elem, c) {
    const ecStr = elem.getAttribute('class');
    const ec = ecStr ? ecStr.split(' ') : [];
    if (_.indexOf(ec, c) < 0) {
        exports.addClass(elem, c);
    }
    else {
        exports.removeClass(elem, c);
    }
};

export function hasClass(elem, c) {
    const ecStr = elem.getAttribute('class');
    const ec = ecStr ? ecStr.split(' ') : [];
    return !(_.indexOf(ec, c) < 0);
};

export function emptyElement(elem, keepChildren) {
    while (elem.firstChild) {
        exports.removeElement(elem.firstChild, keepChildren);
    }
    return elem;
};

export function removeElement(elem, keepChildren) {
    if (!keepChildren) {
        exports.emptyElement(elem);
    }
    const parent = elem.parentNode,
        evt = document.createEvent('CustomEvent');
    if (parent) {
        parent.removeChild(elem);
        evt.initCustomEvent('remove', false, false, null);
        elem.dispatchEvent(evt);
    }
    return elem;
};

export function appendText(elem, txt) {
    const tNode = document.createTextNode(txt);
    elem.appendChild(tNode);
    return tNode;
};

export function px(val) {
    val = val || 0;
    return `${val.toString()}px`;
};

export function isKeyCode(event, kc) {
    return kc === event.which || kc === event.keyCode;
};
