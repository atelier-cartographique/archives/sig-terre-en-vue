


import Emitter from './proto/Emitter';


const Semaphore = Emitter.create(() => {});

Semaphore.prototype.signal = function () {
    this.emit(...arguments);
};

const semaphore = new Semaphore();
export default exports = semaphore;
