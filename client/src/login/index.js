import _ from 'lodash';
import App from '../proto/App';
import Action from '../proto/Action';
import Fragment from '../proto/Fragment';
import Store from '../sys/Store';

import LoginMixin from '../fragment/Login';

const LoginFragment = Fragment(LoginMixin);

class Login extends App {
    constructor(...args) {
        super(...args);
        this.fragment = new LoginFragment();
        this.addFragment(this.fragment);

        this.sys.api.request('checkUser')
            .then(response => {
                const user = response.obj;
                this.sys.store.dispatch(new Action('sys/login', user));
            });
    }

    get appName () {
        return 'login';
    }

    get urlPattern () {
        return '/login';
    }

    activate(options) {

    }
}

export default Login;
