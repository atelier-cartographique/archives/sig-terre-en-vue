

import Semaphore from './Semaphore';
import helpers from './helpers';

const removeElement = helpers.removeElement, addClass = helpers.addClass, removeClass = helpers.removeClass, appendText = helpers.appendText, isKeyCode = helpers.isKeyCode, selectRole = helpers.selectRole, _ = helpers._;


function wrapForm (form, cancelCallback, ctx) {
    const isForm = (form instanceof HTMLFormElement), wrapper = document.createElement('div'), cBlock = document.createElement('div'), cOk = document.createElement('div'), cCancel = document.createElement('div');

    removeClass(form, 'hidden');

    addClass(wrapper, 'form-wrapper');
    addClass(cBlock, 'command-block');
    addClass(cCancel, 'command background-cancel');

    appendText(cCancel, 'cancel');


    function submit () {
        form.submit();
    }

    function cancel () {
        cancelCallback.call(ctx);
    }

    if (isForm) {
        addClass(cOk, 'command background-ok');
        appendText(cOk, 'ok');
        cBlock.appendChild(cOk);
        cOk.addEventListener('click', submit);
    }
    cCancel.addEventListener('click', cancel);

    cBlock.appendChild(cCancel);
    wrapper.appendChild(form);
    wrapper.appendChild(cBlock);
    return wrapper;
}

function body () {
    return document.querySelector('body');
}


const COLORS = [
    'red',
    'blue',
    'yellow',
];

let colorClass = 0;

function getColorClass (prefix) {
    colorClass += 1;
    if (colorClass >= COLORS.length) {
        colorClass = 0;
    }
    return `${prefix}-${COLORS[colorClass]}`;
}

function switchScrollbar (elem, OnOff) {
    elem.style.overflow = !!OnOff ? 'auto' : 'hidden';
}

/**
 * Generate buttons for forms added to it
 * and handle their show/cancel/submit
 * @method Tools
 * @param  {HTMLElement} elem parent element to attach buttons to
 */
class Tools {
    constructor(elem) {
        this.mainView = selectRole('viewport');
        this.container = elem || body();
        this.drawer = document.createElement('div');
        this.drawerView = document.createElement('div');
        addClass(this.drawer, 'drawer hidden');
        addClass(this.drawerView, 'drawer-view');
        this.drawer.appendChild(this.drawerView);
        body().appendChild(this.drawer);
        this.elements = {};
        this.currentElement = null;

        const add = _.bind(this.add, this);
        _.forEach(selectRole('tool', true), add);


        function kh (evt) {
            if (this.isActive && isKeyCode(evt, 27)) {
                this.hide();
            }
        }
        const bkh = _.bind(kh, this);
        window.addEventListener('keydown', bkh);
    }

    add(elem) {
        const label = elem.getAttribute('data-label');


        const wrapper = document.createElement('div'), button = document.createElement('div');

        button.appendChild(document.createTextNode(label));

        addClass(wrapper, `button-wrapper ${getColorClass('background')}`);
        addClass(button, 'button');
        wrapper.appendChild(button);
        this.container.appendChild(wrapper);

        const cancel = _.bind(this.hide, this);

        const show = _.bind(function (name) {
            this.show(name);
        }, this, label);

        this.elements[label] = wrapForm(removeElement(elem, true), cancel, this);
        button.addEventListener('click', show);
    }

    hide() {
        addClass(this.drawer, 'hidden');
        this.isActive = false;
        switchScrollbar(body(), true);
        // switchScrollbar(this.mainView, true);
    }

    show(name) {
        switchScrollbar(body(), false);
        // switchScrollbar(this.mainView, false);
        if (this.currentElement) {
            removeElement(this.currentElement, true);
        }
        this.currentElement = this.elements[name];
        this.drawerView.appendChild(this.currentElement);
        removeClass(this.currentElement, 'hidden');
        removeClass(this.drawer, 'hidden');

        this.isActive = true;
    }
}

export default Tools;
