import _ from 'lodash';
import App from '../proto/App';
import HomeComp from '../component/Home';

class Home extends App {
    constructor(...args) {
        super(...args);
        this.comp = new HomeComp();
        this.addFragment(this.comp.fragment);
    }

    get appName () {
        return 'home';
    }

    get urlPattern () {
        return '/';
    }

    activate() {
        this.sys.api.request('listUser');
    }
}

export default Home;
