
var path = require('path'),
    webpack = require('webpack'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    autoprefixer = require('autoprefixer');


var sourceDir = path.resolve(__dirname),
    destDir = path.resolve('../tev/http/static');

function source (p) {
    return path.join(sourceDir, p);
}

function dest (p) {
    return path.join(destDir, p);
}

var loaders = [],
    plugins =[];

var webpackOptions = {
    devtool: '#eval-source-map',
    context: source('/src'),
    entry: './main',
    output: {
        path: destDir,
        publicPath: '/static/',
        filename: 'app.js'
    },
    module: {
        loaders: loaders
    },
    plugins: plugins,
    resolve: {
    // you can now require('file') instead of require('file.coffee')
        extensions: ['', '.js', '.json']
    },
};

webpackOptions.devtool = 'source-map';
// plugins.push(new webpack.optimize.UglifyJsPlugin({
//     compress: {
//         warnings: false
//     },
//     comments: false
// }));

// babel
loaders.push({
    test: /\.js$/,
    exclude: /(node_modules|vendors)/,
    loader: 'babel-loader',
    query: {
        presets: ['es2015']
    }
});


/// sass
///
var sassExtract = new ExtractTextPlugin('styles.css', {allChunks: true});

plugins.push(sassExtract);

webpackOptions.sassLoader = {
    includePaths: [source('./styles')],
    outputStyle: 'compressed'
};

webpackOptions.resolveUrlLoader = {
    root: destDir
};

webpackOptions.postcss = [ autoprefixer({ browsers: ['last 2 versions'] })];

loaders.push({
    test: /\.scss$/,
    loader: sassExtract.extract([
        'css?-url',
        'postcss',
        'resolve-url',
        'sass?config=sassLoader',
    ])
});

module.exports = webpackOptions;
