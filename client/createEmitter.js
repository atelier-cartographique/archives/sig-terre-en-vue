
var EventEmitter= require('events').EventEmitter,
    _ = require('lodash');


function create (fn) {
    var T = function () {
        fn.apply(this, arguments);
        EventEmitter.call(this);
    };
    _.assign(T.prototype, fn.prototype, EventEmitter.prototype);
    return T;
}


var My = create(function tr (s) {
    this.testString = s;
});

var my = new My('Me Myself And I');

my.on('event', function(e){
    console.log(this.testString || 'err');
});

my.emit('event');
