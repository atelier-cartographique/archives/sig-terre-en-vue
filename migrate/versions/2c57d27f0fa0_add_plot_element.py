"""empty message

Revision ID: 2c57d27f0fa0
Revises: 45df176b7cc7
Create Date: 2016-06-30 13:08:57.990379

"""

# revision identifiers, used by Alembic.
revision = '2c57d27f0fa0'
down_revision = '45df176b7cc7'
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy import (Column, DateTime, Integer,
                        Text, String, Enum)
from sqlalchemy_utils import PasswordType, UUIDType, JSONType
import uuid
import datetime


def create_table(name, cols):
    # try:
    #     op.drop_table(name)
    # except Exception:
    #     pass

    cols.append(Column('id', UUIDType, primary_key=True, default=uuid.uuid4))
    cols.append(Column('ts', DateTime, default=datetime.datetime.utcnow))
    op.create_table(name, *cols)


def upgrade():
    create_table(
        'user_plot_element',
        [
            Column('type', String),
            Column('status', Integer, default=0),
            Column('element', JSONType),
            Column('plot_id', UUIDType),
        ]
    )


def downgrade():
    op.drop_table('user_plot_element')
