"""query description

Revision ID: 0de84f3e7bc8
Revises: cc67fb1ae8c0
Create Date: 2016-04-11 08:52:42.485087

"""

# revision identifiers, used by Alembic.
revision = '0de84f3e7bc8'
down_revision = 'cc67fb1ae8c0'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('user_query',
                  sa.Column('description', sa.Text))


def downgrade():
    op.drop_column('user_query', 'description')
