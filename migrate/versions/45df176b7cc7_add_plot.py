"""add_plot

Revision ID: 45df176b7cc7
Revises: 0de84f3e7bc8
Create Date: 2016-06-20 11:03:20.731457

"""

# revision identifiers, used by Alembic.
revision = '45df176b7cc7'
down_revision = '0de84f3e7bc8'
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy import (Column, DateTime, Integer,
                        Text, String, Enum)
from sqlalchemy_utils import PasswordType, UUIDType
import uuid
import datetime


def create_table(name, cols):
    # op.drop_table(name)
    cols.append(Column('id', UUIDType, primary_key=True, default=uuid.uuid4))
    cols.append(Column('ts', DateTime, default=datetime.datetime.utcnow))
    op.create_table(name, *cols)


def upgrade():
    create_table(
        'user_plot',
        [ Column('capakey', String) ]
    )


def downgrade():
    op.drop_table('user_plot')
