"""initial

Revision ID: cc67fb1ae8c0
Revises: None
Create Date: 2016-03-21 13:17:46.849952

"""

# revision identifiers, used by Alembic.
revision = 'cc67fb1ae8c0'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy import (Column, DateTime, Integer,
                        Text, String, Enum)
from sqlalchemy_utils import PasswordType, UUIDType
import uuid
import datetime
from tev.user.models import Role

def create_table(name, cols):
    # op.drop_table(name)
    cols.append(Column('id', UUIDType, primary_key=True, default=uuid.uuid4))
    cols.append(Column('ts', DateTime, default=datetime.datetime.utcnow))
    op.create_table(name, *cols)

def upgrade():

    create_table(
        "user_auth",
        [
            Column('email', String),
            Column('password',
               PasswordType(schemes=['pbkdf2_sha512', 'md5_crypt' ],
                            deprecated=['md5_crypt']))
        ]
    )

    create_table(
        "user_user",
        [
            Column('auth_id', UUIDType),
            Column('name', String),
            Column('role', Role)
        ]
    )

    create_table(
        'user_resource',
        [
            Column('user_id', UUIDType),
            Column('resource_id', UUIDType),
            Column('type', String),
            Column('name', String)
        ]
    )

    create_table(
        'user_document',
        [
            Column('body', Text),
            Column('format', String)
        ]
    )

    create_table(
        'user_query',
        [
            Column('body', Text),
            Column('geo_column', String)
        ]
    )

    create_table(
        'user_media',
        [
            Column('path', String),
            Column('mimetype', String)
        ]
    )

    create_table(
        'user_link',
        [
            Column('source_id', UUIDType),
            Column('target_id', UUIDType)
        ]
    )




def downgrade():
    pass
