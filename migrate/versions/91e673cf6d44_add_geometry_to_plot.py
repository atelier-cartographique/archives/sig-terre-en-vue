"""add geometry to plot

Revision ID: 91e673cf6d44
Revises: 38c222ec6dea
Create Date: 2016-10-17 11:26:01.793982

"""

# revision identifiers, used by Alembic.
revision = '91e673cf6d44'
down_revision = '38c222ec6dea'
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy import Column
from geoalchemy2 import Geometry


def upgrade():
    op.add_column('user_plot', Column('geom', Geometry('MULTIPOLYGON', srid=31370)))


def downgrade():
    pass
