"""add user id to PlotElement

Revision ID: 7d5dddf219e2
Revises: 2c57d27f0fa0
Create Date: 2016-06-30 13:31:01.845206

"""

# revision identifiers, used by Alembic.
revision = '7d5dddf219e2'
down_revision = '2c57d27f0fa0'
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy import (Column, DateTime, Integer,
                        Text, String, Enum)
from sqlalchemy_utils import PasswordType, UUIDType
import uuid
import datetime


def create_table(name, cols):
    cols.append(Column('id', UUIDType, primary_key=True, default=uuid.uuid4))
    cols.append(Column('ts', DateTime, default=datetime.datetime.utcnow))
    op.create_table(name, *cols)



def upgrade():
    op.add_column('user_plot_element', Column('user_id', UUIDType))


def downgrade():
    op.drop_column('user_plot_element', 'user_id')
