"""unique plot

Revision ID: 38c222ec6dea
Revises: 515a4127ae1b
Create Date: 2016-09-28 11:21:58.224411

"""

# revision identifiers, used by Alembic.
revision = '38c222ec6dea'
down_revision = '515a4127ae1b'
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy import text

def upgrade():
    # from https://wiki.postgresql.org/wiki/Deleting_duplicates
    op.execute(text("""
    DELETE FROM user_plot
    WHERE ts IN (SELECT ts
        FROM (SELECT ts, ROW_NUMBER() OVER (PARTITION BY capakey ORDER BY ts) AS rnum
            FROM user_plot) t
        WHERE t.rnum > 1);
    """))
    op.create_unique_constraint('uniq_plot_capakey', 'user_plot', ['capakey'])


def downgrade():
    pass
