"""final plot element

Revision ID: 515a4127ae1b
Revises: 7d5dddf219e2
Create Date: 2016-08-18 12:16:12.179692

"""

# revision identifiers, used by Alembic.
revision = '515a4127ae1b'
down_revision = '7d5dddf219e2'
branch_labels = None
depends_on = None

from alembic import op
from sqlalchemy import (Column, DateTime, Integer,
                        Text, String, Enum)
from sqlalchemy_utils import PasswordType, UUIDType
import uuid
import datetime



def upgrade():
    op.drop_column('user_plot_element', 'type')
    op.drop_column('user_plot_element', 'status')
    op.drop_column('user_plot_element', 'element')

    op.add_column('user_plot_element', Column('question_code', String))
    op.add_column('user_plot_element', Column('answer_code', String))


def downgrade():
    pass # TODO
