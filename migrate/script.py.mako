"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}

"""

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}

from alembic import op
from sqlalchemy import (Column, DateTime, Integer,
                        Text, String, Enum)
from sqlalchemy_utils import PasswordType, UUIDType
import uuid
import datetime
${imports if imports else ""}

def create_table(name, cols):
    cols.append(Column('id', UUIDType, primary_key=True, default=uuid.uuid4))
    cols.append(Column('ts', DateTime, default=datetime.datetime.utcnow))
    op.create_table(name, *cols)



def upgrade():
    ${upgrades if upgrades else "pass"}


def downgrade():
    ${downgrades if downgrades else "pass"}
