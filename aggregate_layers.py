#!/usr/bin/env python


import click

GEOM_COL = 'geom'
SRID = 31370
CADASTRE_TABLE = 'cadastre_2015'
# CADASTRE_GEOM = 'geom'
# CADASTRE_KEY = 'capakey'

CREATE_TARGET_TEMPLATE = """
CREATE TABLE {table_name} (
gid serial PRIMARY KEY,
capakey character varying(17),
geom geometry(MultiPolygon, 31370)
);
"""

CREATE_TMP_TEMPLATE = """
CREATE TEMPORARY TABLE {table_name} (
gid serial PRIMARY KEY,
source character varying,
{geom_name} GEOMETRY({geom_type}, {geom_srid})
);
"""

CREATE_INDEX_TEMPLATE = """
CREATE INDEX {index_name}
ON {table_name}
USING gist
({geom_name});
"""

AGGREGATE_LAYER_TEMPLATE = """
INSERT INTO {target_table}
    (source, {target_geom})
    SELECT
        '{source_name}', ST_Multi({source_geom})
    FROM {source_table};
"""

AGGREGATE_LAYER_WHERE_TEMPLATE = """
INSERT INTO {target_table}
    (source, {target_geom})
    SELECT
        '{source_name}', ST_Multi({source_geom})
    FROM {source_table}
    WHERE {where_column} = '{where_attribute}';
"""

MASK_TEMPLATE = """
INSERT INTO {target_table}
    (capakey, geom)
    SELECT DISTINCT
        ON (cadastre.capakey)
        cadastre.capakey,
        cadastre.geom
    FROM
        {cadastre_table} AS cadastre,
        {mask_table} AS mask
    WHERE
        ST_Intersects(cadastre.geom, mask.geom)
    ORDER BY
        cadastre.capakey;
"""

EXCLUDE_TEMPLATE = """
INSERT INTO {exclude_table}
    (capakey)
    SELECT
        base.capakey AS capakey
    FROM
        {base_table} AS base
        INNER JOIN
        {constraint_table} AS ex
        ON (ST_Intersects(base.geom, ex.geom));
"""

CONSTRAINT_TEMPLATE = """
INSERT INTO {target_table}
    (capakey, geom)
    SELECT
        base.capakey, base.geom
    FROM
        {base_table} AS base
        LEFT OUTER JOIN
            {exclude_table} AS constr
        ON (base.capakey = constr.capakey)
    WHERE constr.capakey IS NULL;
"""

def table_name (schema, name):
    return '"{}"."{}"'.format(schema, name)

def index_name (name):
    return '"gidx_' + GEOM_COL + '_' + name + '"'

def drop_table (schema, name):
    t_name = table_name(schema, name)
    i_name = index_name(name)
    print('DROP TABLE IF EXISTS {};'.format(t_name))
    print('DROP INDEX IF EXISTS {};'.format(i_name))

def create_target_table(schema, name, base_type='polygon'):
    geom_type = 'MULTI' + base_type.upper()
    t_name = table_name(schema, name)
    i_name = index_name(name)
    drop_table(schema, name)
    print(CREATE_TARGET_TEMPLATE.format(
        table_name=t_name,
        geom_name=GEOM_COL,
        geom_type=geom_type,
        geom_srid=SRID
    ))
    print(CREATE_INDEX_TEMPLATE.format(
        table_name=t_name,
        index_name=i_name,
        geom_name=GEOM_COL
    ))

def create_tmp_table(schema, name, base_type='polygon'):
    geom_type = 'MULTI' + base_type.upper()
    t_name = table_name(schema, name)
    i_name = index_name(name)
    drop_table(schema, name)
    print(CREATE_TMP_TEMPLATE.format(
        table_name=t_name,
        geom_name=GEOM_COL,
        geom_type=geom_type,
        geom_srid=SRID
    ))
    print(CREATE_INDEX_TEMPLATE.format(
        table_name=t_name,
        index_name=i_name,
        geom_name=GEOM_COL
    ))


def aggregate_layer (source, target, schema, gc_name):
    source_list = source.split(':')
    if len(source_list) == 3:
        source_name = source_list[0]
        wc = source_list[1]
        wa = source_list[2]
        print(AGGREGATE_LAYER_WHERE_TEMPLATE.format(
            source_geom=gc_name,
            source_name=source_name,
            target_table=table_name(schema, target),
            source_table=table_name(schema, source_name),
            target_geom=GEOM_COL,
            where_column=wc,
            where_attribute=wa
        ))
    elif len(source_list) == 1:
        print(AGGREGATE_LAYER_TEMPLATE.format(
            source_geom=gc_name,
            source_name=source,
            target_table=table_name(schema, target),
            source_table=table_name(schema, source),
            target_geom=GEOM_COL
        ))
    else:
        raise Exception('wrong source')

def mask_cadastre (mask, target, schema):
    create_target_table(schema, target)
    print(MASK_TEMPLATE.format(
        target_table=table_name(schema, target),
        cadastre_table=table_name(schema, CADASTRE_TABLE),
        mask_table=table_name(schema, mask)
    ))

def apply_constraints (base, target, constraints, schema):
    print('CREATE TEMPORARY TABLE tmp_exclude_list (capakey character varying);')
    base_table = table_name(schema, base)
    target_table = table_name(schema, target)
    for constraint in constraints:
        constraint_table = table_name(schema, constraint)
        print(EXCLUDE_TEMPLATE.format(
            exclude_table='tmp_exclude_list',
            base_table=base_table,
            constraint_table=constraint_table
        ))

    print(CONSTRAINT_TEMPLATE.format(
        target_table=target_table,
        base_table=base_table,
        exclude_table='tmp_exclude_list'
    ))


@click.command()
@click.argument('schema')
@click.argument('target')
@click.option('--source', '-s', type=(str, str), multiple=True)
@click.option('--constraint', '-c', type=str, multiple=True)
def aggregate(schema, target, source, constraint):
    print('BEGIN;')
    tmp = 'tmp_' + target
    base = 'base_' + target

    create_tmp_table(schema, tmp)
    for source_name, gc_name in source:
        aggregate_layer(source_name, tmp, schema, gc_name)

    mask_cadastre(tmp, base, schema)

    create_target_table(schema, target)

    apply_constraints(base, target, constraint, schema)

    drop_table(schema, tmp)
    drop_table(schema, base)
    print('COMMIT;')

if __name__ == '__main__':
    aggregate()
