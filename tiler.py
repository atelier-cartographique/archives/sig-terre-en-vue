#!/usr/bin/env python


import sys
import math
import gdal
from gdalconst import *
import osr
import numpy


driver_tiff = gdal.GetDriverByName('GTiff')
driver_options= ('COMPRESS=JPEG', 'PHOTOMETRIC=YCBCR')
identity_tr = (1, 0, 0, 1, 0, 0)
out_srs = osr.SpatialReference()
out_srs.ImportFromEPSG(31370)

def load_raster (fn):
    return gdal.Open(fn)

def read_data (source, band_n, x, y, width, height):
    data = numpy.zeros((width, height), dtype=numpy.uint8)
    band = source.GetRasterBand(band_n)
    return band.ReadAsArray(x, y, width, height, buf_obj=data)

def configure_sink (sink, proj, tr):
    sink.SetGeoTransform(tr)
    # sink.SetProjection(out_srs.ExportToWkt())
    sink.SetProjection(proj)
    return sink


def create_tile_sink (name, width, height, proj,    tr):
    sink = driver_tiff.Create(name, width, height,
                              3, gdal.GDT_Byte, driver_options)
    return configure_sink(sink, proj, tr)



def write_data (sink, band_n, data):
    sink.GetRasterBand(band_n).WriteArray(data)

def get_transform (source, x_offset, y_offset):
    a0, a1, a2, a3, a4, a5 = source.GetGeoTransform()
    tr = (a0 + (x_offset * a1), a1, a2, a3 + (y_offset * a5), a4, a5)
    # if y_offset != 0:
    #     import pdb; pdb.set_trace()
    return tr

def process_source (source, width, height):
    s_width = source.RasterXSize
    s_height = source.RasterYSize
    xn = int(math.ceil(s_width / width))
    yn = int(math.ceil(s_height / height))
    bands = (1,2,3)

    for y in xrange(yn):
        for x in xrange(xn):
            x_offset = x * width
            y_offset = y * height
            name = '{}_{}.tiff'.format(x_offset, y_offset)
            sink = create_tile_sink(name, width, height,
                                    source.GetProjection(),
                                    get_transform(source, x_offset, y_offset)
                                    )
            print('creating {}'.format(name))
            for band_n in bands:
                write_data(sink, band_n,
                           read_data(
                                source, band_n,
                                x_offset, y_offset, width, height
                                )
                           )


if __name__ == '__main__':

    fn = sys.argv[1]
    width = int(sys.argv[2])
    try:
        height = int(sys.argv[3])
    except Exception:
        height = width
    print('loading {}'.format(fn))
    source = load_raster(fn)
    process_source(source, width, height)
