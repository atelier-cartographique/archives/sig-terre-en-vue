

./aggregate_layers.py tev ac_green_pras_noalv \
-s alv_2015 geom \
-s brugis_affectation:\"AFFECTATION\":AfZAgric geom \
-s brugis_affectation:\"AFFECTATION\":AfZBio geom \
-s brugis_affectation:\"AFFECTATION\":AfZCimetiere geom \
-s brugis_affectation:\"AFFECTATION\":AfZForest geom \
-s brugis_affectation:\"AFFECTATION\":AfZParc geom \
-s brugis_affectation:\"AFFECTATION\":AfZParcKR geom \
-s brugis_affectation:\"AFFECTATION\":AfZResFonc geom \
-s brugis_affectation:\"AFFECTATION\":AfZVerte geom \
-c alv_2015
